### 1. Introducción al curso

Básicamente lo que vamos a aprender en el curso. Se ve interesante

---

### 2. Editor a utilizar

El instructor va a utilizar Sublime Text

---

### 3. Creación de tienda virtual

Se muestra un proyecto real con algunos "bonus". La tienda tendrá todas las funcionalidades básicas que debería tener una tienda. 

- Está conectada a la API de WhatsApp. 
- Tiene galería de imágenes.
- Las URL se cargan con el ID y el slug.
- Permite indicar la cantidad de productos que uno quiere comprar.
- Opción de trabajar con un carro de productos.
- Tiene implementado Webpay.

---