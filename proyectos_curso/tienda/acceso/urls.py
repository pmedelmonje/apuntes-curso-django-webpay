from django.urls import path
from .views import *

urlpatterns = [
	path('login/', accesoLogin, name = "accesoLogin"),
	path('registro/', accesoRegistro, name = "accesoRegistro"),
	path('salir/', accesoSalir, name = "accesoSalir"),
]