from django.urls import path
from .views import *

urlpatterns = [
	path('', carroInicio, name= "carroInicio"),
	path('crear', carroCrear, name = "carroCrear"),
	path('vaciar/', carroVaciar, name="carroVaciar"),
    path('quitarDeCarro/<int:id>', carroQuitar, name="carroQuitar"),
    path('modificarCantidadCarro/<int:id>/<int:cantidad>', carroModificarCantidad, name="carroModificarCantidad"),
    path('pagar/', carroPagar, name="carroPagar"),
    path('webpay/', carroWebpay, name="carroWebpay"),
    path('webpayRespuesta/', carroWebpayRespuesta, name="carroWebpayRespuesta"),
]