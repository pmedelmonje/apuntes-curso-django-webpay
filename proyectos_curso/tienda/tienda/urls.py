from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('home/', include('home.urls'), name="homeUrls"),
    path('acceso/', include('acceso.urls'), name="accesoUrls"),
    path('productos/', include('productos.urls'), name="productosUrls"),
    path('carro/', include('carro.urls'), name="carroUrls"),
    
]
