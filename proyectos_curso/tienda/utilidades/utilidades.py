from datetime import datetime, date, timedelta
import os
from os import remove
from django.conf import settings
from urllib.parse import urlparse, parse_qs
from django.core.paginator import Paginator
from django.template import Context, Template
# para los email
import smtplib 
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
# token
import jwt
import time


def getToken(json):
    token= jwt.encode(json, settings.SECRET_KEY, algorithm='HS256')
    return token


def traducirToken(token):
    return jwt.decode(token, settings.SECRET_KEY, algorithms=['HS256'])


def sendMail(html, asunto, para):

    msg = MIMEMultipart('alternative')
    msg['Subject'] = asunto
    msg['From'] = settings.MAIL_SALIDA
    msg['To'] = para

    msg.attach(MIMEText(html, 'html'))
    
    try:
        server = smtplib.SMTP("smtp.mail.yahoo.com", 587)#(settings.SERVER_STMP, settings.PUERTO_SMTP)
        server.login("pajarracodetierra@yahoo.com", "celtas3791")#(settings.MAIL_SALIDA, settings.PASSWORD_MAIL_SALIDA)
        server.sendmail("pajarracodetierra@yahoo.com", para, msg.as_string())#(settings.MAIL_SALIDA, para, msg.as_string())
        server.quit()
    except smtplib.SMTPResponseException as e:
        pass


def getExtension(file):
    extension = os.path.splitext(str(file))[1]
    if extension == ".png":
        return True
    elif extension == ".jpg":
        return True
    elif extension == ".jpeg":
        return True
    elif extension == ".JPG":
        return True
    elif extension == ".PNG":
        return True
    elif extension == ".JPEG":
        return True
    else:
        return False


def paginacion(total, request):
    paginator = Paginator(total, settings.TOTAL_PAGINAS)
    page = request.GET.get('page')
    datos = paginator.get_page(page)
    numeros=[]
    if len(datos)>=settings.TOTAL_PAGINAS:
        for ultima in range(1, datos.paginator.num_pages):
            numeros.append(ultima)
        numeros.append(ultima+1)
    return [datos, numeros, page]


def numberFormat(numero):
    if numero == None:
        return 0
    else:
        return "{:,}".format(numero).replace(",",".")
