from django.shortcuts import render
from django.http import Http404
from home.models import *

# Create your views here.

def productosDetalle(request, id, slug):
	try:
		producto = Producto.objects.filter(pk = id, 
			slug = slug, estado_id=1).get()
	except Producto.DoesNotExist:
		raise Http404

	relacionados = Producto.objects.filter(estado_id=1, 
		producto_categoria = producto.producto_categoria_id).filter(
		estado_id=1).all()

	fotos = ProductoFotos.objects.filter(producto_id=id).all()

	contexto = {"producto":producto, "relacionados":relacionados,
				"fotos":fotos}

	return render(request, "productos/productosDetalle.html", contexto)

