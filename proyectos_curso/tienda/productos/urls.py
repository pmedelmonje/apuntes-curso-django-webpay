from django.urls import path
from .views import *


urlpatterns = [
	path('detalle/<int:id>/<str:slug>', productosDetalle, 
		name = "productosDetalle"),
]