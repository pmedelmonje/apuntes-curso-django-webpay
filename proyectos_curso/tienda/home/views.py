from django.shortcuts import render
from home.models import *

# Create your views here.


def homeIndex(request):
	productos = Producto.objects.filter(estado_id=1).order_by('-id').all()[:8]
	return render(request, "home/index.html", {"productos":productos})
