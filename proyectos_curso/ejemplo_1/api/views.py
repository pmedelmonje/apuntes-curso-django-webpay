from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.parsers import FileUploadParser
from django.contrib.auth import authenticate, login
from django.shortcuts import Http404, HttpResponse
from django.conf import settings
from jose import jwt
import time
from utilidades import utilidades, dreamhost
from home.models import *
from .serializers import *


class Class_Test(APIView):

	#petición GET
	def get(self, request):
		return Response({"mensaje":"Holi. ¿Tení pololi?"})

	
	#petición POST
	def post(self, request):

		return Response({"mensaje":"Respuesta POST"})


	#petición PUT
	def put(self, request):

		return Response({"mensaje":"Respuesta PUT"})


	#petición DELETE
	def delete(self, request):

		return Response({"mensaje":"Respuesta DELETE"})



class Class_TestParametros(APIView):
	#petición GET
	def get(self, request, id):
		return Response({"mensaje":f"id={id}"})


class Class_TestRequest(APIView):

	#pasar parámetros vía JSON request
	def post(self, request):

		return Response(request.data)
		#return Response({"nombre": request.data.get("nombre")})


class Class_TestLogin(APIView):

	"""
	request: {"usuario":"admin", "password":"gastroll3791"}

	"""

	def post(self, request):
		data = request.data
		if data.get("usuario") == None or data.get("password") == None:
			raise Http404

		user = authenticate(request, username=data.get("usuario"), 
			password=data.get("password"))

		if user is not None:
			login(request, user)
			usersMetadata = UsersMetadata.objects.filter(user_id=
				request.user.id).get()
			nombre = f"{request.user.first_name} {request.user.last_name}"
			token = utilidades.getToken({"id":request.user.id, "campo":"hola"})
			data_json = {"mensaje":"Ok", "nombre": nombre, "token":token}
			return Response(data_json)
		else:
			return Response({"mensaje":"Se ha ingresado datos incorrectos."})



class Class_TestJwt(APIView):
	
	def get(self, request):
		cabeceros = request.headers.get('Authorization')
		if not cabeceros:
			res = HttpResponse("Unauthorized")
			res.status_code = 401
			return res

		try:	
			resuelto = jwt.decode(cabeceros, settings.SECRET_KEY, 
			algorithms = ['HS256'])
		except Exception as e:
			res = HttpResponse("Unauthorized")
			res.status_code = 401
			return res

		if resuelto['campo'] != 'hola':
			res = HttpResponse("Unauthorized")
			res.status_code = 401
			return res

		data = request.data
		return Response({"texto":resuelto})


class Class_TestCrearRegistros(APIView):

	def post(self, request):
		cabeceros = request.headers.get('Authorization')
		if not cabeceros:
			res = HttpResponse("Unauthorized")
			res.status_code = 401
			return res

		try:	
			resuelto = jwt.decode(cabeceros, settings.SECRET_KEY, 
			algorithms = ['HS256'])
		except Exception as e:
			res = HttpResponse("Unauthorized")
			res.status_code = 401
			return res

		if resuelto['campo'] != 'hola':
			res = HttpResponse("Unauthorized")
			res.status_code = 401
			return res

		data = request.data
		cat = Categoria.objects.create(nombre=data["nombre"], 
			slug=data['nombre'])		
		return Response({"texto":f"Se creó la categoría {cat.nombre}"})


class Class_TestSerializable(APIView):

	def get(self, request):
		datos = Categoria.objects.all()
		datos_json = CategoriaSerializer(datos, many=True)
		return Response(datos_json.data)

	#request: {"nombre":"Ropa"}

	def post(self, request):
		datos_json = CategoriaSerializer(data = request.data)

		if datos_json.is_valid():
			datos_json.save()
			return Response(datos_json.data)

		return Response(datos_json.errors, status=400)


"""
class Class_TestProductos(APIView):
	
	def get(self, request):
		datos = Producto.objects.all()
		datos_json = ProductoSerializer(datos, many=True)
		return Response(datos_json.data)

	def post(self, request):
		parser_class = (FileUploadParser, )
		datos_json = ProductoSaveSerializer(data = request.data)

		if datos_json.is_valid():
			datos_json.save()
			return Response(datos_json.data)

		return Response(datos_json.errors, status=400)
"""

class Class_TestProductos(APIView):
	
	def get(self, request):
		datos = Producto.objects.all()
		datos_json = ProductoSerializer(datos, many=True)
		return Response(datos_json.data)

	def post(self, request):
		parser_class = (FileUploadParser, )
		datos_json = ProductoSaveSerializer(data = request.data)

		if datos_json.is_valid():
			datos_json.save()
			fotoarray = datos_json.data["foto"].split("/")
			foto = f"producto/{fotoarray[3]}"
			dreamhost.moverArchivoProducto(foto, 
				int(datos_json.data["id"]))

			return Response(datos_json.data, status=201)

		return Response(datos_json.errors, status=400)
