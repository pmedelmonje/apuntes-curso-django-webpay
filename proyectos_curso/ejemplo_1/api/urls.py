from django.urls import path
#from django.conf.urls import url
from django.urls import re_path
from .views import *

urlpatterns = [
	path('test/', Class_Test.as_view(), name="api_test"),
	path('test-parametros/<int:id>', Class_TestParametros.as_view(), 
		name="api_test_parametros"),
	path('test-request/', Class_TestRequest.as_view(),
		name="api_test_request"),
	path('test-login/', Class_TestLogin.as_view()),
	path('test-jwt/', Class_TestJwt.as_view()),
	path('test-crear-registros/', Class_TestCrearRegistros.as_view()),
	path('test-serializable/', Class_TestSerializable.as_view()),
	path('test-productos/', Class_TestProductos.as_view()),
]

