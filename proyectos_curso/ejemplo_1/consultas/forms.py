from django import forms
from utilidades import *
from home.models import *

class FormularioProducto(forms.Form):
	nombre = forms.CharField(required = True, widget=forms.TextInput(attrs=
	{'class':'form-control', 'placeholder':'Nombre', 'autocomplete':'off'}))
	
	precio = forms.CharField(required = True, widget=forms.TextInput(attrs=
	{'class':'form-control', 'placeholder':'Precio', 'onkeypress':'return soloNumeros(event)'}))
	
	categoria = forms.ChoiceField(required = True, widget =forms.Select(
		attrs={'class':'form-control',}), choices=formularios.get_categorias_choices
	)
	
	descripcion = forms.CharField(required = True, widget=forms.Textarea(attrs=
	{'class':'form-control', 'placeholder':'Descripción', 'rows':3, 'columns':100, 'autocomplete':'off'}))
	
	file = forms.CharField(required=False, widget=forms.TextInput(attrs={
		'type':'file', 'id':'formFile', 'class':'form-control'}))


class FormularioAtributo(forms.Form):
	atributos = forms.CharField(required = True, widget=
		forms.CheckboxSelectMultiple(attrs={'class':'form-check-input'},
		choices = Atributo.objects.all().values_list('id','nombre')))