from django.shortcuts import render
from django.http import Http404, HttpResponseRedirect
from django.core.files.storage import FileSystemStorage
from django.contrib import messages
from datetime import datetime, date, timedelta
import os
from home.models import *
from .forms import *
from utilidades import utilidades, dreamhost


# Create your views here.

def consultasHome(request):
	categorias = Categoria.objects.all()
	total = Producto.objects.order_by("-id").all()
	paginar = utilidades.get_paginacion(total, request)

	return render(request, "consultas/consultasHome.html", 
				{"categorias" : categorias, "total" : total,
				"datos" : paginar[0], "numeros": paginar[1],
				"page" : paginar[2], })


def consultasCrear(request):

	if request.method == "POST":
		form = FormularioProducto(request.POST, request.FILES)
		if request.POST['foto'] == 'vacio':
			foto = "default.png"
		else:
			myfile = request.FILES['file']
			if utilidades.getExtension(request.FILES['file']):
				fs = FileSystemStorage()
				fecha = datetime.now()
				foto = f"{datetime.timestamp(fecha)}{os.path.splitext(str(request.FILES['file']))[1]}"
				filename = fs.save(f"producto/{foto}", myfile)
				uploaded_file_url = fs.url(filename)
				dreamhost.moverArchivoProducto2(foto)
			else:
				mensaje = "Formato no válido. Debe ser un archivo jpg, jpeg, png o gif."
				messages.add_message(request, messages.WARNING, mensaje)
				return HttpResponseRedirect('/consultas/crear')

		Producto.objects.create(nombre = request.POST['nombre'], 
			precio = request.POST['precio'], descripcion = request.POST['descripcion'],
			foto = foto, categoria_id = request.POST['categoria'])
		messages.add_message(request, messages.SUCCESS, 
			f"El Producto {request.POST['nombre']} se ha ingresado correctamente")
	else:
		form = FormularioProducto()

	return render(request, "consultas/consultasCrear.html", {'form':form,})


def consultasBuscador(request):
	if not request.GET.get('b'):
		b = ''
	else:
		b = request.GET.get('b')

	categorias = Categoria.objects.all()
	total = Producto.objects.filter(nombre__icontains=b).order_by("-id").all()
	paginar = utilidades.get_paginacion(total, request)

	return render(request, "consultas/buscador.html", 
				{"categorias" : categorias, "total" : total,
				"datos" : paginar[0], "numeros": paginar[1],
				"page" : paginar[2], "b" : b,})


def consultaProductosPorCategoria(request, slug):
	try:
		cat = Categoria.objects.filter(slug = slug).get()
	except Categoria.DoesNotExist:
		raise Http404

	#productos = Producto.objects.filter(categoria_id = cat.id).all()
	productos = Producto.objects.filter(categoria__slug = slug)
	#usando un where in
	#productos = Producto.objects.filter(categoria_id__in=[cat.id]).all()
	categorias = Categoria.objects.all()

	return render(request, "consultas/productosPorCategoria.html", 
				{'cat' : cat, 'productos' : productos})


def productoDetalle(request, id, slug):
	try:
		producto = Producto.objects.filter(slug = slug, id = id).get()
	except Producto.DoesNotExist:
		raise Http404

	categorias = Categoria.objects.all()

	return render(request, "consultas/productoDetalle.html", 
		{"producto" : producto, "categorias" : categorias})


def consultasAgregarAtributo(request, id):
	try:
		producto = Producto.objects.get(pk = id)
	except Producto.DoesNotExist:
		raise Http404

	if request.method == "POST":
		form = FormularioAtributo(request.POST)
		if form.is_valid():
			datosForm = form.cleaned_data
			modelos_checks = eval(datosForm['atributos'])
			ProductoAtributo.objects.filter(producto_id = id).delete()
			for modelos_check in modelos_checks:
				guardar = ProductoAtributo()
				guardar.producto_id = id
				guardar.atributo_id = modelos_check
				guardar.save()
			messages.add_message(request, messages.SUCCESS, 
			f"Atributo(s) aplicado(s) exitosamente")
			return HttpResponseRedirect(f"/consultas/agregarAtributo/{id}")
		else:
			pass
	else:
		form = FormularioAtributo()

	#Otra forma de hacerlo
	atributos = Atributo.objects.all()
	return render(request, "consultas/agregarAtributo.html", 
		{'form' : form, 'producto' : producto, 'atributos': atributos})