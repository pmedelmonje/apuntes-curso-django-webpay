from django.urls import path
from .views import *

urlpatterns = [
	path('', consultasHome, name = "consultasHome"),
	path('buscador/', consultasBuscador, name = "consultasBuscador"),
	path('crear/', consultasCrear, name = "consultasCrear"),
	path('agregarAtributo/<int:id>', consultasAgregarAtributo, name = "agregarAtributo"),
	path('productos-por-categoria/<str:slug>', consultaProductosPorCategoria, 
		name = 'productos-por-categoria'),
	path('producto-detalle/<int:id>/<str:slug>/', 
		productoDetalle, name = "producto-detalle"),
]