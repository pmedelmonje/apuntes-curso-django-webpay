from functools import wraps
from django.contrib.auth import authenticate
from home.models import *
from django.http import HttpResponseRedirect, Http404
from django.contrib import messages


def logueado():
	def _activo_required(func):
		@wraps(func)
		def _decorator(request, *args, **kwargs):
			#aquí va el código
			if not request.user.is_authenticated:
				messages.add_message(request, messages.WARNING, 
			f"Debes iniciar sesión para ver este contenido")
				return HttpResponseRedirect("/formularios/login")
			else:
				return func(request, *args, **kwargs)

		return _decorator
	return _activo_required


def ejemplo():
	def _activo_required(func):
		@wraps(func)
		def _decorator(request, *args, **kwargs):
			#aquí va el código
			pass

		return _decorator
	return _activo_required