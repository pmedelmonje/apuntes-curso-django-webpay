from django.shortcuts import render
from django.http import HttpResponse, Http404, HttpResponseRedirect
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from home.models import *
from .forms import *
from .decorators import *

# Create your views here.

def formulariosHome(request):
	return render(request, "formularios/formulariosHome.html", {})

def formularioSimple(request):
	if request.method == "POST":		
		descripcion = f"El nombre es {request.POST['nombre']}, correo = {request.POST['correo']}, correo = {request.POST['telefono']}, mensaje = {request.POST['mensaje']}"		
		Tracking.objects.create(descripcion = descripcion)
		messages.add_message(request, messages.SUCCESS, 
			"Formulario ingresado correctamente") 

		return HttpResponseRedirect("/formularios/simple")

	return render(request, "formularios/formularioSimple.html", {})

def formulariosForm(request):
	#form = FormularioEjemplo(request.POST or None)
	
	if request.method == "POST":
		form = FormularioEjemplo(request.POST or None)
		if form.is_valid():
			data = form.cleaned_data
			descripcion = f"""El valor de nombre es {data['nombre']},
			correo = {data['correo']}, teléfono = {data['telefono']},
			mensaje = {data['mensaje']}"""
			save = Tracking()
			save.descripcion = descripcion
			save.save()			
			messages.add_message(request, messages.SUCCESS, f"Formulario ingresado correctamente")

			return HttpResponseRedirect("/formularios/form")
	else:
		form = FormularioEjemplo()
	
	return render(request, "formularios/formulariosForm.html", 
				{'form' : form,})


def formulariosLogin(request):
	form = FormularioLogin(request.POST or None)
	if request.method == "POST":
		if form.is_valid():
			#data = form.cleaned_data
			user = authenticate(request, username=request.POST['usuario'],
				password=request.POST['password'])
			if user is not None:
				login(request, user)

				usersMetadata = UsersMetadata.objects.filter(user_id=request.user.id).get()
				#variable de sesión
				request.session['users_metadata_id'] = usersMetadata.id
				return HttpResponseRedirect("/formularios/logueado")
			else:
				messages.add_message(request, messages.WARNING, 
					f"Datos incorrectos. Vuelva a intentar.")
				return HttpResponseRedirect("/formularios/login")


	return render(request, "formularios/formulariosLogin.html", {'form':form})


@logueado()
def formulariosLogueado(request):	
	return render(request, "formularios/formulariosLogueado.html", {})


def formulariosLogout(request):
	logout(request)
	#borrar variables de sesión (solo si las creamos)
	try:
		del request.session['users_metadata_id']
	except KeyError:
		pass
	#/borrar variables de sesión
	messages.add_message(request, messages.WARNING,
		"Sesión cerrada exitosamente")
	return HttpResponseRedirect("/formularios/login")
