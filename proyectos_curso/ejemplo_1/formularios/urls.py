from django.urls import path
from .views import *

urlpatterns = [
	path('', formulariosHome, name = "formulariosHome"),
	path('simple/', formularioSimple, name="formularioSimple"),
	path('form/', formulariosForm, name = "formulariosForm"),
	path('login/', formulariosLogin, name = "formulariosLogin"),
	path('logout/', formulariosLogout, name = "formulariosLogout"),
	path('logueado/', formulariosLogueado, name = "formulariosLogueado"),
]