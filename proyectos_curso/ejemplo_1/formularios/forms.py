from django import forms
from django.core import validators
from django.forms import PasswordInput

class FormularioLogin(forms.Form):
	usuario = forms.CharField(required=True, widget=forms.TextInput(
		attrs={'class':'form-control', 'placeholder':'Usuario', 
		'autocomplete':'off'}))
	password = forms.CharField(required=True, widget=forms.PasswordInput(attrs={
		'class':'form-control', 'placeholder':'Contraseña',
		'autocomplete':'off'}))

class FormularioEjemplo(forms.Form):
	nombre = forms.CharField(required = True, widget=forms.TextInput(attrs=
		{'class':'form-control', 'placeholder':'Escriba su nombre'}))
	correo = forms.CharField(required = True, 
		widget=forms.TextInput(
			attrs= {'class':'form-control', 'placeholder':'Email',}
			),
			validators = [
				validators.MinLengthValidator(4, 
					message="El texto introducido es muy corto"),
				validators.RegexValidator('^[a-z0-9]+[\._]?[a-z0-9]+[@]\w+[.]\w{2,3}$',
					message="El formato ingresado no es válido")
			],
			error_messages={'required':'El campo de correo está vacío'},
		)
	telefono = forms.CharField(required = True, widget=forms.TextInput(
		attrs= {'class':'form-control', 'placeholder':'N° telefónico', 'type':'email'}
		),
		validators=[
			validators.MinLengthValidator(4, 
				message="El texto introducido es muy corto"),
			validators.RegexValidator('^[+0-9]*$',
				message="El teléfono debe tener el formato '+56912345678'")
		],
	)
	mensaje = forms.CharField(required = True, widget=forms.Textarea(
		attrs= {'class':'form-control', 'placeholder':'Mensaje', 
			'rows': 5, 'cols': 100,}
		),
		validators=[
			validators.MinLengthValidator(4, 
				message="El texto introducido es muy corto"),
			validators.RegexValidator('^[A-Za-z0-9ñÑáéíóúÁÉÍÓÚäëïöüÄËÏÖÜ,. ]*$',
				message="El texto introducido contiene caracteres inválidos. Por favor solo use letras y números")
		],
	)
		

