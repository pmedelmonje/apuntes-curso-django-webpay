function carga_ajax_get(ruta, valor1, div) {
	$.get(ruta, { valor1: valor1 }, function(resp) {
		$("#" + div + "").html(resp);
	});
	return false;
}

function buscador(){
	if(document.getElementById('b').value == 0){
		return false;
	}
	window.location="/consultas/buscador?b="+document.getElementById('b').value+"&page=1";
}

function soloNumeros(evt) {
	key = (document.all) ? evt.keyCode : evt.which;
	//alert(key)
	if (key == 17) return false;
	//dígitos,del, sup,tab,arrows
	return ((key >= 48 && key <= 57) || key == 8 || key == 127 || 
		key == 9 || key == 0);
}

function crear_producto() {
	var form = document.form;
	if(form.file.value==0){
		form.foto.value = 'vacio';
	}
	form.submit();
}

function set_atributos() {
	if ($('input[type=checkbox]:checked').length===0) {
		//e.preventDefault();
		alert('Debe seleccionar al menos un atributo');
		return false;
	}
	document.form.submit();
}


function logout(ruta) {
	jCustomConfirm('¿Realmente quiere cerrar sesión?',
		'Ejemplo 1', 'Aceptar', 'Cancelar', function(r) {
			if (r) {
				window.location = ruta;
			}
		});
}