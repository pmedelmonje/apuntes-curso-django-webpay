# Generated by Django 4.1.1 on 2022-09-18 03:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0005_producto_foto'),
    ]

    operations = [
        migrations.CreateModel(
            name='Tracking',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('descripcion', models.TextField()),
                ('fecha', models.DateTimeField(auto_now=True)),
            ],
            options={
                'verbose_name': 'Tracking',
                'verbose_name_plural': 'Trackings',
                'db_table': 'tracking',
            },
        ),
    ]
