# Generated by Django 4.1.1 on 2022-09-26 14:47

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0010_nombre'),
    ]

    operations = [
        migrations.AlterField(
            model_name='nombre',
            name='numero',
            field=models.CharField(max_length=100, null=True),
        ),
    ]
