from django.urls import path
from .views import *

urlpatterns = [
	path('', homeStart, name="home"),
	path('aboutUs/', aboutUs, name = "aboutUs"),
]