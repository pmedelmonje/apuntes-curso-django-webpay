from django.contrib import admin
from .models import *
from utilidades import formularios

# Register your models here.


# admin.site.site_header = 'Holi'
# admin.site.index_title = 'lala'
# admin.site.site_title = 'lele'

class EstadoAdmin(admin.ModelAdmin):
	list_display = ('id', 'nombre')
	search_fields = ('nombre', )


class GeneroAdmin(admin.ModelAdmin):
	list_display = ('id', 'nombre')
	search_fields = ('nombre', )


class PaisAdmin(admin.ModelAdmin):
	list_display = ('id', 'nombre')
	search_fields = ('nombre', )


class PerfilAdmin(admin.ModelAdmin):
	list_display = ('id', 'nombre')
	search_fields = ('nombre', )


class UsersMetadataAdmin(admin.ModelAdmin):
	list_display = ('id', formularios.set_user, formularios.set_birth_date, 'telefono')
	

class CategoriaAdmin(admin.ModelAdmin):
	list_display = ('id', 'nombre')
	search_fields = ('nombre', )


class TrackingAdmin(admin.ModelAdmin):
	list_display = ('id', 'descripcion', 'fecha')
	search_fields = ('descripcion', )


class ProductoAdmin(admin.ModelAdmin):
	list_display = ('nombre', formularios.set_categoria_con_link, 
					formularios.set_date, formularios.foto_producto)
	search_fields = ('nombre', )

class MetadataAdmin(admin.ModelAdmin):
	list_display = ('description', 'keyword', 'correo', 
					'telefono', 'titulo')

class AtributoAdmin(admin.ModelAdmin):
	list_display = ('nombre', )
	search_fields = ('nombre', )


admin.site.register(Estado, EstadoAdmin)
admin.site.register(Genero, GeneroAdmin)
admin.site.register(Pais, PaisAdmin)
admin.site.register(Perfil, PerfilAdmin)
admin.site.register(UsersMetadata, UsersMetadataAdmin)
admin.site.register(Categoria, CategoriaAdmin)
admin.site.register(Producto, ProductoAdmin)
admin.site.register(Tracking, TrackingAdmin)
admin.site.register(Metadata, MetadataAdmin)
admin.site.register(Atributo, AtributoAdmin)

