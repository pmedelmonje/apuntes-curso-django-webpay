from django.db import models
from django.contrib.auth.models import User
from autoslug import AutoSlugField
from django.db.models.signals import post_save, pre_save, pre_delete
from django.dispatch import receiver

# Create your models here.

class Estado(models.Model):
	#id = models.AutoField(primary_key = True)
	nombre = models.CharField(max_length = 100)
	slug = AutoSlugField(populate_from = "nombre")

	def __str__(self):
		return self.nombre

	class Meta:
		db_table = "estado"
		verbose_name = "Estado"
		verbose_name_plural = "Estados"


class Genero(models.Model):
	nombre = models.CharField(max_length=100, null = True)

	def __str__(self):
		return self.nombre

	class Meta:
		db_table = 'genero'
		verbose_name = 'Género'
		verbose_name_plural = 'Géneros'


class Pais(models.Model):
	nombre = models.CharField(max_length=100, blank = True, null = True)

	def __str__(self):
		return self.nombre

	class Meta:
		db_table = "pais"
		verbose_name = "País"
		verbose_name_plural = "Países"


class Perfil(models.Model):
	nombre = models.CharField(max_length = 100, null = True)

	def __str__(self):
		return self.nombre

	class Meta:
		db_table = 'perfil'
		verbose_name = 'Perfil'
		verbose_name_plural = 'Perfiles'


class Metadata(models.Model):
	description = models.CharField(max_length=255, blank = True,
		null = True)
	keyword = models.TextField(default='0')
	correo = models.CharField(max_length=100, blank = True, null = True)
	telefono = models.CharField(max_length = 50, blank=True, null = True)
	titulo = models.CharField(max_length = 255, default = "dd")

	def __str__(self):
		return self.correo

	class Meta:
		db_table = "metadata"


class Categoria(models.Model):	
	nombre = models.CharField(max_length = 100)
	slug = AutoSlugField(populate_from = "nombre")

	def __str__(self):
		return self.nombre

	class Meta:
		db_table = "categoria"
		verbose_name = "Categoria"
		verbose_name_plural = "Categorias"


class Tracking(models.Model):
	descripcion = models.TextField()
	fecha = models.DateTimeField(auto_now = True)

	def __str__(self):
		return self.descripcion

	class Meta:
		db_table = "tracking"
		verbose_name = "Tracking"
		verbose_name_plural = "Trackings"


class Nombre(models.Model):
	nombre = models.CharField(max_length = 100, null = True)
	numero = models.CharField(max_length = 100, null = True)
	
	def __str__(self):
		return self.nombre

	class Meta:
		db_table = "nombre"
		verbose_name = "Nombre"
		verbose_name_plural = "Nombres"



class Atributo(models.Model):
	nombre = models.CharField(max_length = 100, null = True)

	def __str__(self):
		return self.nombre

	class Meta:
		db_table = "atributo"
		verbose_name = "Atributo"
		verbose_name_plural = "Atributos"


class Producto(models.Model):	
	categoria = models.ForeignKey(Categoria, on_delete = models.DO_NOTHING)
	nombre = models.CharField(max_length = 100)
	slug = AutoSlugField(populate_from = "nombre")
	fecha = models.DateTimeField(auto_now = True)
	descripcion = models.TextField()
	foto = models.ImageField(upload_to = "producto", default = "default.png")
	precio = models.PositiveIntegerField(default=0)

	def __str__(self):
		return self.nombre

	class Meta:
		db_table = "producto"
		verbose_name = "Producto"
		verbose_name_plural = "Productos"


@receiver(post_save, sender=Producto)
def producto_save(sender, instance, **kwargs):
	if kwargs['created']:
		Tracking.objects.create(descripcion = f"Se creó el producto {instance.id}")


@receiver(pre_save, sender=Producto)
def producto_change(sender, instance: Producto, **kwargs):
	if instance.id is None:
		pass
	else:
		previous = Producto.objects.get(id = instance.id)
		if previous.nombre != instance.nombre:
			Tracking.objects.create(descripcion = f"Se modificó el producto {instance.id}")


@receiver(pre_delete, sender = Producto)
def producto_delete(sender, instance: Producto, **kwargs):
	if instance.id is None:
		pass
	else:
		Tracking.objects.create(descripcion = f"Se eliminó el producto {instance.id}")
			


class ProductoAtributo(models.Model):
	atributo = models.ForeignKey(Atributo, models.DO_NOTHING)
	producto = models.ForeignKey(Producto, models.DO_NOTHING)

	def __str__(self):
		return self.atributo.nombre

	class Meta:
		db_table = "producto_atributo"
		verbose_name = "Producto Atributo"
		verbose_name_plural = "Productos Atributos"

class UsersMetadata(models.Model):
	user = models.ForeignKey(User, on_delete = models.DO_NOTHING)
	pais = models.ForeignKey(Pais, on_delete = models.DO_NOTHING)
	genero = models.ForeignKey(Genero, on_delete = models.DO_NOTHING)
	correo = models.CharField(max_length = 100, blank = True, null = True)
	telefono = models.CharField(max_length = 100, blank = True, null = True)
	direccion = models.CharField(max_length = 100, blank = True, null = True)
	fecha_nacimiento = models.DateField(default = '1980-01-01')

	def __str__(self):
		return f"{self.user.first_name} {self.user.last_name}"

	class Meta:
		db_table = 'users_metadata'
		verbose_name = 'User metadata'
		verbose_name_plural = 'Users metadata'


