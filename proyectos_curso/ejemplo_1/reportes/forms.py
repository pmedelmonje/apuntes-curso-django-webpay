from django import forms
from django.db.models.fields  import validators


class FormularioImportarExcel(forms.Form):
	file = forms.FileField(
		required = False,
		widget = forms.FileInput(
			attrs={'class':'ss', 'id':'file', 'data-min-file-count':'1'}
			)		
		)


class FormularioImportarTxt(forms.Form):
	file = forms.FileField(
		required = False,
		widget = forms.FileInput(
			attrs={'class':'form-control', 'id':'file', 'data-min-file-count':'1'}
			)		
		)