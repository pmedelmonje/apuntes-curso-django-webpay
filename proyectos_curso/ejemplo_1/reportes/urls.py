from django.urls import path
from .views import *

urlpatterns = [
	path('', reportesHome, name = "reportesHome"),
	path('pdf/', reportesPdf, name = "reportesPdf"),
	path('importarExcel/', reportesImportarExcel, name = "reportesImportarExcel"),
	path('importarTxt/', reportesImportarTxt, name = "reportesImportarTxt"),
	path('exportarExcel/', reportesExportarExcel, name = "reportesExportarExcel"),
	path('ejecutarExportarExcel/', ejecutarExportarExcel, name = "ejecutarExportarExcel"),
	path('clienteApi/', clienteApi, name = "clienteApi"),
]