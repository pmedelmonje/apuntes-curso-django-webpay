from django.shortcuts import render
from django.http import HttpResponse, Http404, HttpResponseRedirect
from django.conf import settings
from django.template.loader import render_to_string
from django.core.files.storage import FileSystemStorage
from django.contrib import messages
import requests
import xlrd #pip install xlrd 
import django_excel as excel #pip install django_excel
from weasyprint import HTML
import os
from .forms import *
from home.models import *

# Create your views here.

def reportesHome(request):
	return render(request, "reportes/reportesHome.html", {})


def reportesPdf(request):
	ruta = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
	base_url = settings.BASE_URL
	template = f"{ruta}/templates/layout/ajax.html"
	context = {'template' : template, 'request' : request, 'ruta' : ruta,
	'base_url' : base_url}

	html = render_to_string(f"{ruta}/templates/reportes/reportesPdf.html", context)

	response = HttpResponse(content_type="application/pdf")
	response["Content-Disposition"] = "inline; mi_pdf.pdf"
	HTML(string=html).write_pdf(response)

	return response


def reportesImportarExcel(request):
	if request.method == "POST":
		form = FormularioImportarExcel(request.POST, request.FILES)
		if form.is_valid():
			datos = form.cleaned_data
			#guardar el archivo
			myfile=request.FILES['file']
			fs=FileSystemStorage()
			filename = fs.save(f"excel/archivo.xls", myfile)
			uploaded_file_url = fs.url(filename)

			#leer el archivo
			documento = xlrd.open_workbook(settings.BASE_DIR + "/media/excel/archivo.xls")
			datos = documento.sheet_by_index(0)

			#guardar en la BD
			for i in range(datos.nrows):
				if i >= 0:
					Nombre.objects.create(numero=repr(
						datos.cell_value(i,0)).replace("'","").replace(".0",""),
					nombre = repr(datos.cell_value(i,1)).replace("'",""))

		messages.add_message(request, messages.SUCCESS, 
			"Datos importados exitosamente")

		os.remove(settings.BASE_DIR + "/media/excel/archivo.xls")

		return HttpResponseRedirect(f"/reportes/importarExcel")

	else:
		form = FormularioImportarExcel()
	return render(request, "reportes/reportesImportarExcel.html", {'form' : form})


def reportesImportarTxt(request):
	if request.method == "POST":
		form = FormularioImportarTxt(request.POST, request.FILES)	
		if form.is_valid():
			datos = form.cleaned_data
			#guardar el archivo
			myfile=request.FILES['file']
			fs=FileSystemStorage()
			filename = fs.save(f"txt/archivo.txt", myfile)
			uploaded_file_url = fs.url(filename)	
			
			archivo = open(settings.BASE_DIR + "/media/txt/archivo.txt")
			lineas = archivo.readlines()

			for linea in lineas:
				Tracking.objects.create(descripcion=linea)

			messages.add_message(request, messages.SUCCESS, 
			"Datos importados exitosamente")

			os.remove(settings.BASE_DIR + "/media/txt/archivo.txt")

			return HttpResponseRedirect("/reportes/importarTxt")

	else:
		form = FormularioImportarTxt()
	return render(request, "reportes/reportesTxt.html", {'form':form,})



def reportesExportarExcel(request):
	return render(request, "reportes/reportesExportarExcel.html", {})


def ejecutarExportarExcel(request):
	export = []
	export.append(["ID","NOMBRE","NÚMERO"])
	datos = Nombre.objects.all()
	for dato in datos:
		export.append([dato.id, dato.nombre, dato.numero])
	sheet = excel.pe.Sheet(export)
	return excel.make_response(sheet, "xlsx", file_name="archivo.xlsx")


def clienteApi(request):
	url = "http://127.0.0.1:8000/api/v1/test-request/"
	response = requests.post(url=url)
	return render(request, "reportes/clienteApi.html", 
		{"response":response})