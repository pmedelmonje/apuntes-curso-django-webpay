from django.urls import path
from .views import *

urlpatterns = [
	path('', estiloHome, name = "estiloHome"),
	path('ajax/<int:id>', estiloAjax, name = "estiloAjax"),
	path('modal/<int:id>', estiloModal, name = "estiloModal"),
]