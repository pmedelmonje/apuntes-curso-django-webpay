from django.shortcuts import render

# Create your views here.

def estiloHome(request):
	return render(request, "estilo/estiloHome.html", {})

def estiloAjax(request, id):
	return render(request, "estilo/estiloAjax.html", {'id': id})

def estiloModal(request, id):
	return render(request, "estilo/estiloModal.html", {'id': id})
