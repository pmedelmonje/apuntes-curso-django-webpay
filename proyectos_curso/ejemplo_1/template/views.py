from django.shortcuts import render

# Create your views here.

def templateHome(request):
	listas = ["uno", "dos", "tres", "cuatro", "perro", "gato", "ratón"]
	texto = "<h1>asdf</h1>"
	halcon = "falcon.png"
	color = "rojo"
	return render(request, "template/templateHome.html", 
				{"listas" : listas, "texto" : texto, 
				"halcon": halcon, "color" : color})