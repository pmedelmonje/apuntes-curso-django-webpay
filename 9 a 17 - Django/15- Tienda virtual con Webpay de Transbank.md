### 71. Creación del proyecto e implementación de template


Creamos un proyecto nuevo:

`django-admin startproject tienda`

**NOTA:** Reconfiguré el usuario "root" en MySQL para usar ese en este proyecto.

Creamos igual algunas apps iniciales para empezar a trabajar: "home", "productos", "carro" y "acceso". Las coloqué en INSTALLED_APPS.

Importamos también un archivo "conf.json". Tenemos que crear una base de datos para colocarla en el archivo. Yo le voy a poner el mismo nombre que sale en el curso: "curso_django_3_tienda".

Se incluye variables para el envío de correos electrónicos que va a ser una de las funcionalidades de la tienda, que va a utilizar el SMTP de Dreamhost.

Además, se incluye variables para configurar el entorno de pruebas de Transbank. Todo lo anterior se va a explicar más adelante. El instructor va a usar la API en vez del SDK, ya que le parece más eficiente y fácil de usar.

Modificamos el archivo "settings" con las mismas importaciones que habíamos hecho en el proyecto de ejemplo.

~~~
import os
from pathlib import Path
import json
from django.utils.html import format_html
~~~

Creamos las mismas variables que en el otro "conf.json", y además añadimos variables para las configuraciones del correo y de Webpay:

~~~
SERVER_SMTP = conf['smtp']
PUERTO_SMTP = conf['smtp_puerto']
MAIL_SALIDA = conf['email']
PASSWORD_MAIL = conf['email_password']
WEBPAY_URL = conf['webpay_url']
WEBPAY_ID = conf['webpay_id']
WEBPAY_SECRET = conf['webpay_secret']
~~~

Si trabajamos con PayPal, sería muy parecido a como se definieron las variables para Webpay.

Cambiamos también el directorio de "templates" y los parámetros de la BD para trabajar con MySQL.

`'DIRS': [os.path.join(BASE_DIR, 'templates')],`

~~~
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': conf['bd'],
        'USER': conf['user'],
        'PASSWORD': conf['password'],
        'HOST': conf['server'],
        'PORT': conf['puerto'],
        'OPTIONS': {
        'autocommit': True,
        },
    }
}

~~~

La configuración igualmente del directorio "static":

~~~
STATIC_URL = 'static/'
STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'static'),
    )
~~~

Por costumbre le sigo llamando "static", aunque el instructor le llama "assets".

Para darle estilo a la página, el instructor va a usar "Start Bootstrap" (https://startbootstrap.com). Yo igual la voy a usar para probar porque no la conocía. Tiene 2 plantillas gratis que nos van a servir: una para una página de inicio para una tienda y otra para mostrar el detalle de un producto. Descomprimo los archivos descargados y los muevo a "static".

Dentro de "templates", en el directorio "layout", vamos a copiar los archivos "flash.html" y "ajax.html". La plantilla "ajax.html" la vamos a usar para crear la vista de paso para el pago con Webpay. Le añadimos la carga de "static", el bloque CSS y el bloque JS. El archivo "base.html" se va a basar en el archivo "index.html" que viene en la plantilla de startbootstrap que descargamos.

Después de configurar inicialmente la plantilla base y la plantilla home, le creamos una ruta y una vista.

Después de echar a andar por primera vez, no ha explotado.

Vamos a colocar un botón de WhatsApp debajo del footer.

~~~
<a class="whats-app" href="https://api.whatsapp.com/send?phone=+56943408660&text=https://www.mipagina.com/ %0D%0A%0D%0AHola. Necesito información de: " target="_blank" title="Estamos disponibles. Cuéntanos en qué te podemos ayudar">
    <i class="fab fa-whatsapp my-float"></i>    
</a>
~~~

Le falta modificar algunas cosas.

No es por nada, pero implementar Telegram es más fácil.

Vamos a reducir igualmente la cantidad de productos que hay en la plantilla. Vamos a dejar uno o dos para copiar el código después.

---

### 72. Migraciones e implementación del backend

Creamos los modelos de la base de datos en la app "home". Importamos esto en el archivo:

~~~
from django.contrib.auth.models import User
from autoslug import AutoSlugField
from datetime import datetime
~~~

Como los archivos de la tienda están subidos, tomé el archivo de modelos y copié los modelos en mi archivo para agilizar el proceso. Le hice algunas modificaciones. Luego, hacemos la migración inicial.

También copiamos el directorio "templatetags" que viene en los archivos descargados. Hay algunos, por ejemplo, que formatean la fecha, muestran los productos en el carrito, calculan diferencias entre fechas, etc. **Le cambié el nombre a "templatetags.py" para no confundirme.** Entonces, en la plantilla, importamos el módulo con `{% load templatetags %}`.

Para que no nos arroje un error al cargar la plantilla, primero creamos un registro en la tabala "metadata". Actualizamos la plantilla base y cargamos los keywords en las etiquetas "meta".

También creamos un directorio para módulo de nombre "utilidades" (debe tener un archivo `__init__.py` para ser reconocido por Python como un módulo) que tendrá un archivo "utilidades.py", un archivo "dreamhost.py" y un archivo "formularios.py", los cuales contendrán las funciones que vamos a crear.

Estas son las importaciones iniciales para el archivo "utilidades.py":

~~~
from datetime import datetime, date, timedelta
import os
from os import remove
from django.conf import settings
from urllib.parse import urlparse, parse_qs
from django.core.paginator import Paginator
from django.template import Context, Template
# para los email
import smtplib 
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMETExt
# token
import jwt
import time

~~~

Copié desde los recursos del curso. Hago lo mismo con los archivos "formularios" y "dreamhost". Había también un archivo "webpay.py" que también aproveché de copiar.

Para empezar a trabajar de lleno, vamos a crear algunos registros en la base de datos. Por ejemplo, en la tabla "genero", "estado", "pais", etc.

Lo de las comunas se me hizo muy largo, así que voy a colocar algunas por mientras.

Después de haber creado algunos registros y copiado los archivos desde los recursos, creamos un usuario para trabajar (nombre de usuario: "admin@loro.com").

Creamos algunos registros para proveedores, categorías, productos, etc., desde el backend de Django, para tener para la próxima lección.

---

### 73. Programación de página de detalle de producto


Modificamos la vista "homeIndex()" para que muestre los productos.

`productos = Producto.objects.filter(estado_id=1).order_by('-id').all()[:8]`

El queryset muestra hasta los 9 primeros productos con un "estado_id" "activo", y los ordena desde el id mayor (lo que sería desde el más reciente).

Nos vamos ahora a la plantilla "homeIndex" y creamos un bucle for para mostrar los items. Le hago los retoques correspondientes. Colocamos también en las plantillas `{% load templatetags %}` para utilizar los template tags personalizados, como este:

`${{producto.precio|numberFormat}}`

**RECORDATORIO:** Las imágenes se están cargando desde una carpeta dinámica, así que tenemos que colocar la ruta con el tag "static", y luego el campo correspondiente, tal que así:

`<img class="card-img-top" src="{% static 'upload/producto/' %}{{producto.foto}}" alt="{{producto.nombre}}" />`

Ya se me había olvidado.

Ahora tenemos que configurar las rutas y vistas para los productos, dentro de la app "productos".

~~~
def productosDetalleProducto(request, id, slug):
    try:
        producto = Producto.objects.filter(pk = id, 
            slug = slug, estado_id=1).get()
    except Producto.DoesNotExist:
        raise Http404

    relacionados = Producto.objects.filter(estado_id=1, 
        producto_categoria = producto.producto_categoria_id).filter(
        estado_id=1).all()

    fotos = ProductoFotos.objects.filter(producto_id=id).all()

    contexto = {"producto":producto, "relacionados":relacionados,
                "fotos":fotos}

    return render(request, "productos/productoDetalle.html", contexto)
~~~

Falta crear la plantilla. Voy a usar la plantilla descargada para mostrar los detalles del producto. Hay que hacerle la "manito de gato" para adaptarla. El instructor le incluye distintos elementos, como un carrusel de fotos, una sección de artículos relacionados, etc. Igualmente, reemplazamos el SKU de la plantilla por la categoría.

Yo, al menos, le puse un tag if para que muestre el "precio_antes" sólo si tiene un valor mayor a 0.

Vamos a poblar la tabla "ProductosFotos" colocando varias imágenes asociadas a un producto, en el panel de administración, para probar un carrusel de Bootstrap. Subí 3 e implementé el carrusel.

**NOTA:** En los productos con una sola imagen, el carrusel no funciona y no se carga la imagen. Habría que poner un condicional que muestre el carrusel solo si un producto tiene más de una foto. Ya lo hice.

También vamos a programar para que de la opción de añadir al carrito solamente si hay stock del producto. Al no haber stock, saldrá un aviso en vez del botón. De haber stock, se mostrará un selector con tantas opciones como productos haya.

Un equivalente al `for i in range(1, cantidad)` en una plantilla es lo siguiente:

~~~
 <select class="form-control form-control-sm" id="cantidad" >

  {% with ''|center:producto.stock as range %}
  {% for _ in range %}
  <option value="{{ forloop.counter }}">
    {{ forloop.counter }}
  </option>
  {% endfor %} 
  {% endwith %}
</select>
~~~

Hay unos botones que pone el instructor con estilos propios: un botón propio de "añadir al carrito" y un botón de "pedir por WhatsApp". Este último siempre aparece, aunque no haya stock del producto, así que en la plantilla está fuera de la condiciónal if.

El botón de agregar al carro tiene una función JS que se va a explicar después.

**OTRA NOTA:** Entre los productos relacionados, se muestra el mismo producto que estamos cargando en la plantilla. Lo hice así y me funcionó:

~~~
{% if relacionado.id != producto.id %}
(el código HTML del producto)
{% endif %}
~~~

---

### 74. Login, registro y envío de mail de verificación, restablecer contraseña

Trabajamos ahora con la app "acceso". Descomentamos la ruta en el archivo "urls" principal y creamos un archivo "urls.py" y "forms.py" dentro del módulo. Hacemos las siguientes importaciones en el archivo "views":

~~~
from django.shortcuts import render
from django.http import Http404, HttpResponseRedirect, HttpResponse
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.contrib.auth.hashers import make_password
from django.conf import settings
from datetime import datetime, date, timedelta
from utilidades import utilidades
import time
from home.models import *
from .forms import *
~~~

También creamos un directorio en "templates" para la app de acceso y una plantilla "accesoHome.html". Lo típico de siempre.

Además, en la plantilla base, vamos a colocar una validación para cuando un usuario esté autenticado, para que el link del login se muestre solamente cuando no se ha iniciado sesión.

~~~
{% if user.is_authenticated %}
<li class="nav-item"><a class="nav-link" href="#">Mi perfil</a></li>
<li class="nav-item"><a class="nav-link" href="#">Salir</a></li>
{% else %}
<li class="nav-item"><a class="nav-link" href="{% url 'accesoLogin' %}">Login</a></li> 
<li class="nav-item"><a class="nav-link" href="#">Registrarse</a></li>  
{% endif %} 
~~~

Para hacer pruebas, creamos un registro en el modelo UsersMetadata. Luego, creamos una ruta para el registro y una para salir (pendiente).

La vista de salir es igual a la que se usó en la sección de formularios.

~~~
def accesoSalir(request):
    logout(request)
    try:
        del request.session['users_metadata_id']
    except KeyError:
        pass
    messages.add_message(request, messages.WARNING, 
        "Sesión cerrada exitosamente")

    return HttpResponseRedirect("acceso/login")
~~~

En la plantilla, colocamos el link así para llamar una función de JavaScript que aplica un alert de jQuery:

`<li class="nav-item"><a class="nav-link" href="javascript:void(0);" onclick="salir('{% url 'accesoSalir' %}')">Salir</a></li>`


Puse también un `{% include "layout/flash.html" %}` en la plantilla base.


Para el archivo "forms" importamos lo siguiente:

~~~
from django.db import models
from django import forms
from django.forms import ModelForm, PasswordInput
~~~

El formulario de login

~~~
class FormularioLogin(forms.Form):
    correo = forms.EmailField(required=True, widget=forms.TextInput(
        attrs= {'class':'form-control', "placeholder":"E-Mail", 
        "autocomplete":"off"}))
    password = forms.CharField(widget={'class':'form-control',
        'placeholder':'Contraseña', 'autocomplete':'off'})
~~~

El formulario de registro

~~~
class FormularioRegistro(forms.Form):
    nombre = forms.CharField(required=True, widget=forms.TextInput(attrs={
        'class': 'form-control', 'placeholder': 'Nombre', 'autocomplete':'off'}))
    apellido = forms.CharField(required=False, widget=forms.TextInput(attrs={
        'class': 'form-control', 'placeholder': 'Apellido', 'autocomplete':'off'}))
    correo = forms.CharField(required=True, widget=forms.TextInput(attrs={
        'class': 'form-control', 'placeholder': 'E-Mail', 'autocomplete':'off'}))
    password = forms.CharField(widget=PasswordInput(attrs={
        'class': 'form-control', 'placeholder': 'Contraseña', 'autocomplete':'off'}))
    password2 = forms.CharField(widget=PasswordInput(attrs={
        'class': 'form-control', 'placeholder': 'Repetir Contraseña', 'autocomplete':'off'}))
~~~

Creamos un formulario con clases de Bootstrap en la plantilla "login". Luego, escribimos el código de la vista de otra forma válida:

~~~
def accesoLogin(request):
    if request.user.is_authenticated:
        return HttpResponseRedirect("/home")
    form = FormularioLogin(request.POST or None)

    if request.method == "POST":
        if form.is_valid():
            correo = request.POST['correo']
            password = request.POST['password']

            user = authenticate(request, username=correo,
                password=password)

            if user is not None:
                login(request, user)
                usersMetadata = UsersMetadata.objects.filter(
                    user_id = request.user.id).get()
                request.session['users_metadata_id'] = usersMetadata.id
                return HttpResponseRedirect('/')
            else:
                messages.add_message(request, messages.WARNING, 
                    "Correo y/o contraseña inválido(s). Por favor reintentar")
                return HttpResponseRedirect("/acceso/login")

    return render(request, "acceso/accesoLogin.html", {'form':form})
~~~

La diferencia es que al principio se valida si el usuario ha iniciado sesión, para redirigir antes de continuar con el resto del código de la vista, y así no ocurra que al usuario le salga el formulario de login habiendo ya iniciado sesión.

Creamos ahora la plantilla para el registro. Estoy usando la misma que está en los archivos. La vista utiliza la tabla "auth_user" que crea Django, y deja el valor "is_active" en 0, lo que deja al usuario recién creado inactivo para operar, como medida de seguridad. El código, al momento de registrar un usuario, envía un correo con un token de activación de la cuenta (debe haber configurada una cuenta de correo en el archivo "conf.json" para que funcione). **Nota:** A veces ocurre que banean las direcciones Gmail, así que lo ideal es no usar una cuenta "principal". También hay que instalar "python-slugify" con pip.

---
**Django no me está reconociendo el archivo "settings", así que no me funcionan las constantes por el momento. Lo del envío de correos va a quedar pendiente hasta encontrar una solución. Lo raro es que el método "getToken()" funciona bien, y también recoge datos desde el archivo "settings". Además, me falta un servidor de correo que no tenga problemas de autorización. Gmail y Yahoo no funcionan. Me falta probar con Outlook.**

---

Se incluye también la vista para la verificación, que tiene un método para traducir el token y cambia el estado del usuario creado a "activo".

**Nota:** Los usuarios creados de esta manera no tienen acceso al panel de administración, porque el campo "is_staff" está en 0. Como medida de seguridad nos puede servir, para que no cualquiera pueda entrar al panel de administración. Igualmente podemos cambiar la ruta en "urls", para que no sea tan "fácil de intuir".

También el instructor crea una vista para recuperar la contraseña y una para restablecerla.

---

### 75. Programación de carrito, checkout y página de pago

El instructor, en phpmyadmin, muestra solamente las tablas que se van a utilizar para la gestión del carrito, que son "carrito", "producto", "orden_de_compra" y "orden_de_compra_detalle". 

Hay muchas formas de persistir la información, en este caso, del carro de compra. Una es con cookies de JavaScript, que le permiten al usuario volver a abrir el navegador y ver el carrito tal como lo había dejado; pero eso tiene desventajas, como que no va a funcionar si ese usuario accede desde otro dispositivo o navegador. Para efectos del curso, en vez de con cookies, el instructor ha creado una tabla "carrito" para guardar los productos.

Descomentamos la ruta en "urls" para la app de "carro", creamos un archivo "urls.py" dentro del directorio de esa app. Dentro de "views" importamos:

~~~
from django.shortcuts import render
from django.http import Http404, HttpResponseRedirect,
    HttpResponse
from django.contrib import messages
from home.models import *
from acceso.decorators import logueado
from utilidades import utilidades
import json
from django.conf import settings
~~~

Copié también las vistas para la página de inicio y la creación del carro. Ambas utilizan el decorador "@logueado", para que requieran que un usuario haya iniciado sesión para verlas.

Hay que copiar el archivo "decorators.py" dentro del directorio de "acceso".

Creamos inicialmente una ruta principal para el carro y una para crear los valores en el carro. Creamos también un directorio "carro" dentro de "templates" y un archivo "carroHome.html". La plantilla va a tener una tabla con los productos agregados para confirmar la compra. La plantilla de detalle del producto tiene un formulario oculto que llama a una url para añadir el producto al carro. Lo tenía comentado porque estaba dando errores, y ahora lo descomenté.

El botón de agregar al carro llama a una función JS "agregarAlCarro()" que está en "scripts.js", que śe apropia del formulario, toma el valor seleccionado de la cantidad, y envía esa información hacia la vista "carroCrear". Luego, esa vista recibe esa información, crea un registro en la tabla de carro, y luego redirige hacia la ruta del carro.

Copié las otras rutas y vistas que faltaban, para seguir más fluidamente. Quedé hasta la parte donde se agrega productos al carro y se carga luego esa página. Falta programar el botón de carro que está en la plantilla base, y que el "pill" muestre la cantidad de productos.

Se usa un templatetag que muestra cuántos items hay en el carro.

Se puede buscar "snippet bootstrap" en Google para buscar, por ejemplo, un checkout hecho en Bootstrap, y así tener componentes vistosos rápidamente. Hay muchas páginas que ofrecen estos componentes.

Acorté un poco el botón del carro, quitándole la palabra "items", para que no ocupe tanto espacio.


---

### 76. Integración con Webpay de Transbank

