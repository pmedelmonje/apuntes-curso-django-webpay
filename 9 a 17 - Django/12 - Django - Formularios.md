## Django - Formularios

---

### 57. Formularios simples

Creamos una app nueva de nombre "formularios". Le ponemos un path en el archivo base de urls, le creamos un directorio para las plantillas, la incluimos en "settings.py", etc.

Creamos una plantilla "formulariosHome.html" y una plantilla "formularioSimple.html"; una ruta y una vista para cada una. El archivo "formulariosHome" tendrá links hacia cada una de las plantillas de las siguientes clases.

Empezamos con la plantilla "formulariosSimples.html". Colocamos un formulario simple de Bootstrap para simular un formulario de contacto:

~~~
<form name="form" method="post" action="{% url 'formularioSimple' %}">
	{% csrf_token %}
	<div class="mb-3">
		<label for="nombre" class="form-label">Nombre</label>
		<input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre">
	</div>
	<div class="mb-3">
		<label for="correo" class="form-label">Dirección de Email</label>
		<input type="email" class="form-control" id="correo" name="correo" placeholder="nombre@correo.com">
	</div>
	<div class="mb-3">
		<label for="telefono" class="form-label">Teléfono</label>
		<input type="text" class="form-control" id="telefono" name="telefono" placeholder="Teléfono">
	</div>
	<div class="mb-3">
		<label for="mensaje" class="form-label">Mensaje</label>
		<textarea class="form-control" id="mensaje" name="mensaje" rows="3"></textarea>
	</div>
	<hr>
		<a href="javascript:void(0);" class="btn btn-success" onclick="document.form.submit()">
		Enviar
		</a>
	</div>						
</form>
~~~

Está configurado el formulario para que se redirija a la misma plantilla al momento de ingresar los datos. `{% csrf_token %}` crea un campo oculto llamado CSRF, que sirve para prevenir ataques de hackers. No se puede enviar un formulario si no está ese tag.

Lo que recomienda el instructor es colocar, en el archivo base, lo siguiente:

`<meta name="csrf-token" content="{{ csrf_token }}">`

Esto puede prevenir problemas con la caché del navegador, especialmente donde hay archivos CSS y JS sujetos a cambio. Hacer lo mismo en esos archivos. Por ejemplo, en el archivo "funciones.js", que estamos constantemente modificando:

`<script type="text/javascript" src="{% static 'js/funciones.js' %}?id={{ csrf_token }}"></script>`

Igualmente vamos a importar la librería "messages" para mostrar mensajes "flash" en la plantilla al completarse determinada acción; en este caso, el envío de un formulario. También vamos a importar desde django.http la librería "HttpResponseRedirect":

~~~
from django.http import HttpResponse, Http404, HttpResponseRedirect
from django.contrib import messages
~~~

Modificamos la vista para que procese el formulario y nos muestre el nombre que ingresamos, solo para probar cómo funciona: 

~~~
def formularioSimple(request):
	if request.method == "POST":
		return HttpResponse(f"el valor de nombre es {request.POST['nombre']}")

	return render(request, "formularios/formularioSimple.html", {})
~~~

Probamos ahora creando un registro en Tracking:

~~~
def formularioSimple(request):
	if request.method == "POST":		
		descripcion = f"El nombre es {request.POST['nombre']}, correo = {request.POST['correo']}, correo = {request.POST['telefono']}, mensaje = {request.POST['mensaje']}"		
		Tracking.objects.create(descripcion = descripcion) 

	return render(request, "formularios/formularioSimple.html", {})
~~~

Normalmente cuando se envía un formulario, tenemos que sacar al usuario de esa página. Colocamos un return dentro del condicional. Por ejemplo:

~~~
def formularioSimple(request):
	if request.method == "POST":		
		descripcion = f"El nombre es {request.POST['nombre']}, correo = {request.POST['correo']}, correo = {request.POST['telefono']}, mensaje = {request.POST['mensaje']}"		
		Tracking.objects.create(descripcion = descripcion) 

		return HttpResponseRedirect("/formularios/simple")

	return render(request, "formularios/formularioSimple.html", {})

~~~

Hay reglas de validación para los campos que se pueden hacer con JavaScript, pero Django también ofrece algunas opciones.

Usamos el módulo "messages" cuando queremos mostrar un mensaje "set flash". Básicamente crea una cookie que dura 1 segundo. Vamos a usar en el ejemplo un alert de Bootstrap. Añadimos al condicional de la vista el código para que muestre un mensaje de éxito:

~~~
messages.add_message(request, messages.SUCCESS, 
			"Formulario ingresado correctamente") 
~~~

El primer parámetro es "request"; el segundo es el tipo de mensaje (en este caso, uno de éxito); y el tercero es el texto del mensaje.

El panel de administración de Django está programado para mostrar los mensajes flash, pero si queremos mostrarlos en nuestras plantillas, tenemos que agregar código adicional. Básicamente pueden llegar dos tipos de mensajes: mensajes de error y mensajes de éxito. Una buena opción es crear un archivo "flash.html" en el directorio "layout", porque podemos reutilizar un código de ese tipo:

~~~
{% if form.errors %}
<div class="alert alert-danger alert-dismissible fade show" role="alert">
	<div class="container">
		<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="close"></button>			
		<p>Hay errores en el formulario: {{form.errors}}</p>
	</div>	
</div>
{% endif %}
{% if messages %}
{% for message in messages %}
<div class="alert alert-{{message.level_tag}} alert-dismissible fade show" role="alert">
	<div class="container">
		<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="close"></button>			
		{{message.message}}
	</div>	
</div>
	{% endfor %}
{% endif %}

~~~

De hecho, si trabajamos con Bootstrap, podríamos usar este código en cualquier proyecto donde usemos la misma versión de Bootstrap. La primera sección se ejecuta cuando hay un error en el formulario. La segunda sección, cuando se ha enviado el formulario correctamente.

Podría usarse librerías como Sweetalert, pero ese tipo de alertas me parece lo suficientemente práctico.

---

### 58. Formularios de clases y validaciones

Vamos a crear una vista para probar los formularios de Django:

~~~
def formulariosForm(request):
	return render(request, "formularios/formulariosForm.html", {})
~~~

Le creamos la ruta correspondiente y una plantilla con el mismo código, inicialmente, que en la plantilla de formulario simple. La idea es que procesemos el formulario con clases. En el directorio de la app "formularios" creamos un archivo de nombre "forms.py" (siempre debe tener ese nombre). Dentro del archivo creamos una clase para manejar el formulario. Importamos el módulo "forms":

`from django import forms`

Hasta ahora, hemos utilizado la app "home" para gestionar el panel de administración, crear modelos, etc., pero con respecto a los formularios, se recomienda tenerlos en la app donde se van a utilizar.

Para empezar, creamos un formulario con un solo campo (al ser clases, hay que acostumbrarse a poner la primera letra en mayúscula):

~~~
class FormularioEjemplo(forms.Form):
	nombre = forms.CharField(required = True)
~~~

Lo cargamos en la vista:

`from .forms import *`

~~~
def formulariosForm(request):
	form = FormularioEjemplo(request.POST or None)
	#form = FormularioEjemplo()
	return render(request, "formularios/formulariosForm.html", {'form' : form,})
~~~

Cuando colocamos como parámetro el método POST, debe estar informado de antes que se va a hacer una petición POST. Si no se hace, esa forma puede fallar. Una opción es declarar la variable con el formulario sin parámetros.

Para incrustar el formulario en la plantilla, lo colocamos en un tag:

~~~
<div class="mb-3">
	<label for="nombre" class="form-label">Nombre</label>
	{{form.nombre}}
</div>
~~~

De ese modo, no se le puede cargar la clase dentro de la plantilla, así que tenemos que aplicarle los estilos de otro modo. Una de las formas que muestra el instructor es aplicarle un widget dentro del atributo de la clase:

~~~
class FormularioEjemplo(forms.Form):
	nombre = forms.CharField(required = True, widget=forms.TextInput(attrs=
		{'class':'form-control'}))
~~~

Dentro del diccionario, podemos pasarle más parámetros si queremos; por ejemplo un placeholder, desactivar el autocompletado, etc.

Creamos ahora, dentro de la clase, el campo para el correo:

~~~
correo = forms.CharField(required = True, widget=forms.TextInput(attrs=
		{'class':'form-control', 'placeholder':'Email'}))
~~~

El instructor lo hace con un CharField, pero yo voy a probar con el EmailField. 

~~~
correo = forms.EmailField(required = True, widget=forms.EmailInput(attrs=
		{'class':'form-control', 'placeholder':'Email'}))
~~~

Nota: El widget para un EmailField es "EmailInput". Me funciona, pero hay que colocar un validador para que lo reconozca como correo, o puede colocarse en "attrs" una clave:valor "'type':'email'".

Creamos campos para el resto de los datos:

~~~
telefono = forms.CharField(required = True, widget=forms.TextInput(attrs=
		{'class':'form-control', 'placeholder':'N° telefónico'}))
mensaje = forms.CharField(required = True, widget=forms.Textarea(attrs=
		{'class':'form-control', 'placeholder':'Mensaje', 'rows': 5s, 'cols': 100,}))
~~~

Yo había visto en otro tutorial que podemos colocar en el texto del label un tag con el campo correspondiente. Por ejemplo, en vez de escribir "Nombre", podemos colocar `{{form.nombre.label}}`.

~~~
<div class="mb-3">
	<label for="nombre" class="form-label">{{form.nombre.label}}</label>
	{{form.nombre}}
</div>
~~~

Vamos ahora a procesar el formulario dentro de la vista. Antes, vamos a integrar el paquete de validación que tiene Django, dentro del archivo de formularios:

`from django.core import validators`

Vamos a agregar un validador al campo de correo. Podríamos enviar mensajes de error personalizados si queremos, utilizando un diccionario de nombre "error_messages" (este no requiere la importación de un paquete).

~~~
correo = forms.CharField(required = True, 
		widget=forms.TextInput(
			attrs= {'class':'form-control', 'placeholder':'Email',
			'type':'email'}
			),
			validators = [],
			error_messages={'required':'El campo de correo está vacío'},
		)
~~~

Ese es un ejemplo. Aún no está listo el código, pero por el momento funciona.

Cuando tenemos un error, muestra todos los errores de una vez. Igualmente podemos mostrar errores "sueltos". Por ejemplo, en el input del correo dentro de la plantilla, podríamos poner debajo del tag {{form.correo}}:

`{{form.errors.correo}}`

La idea es colocarlo en un tag "if", porque si no, va a aparecer siempre que se envíe el formulario, aunque no haya errores.

~~~
<div class="mb-3">
	<label for="correo" class="form-label">{{form.correo.label}}</label>
	{{form.correo}}
	{% if form.errors %}
		<span class="text-danger">{{form.errors.correo}}</span>
	{% endif %}
</div>
~~~

El "span" se lo puse yo. Era para probar si podía aplicarle estilos al texto de error, y me resultó.

Ocuparemos ahora el "validators". Podremos una validación dentro del mismo campo de correo. Tenemos que quitar de la constante "attrs" el "type":"email" para que funcione:

~~~
validators = [
	validators.MinLengthValidator(4, message="El texto introducido es muy corto"),
	validators.RegexValidator('^[a-z0-9]+[\._]?[a-z0-9]+[@]\w+[.]\w{2,3}$',
	message="El formato ingresado no es válido")
],
~~~

Incluye un validador de largo mínimo de texto en el campo, y también un validador con expresiones regulares para los caracteres típicos de una dirección de email.

Vamos a colocar ahora un validator para el mensaje:

~~~
validators=[
	validators.MinLengthValidator(4, 
		message="El texto introducido es muy corto"),
	validators.RegexValidator('^[A-Za-z0-9ñÑáéíóúÁÉÍÓÚäëïöüÄËÏÖÜ,. ]*$',
		message="El texto introducido contiene caracteres inválidos. Por favor solo use letras y números")
],
~~~

Tiene igualmente un validador de largo mínimo de texto, y también tiene un RegexValidator que previene inyecciones de código, que va a permitir solamente letras y números en el texto.

Se incluye uno también para el teléfono, para que solo tenga números:

~~~
validators=[
	validators.MinLengthValidator(4, 
		message="El texto introducido es muy corto"),
	validators.RegexValidator('^[+0-9]*$',
		message="El teléfono debe tener el formato '+56999999999'")
],
~~~

Completamos ahora la validación del formulario en la vista. Se hace de una forma muy parecida al formulario simple, pero Django, cuando se está usando formularios con clases, recomienda utilizar una variable que tenga como valor "form.cleaned_data". El método .cleaned_data toma los datos del formulario y los vuelve a "limpiar". Entonces, cuando queramos usar los datos, en vez de usar `request.POST['campo']`, usamos `variable['campo']`.


La vista queda así:

~~~
def formulariosForm(request):
	#form = FormularioEjemplo(request.POST or None)
	
	if request.method == "POST":
		form = FormularioEjemplo(request.POST or None)
		if form.is_valid():
			data = form.cleaned_data
			descripcion = f"""El valor de nombre es {data['nombre']},
			correo = {data['correo']}, teléfono = {data['telefono']},
			mensaje = {data['mensaje']}"""
			Tracking.objects.create(descripcion = descripcion)
			messages.add_message(request, messages.SUCCESS, f"Formulario ingresado correctamente")

			return HttpResponseRedirect("/formularios/form")
	else:
		form = FormularioEjemplo()
	
	return render(request, "formularios/formulariosForm.html", 
				{'form' : form,})
~~~

Otra forma sería:

~~~
if form.is_valid():
	data = form.cleaned_data
		descripcion = f"""El valor de nombre es {data['nombre']},
		correo = {data['correo']}, teléfono = {data['telefono']},
		mensaje = {data['mensaje']}"""
		save = Tracking()
		save.descripcion = descripcion
		save.save()			
		messages.add_message(request, messages.SUCCESS, f"Formulario ingresado correctamente")
~~~

---

Hay una diferencia entre hacer validaciones de lado del cliente, como las que se hacen con JavaScripts, y las que se hacen de lado de servidor, con Python, PHP, C# con .Net, etc. Las validaciones de lado del cliente están expuestas a inyecciones "Cross-Site Scripting" (XSS), que destruyen (se saltan) las validaciones e introducen código malicioso. El gran problema con los frameworks frontend es que las validaciones que se hacen por ahí pueden ser burladas por alguien que sepa usar bien JavaScript, así que igualmente hay que tener reglas de validación en el backend (como en las APIs) para sanitizar los datos que nos llegan.


---

### 59. Upload de archivos

Trabajamos en el modelo "Producto". Volvemos a la plantilla de consultas para crear un botón para añadir un producto. También creamos una ruta nueva dentro de la app "consultas" para una vista y plantilla de creación de un registro.

`path('crear/', consultasCrear, name = "consultasCrear"),`

`def consultasCrear(request):
	return render(request, "consultas/consultasCrear.html", {})`

En la plantilla "consultasCrear" vamos a colocar todos los componentes para crear un producto. Creamos dentro de la app "consultas" un archivo "forms.py".

El instructor ofrece una función JavaScript para que solo se pueda ingresar números en el campo (está en "funciones.js"). La colocamos dentro de los attrs del campo precio:

~~~
class FormularioProducto(forms.Form):
	nombre = forms.CharField(required = True, widget=forms.TextInput(attrs=
	{'class':'form-control', 'placeholder':'Nombre', 'autocomplete':'off'}))
	precio = forms.CharField(required = True, widget=forms.TextInput(attrs=
	{'class':'form-control', 'placeholder':'Precio', 'onkeypress':'return soloNumeros(event)'}))
	descripcion = forms.CharField(required = True, widget=forms.Textarea(attrs=
	{'class':'form-control', 'placeholder':'Descripción', 'rows':3, 'columns':100, 'autocomplete':'off'}))
~~~

Probamos hasta aquí colocando el formulario dentro de la vista.

~~~
def consultasCrear(request):

	if request.method == "POST":
		form = FormularioProducto(request.POST, request.FILES)
	else:
		form = FormularioProducto(request.POST)

	return render(request, "consultas/consultasCrear.html", {'form':form,})
~~~

Falta ahora colocar el campo de la categoría, que se hace de forma distinta por ser un tipo "select", y que va a traer datos de una tabla. En el archivo "formularios.py" creamos el siguiente método:

~~~
def get_categorias_choices():
	return [(value.pk, value.nombre) for value in Categoria.objects.all()]
~~~

Para que funcione, en el archivo "forms" tenemos que importar:

`from utilidades import *`

El campo categoría queda así:

~~~
categoria = forms.ChoiceField(required = True, widget =forms.Select(
		attrs={'class':'form-control',}), choices=formularios.get_categorias_choices
	)
~~~

Con lo anterior, el selector de las categorías funciona.

Para manejar imágenes, el instructor usa una librería de Bootstrap, que hay que buscar como "bootstrap fileinput". Se copia el archivo "fileinput.js" y "fileinput.css" dentro de static. Bootstrap, de todos modos, tiene unos inputs para trabajar con archivos, pero por ahora vamos a usar el "fileinput". Se importan en la plantilla utilizando un bloque para cada rchivo.

~~~
{% block css %}	
<link rel="stylesheet" type="text/css" href="{% static 'lib/bootstrap-fileinput/fileinput.css' %}">
{% endblock %}

{% block js %}
<script type="text/javascript" src="{% static 'lib/bootstrap-fileinput/fileinput.js' %}"></script>
{% endblock %}
~~~

Al menos así, al cargar la plantilla los carga correctamente. Creamos ahora el campo del formulario en "forms". Lo vamos a hacer de la forma en que la librería pide que se haga:

~~~
file = forms.CharField(required=True, widget=forms.TextInput(
		attrs={'type':'file', 'id':'file-0a', 'class':'file'}))
~~~

Colocamos en la plantilla el input correspondiente:

~~~
<div class="mb-3">
	<label for="file" class="form-label">Imagen</label>
	{{form.file}}
</div>
~~~

Al instructor al menos no le muestra el diseño que ofrece la librería, así que el instructor lo va a dejar para después. Podemos probar, por mientras, el que trae Bootstrap, cambiando la clase por "form-control", y el id por "formFile". De todos modos, a mí al menos me funcionó.

Cuando tenemos un campo en el formulario para subir archivos en el servidor, tenemos que incluir un atributo en la etiqueta del formulario:

`enctype="multipart/form-data"`

Eso informa al servidor de que el formulario está habilitado para que el usuario pueda compartir archivos desde su PC hacia el formulario. No necesariamente hacia el servidor, ya que podríamos querer otro tipo de funcionalidades en algún momento.

Vamos a colocar una funcionalidad de validación de JavaScript, que vamos a crear en nuestro archivo de funciones. Lo que va a hacer esta función va a hacer que tomemos una decisión si no se carga la imagen.

~~~
function crear_producto() {
	var form = document.form;
	if(form.file.value==0){
		form.foto.value = 'vacío';
	}
	form.submit();
}
~~~

Para que funcione, vamos a crear un input oculto dentro del formulario:

`<input type="hidden" name="foto" id="foto" />`

Lo que va a ocurrir es que, si no se carga la imagen, se le dará el valor "vacío". Esto es para que Django no tenga que hacer un "upload" si es que el campo está vacío. 

Cambiamos el "onclick" del link de submit por esa función de JS.

Importamos en la vista:

~~~
from django.core.files.storage import FileSystemStorage
from datetime import datetime, date, timedelta
import os
~~~

La vista, por el momento, va así:

~~~
def consultasCrear(request):

	if request.method == "POST":
		form = FormularioProducto(request.POST, request.FILES)
		if request.POST['foto']=='vacío':
			foto = "default.png"
		else:
			pass
	else:
		form = FormularioProducto(request.POST)

	return render(request, "consultas/consultasCrear.html", {'form':form,})
~~~

Se implementa una condicional que revisa si el valor de "foto" es vacío, para que de ser así, se defina como imagen por defecto "default.png". Falta la otra parte del código.

También ofrece 2 métodos más que nos van a servir en cualquier proyecto, que se incluyen en el archivo "utilidades", y que requieren la importación de la librería "os" en ese archivo.

~~~
def getExtension(file):
	extension = os.path.splitext(str(file))[1]
	if extension == ".png":
		return True
	elif extension == ".jpg":
		return True
	elif extension == ".jpeg":
		return True
	elif extension == ".JPG":
		return True
	elif extension == ".PNG":
		return True
	elif extension == ".JPEG":
		return True
	else:
		return False

def getExtensionSoloPdf(file):
	extension = os.path.splitext(str(file))[1]
	if extension == ".pdf":
		return True
	else:
		return False	
~~~

Los métodos revisan la extensión de un archivo. En el primer caso, archivos de formato de imagen; y en el segundo caso, archivos PDF (por si en algún momento queremos habilitar una subida de archivos PDF).

No está funcionando la subida de archivos. Sale error "el campo es obligatorio".

Voy a seguir con el resto, a ver si se resuelve de ese modo.

Actualizamos la vista:

~~~
def consultasCrear(request):

	if request.method == "POST":
		form = FormularioProducto(request.POST, request.FILES)
		if request.POST['foto'] == 'vacio':
			foto = "default.png"
		else:
			myfile = request.FILES['file']
			if utilidades.getExtension(request.FILES['file']):
				fs = FileSystemStorage()
				fecha = datetime.now()
				foto = f"{datetime.timestamp(fecha)}{os.path.splitext(str(request.FILES['file']))[1]}"
				filename = fs.save(f"producto/{foto}", myfile)
				uploaded_file_url = fs.url(filename)
			else:
				mensaje = "Formato no válido. Debe ser un archivo jpg, jpeg, png o gif."
				messages.add_message(request, messages.WARNING, mensaje)
				return HttpResponseRedirect('/consultas/crear')
	else:
		form = FormularioProducto()

	return render(request, "consultas/consultasCrear.html", {'form':form,})
~~~

Creamos otra función en el archivo "dreamhost". No es obligatorio, pero va a servir cuando utilicemos servicios como Dreamhost, donde se tiene que subir las imágenes en directorios diferentes:

~~~
def moverArchivoProducto2(file):
		shutil.move(f"{RUTA}ejemplo_1/media/producto/{file}", f"{RUTA2}static/uploads/producto/{file}")
~~~

Funciona lo de mover el archivo, pero sigue hinchando con que "el campo es obligatorio". (Se resolvía cambiando el "required" a False...)

Terminamos de escribir la vista:

~~~
def consultasCrear(request):

	if request.method == "POST":
		form = FormularioProducto(request.POST, request.FILES)
		if request.POST['foto'] == 'vacio':
			foto = "default.png"
		else:
			myfile = request.FILES['file']
			if utilidades.getExtension(request.FILES['file']):
				fs = FileSystemStorage()
				fecha = datetime.now()
				foto = f"{datetime.timestamp(fecha)}{os.path.splitext(str(request.FILES['file']))[1]}"
				filename = fs.save(f"producto/{foto}", myfile)
				uploaded_file_url = fs.url(filename)
				dreamhost.moverArchivoProducto2(foto)
			else:
				mensaje = "Formato no válido. Debe ser un archivo jpg, jpeg, png o gif."
				messages.add_message(request, messages.WARNING, mensaje)
				return HttpResponseRedirect('/consultas/crear')

		Producto.objects.create(nombre = request.POST['nombre'], 
			precio = request.POST['precio'], descripcion = request.POST['descripcion'],
			foto = foto, categoria_id = request.POST['categoria'])
		messages.add_message(request, messages.SUCCESS, 
			f"El Producto {request.POST['nombre']} se ha ingresado correctamente")
	else:
		form = FormularioProducto()

	return render(request, "consultas/consultasCrear.html", {'form':form,})
~~~

Django tiene funcionalidades para recorrer ciertos directorios para encontrar archivos, pero hay servidores donde hay que tener determinados archivos en un lado y otros en otro, y estas funcionalidades de Django no van a servir. Por eso se recomienda acostumrbarse a las funciones de Dreamhost que hemos visto hasta ahora.

---

### 60. Manejo de checkbox

Vamos al archivo "models" y creamos un modelo de nombre "Atributo":

~~~
class Atributo(models.Model):
	nombre = models.CharField(max_length = 100, null = True)

	def __str__(self):
		return self.nombre

	class Meta:
		db_table = "atributo"
		verbose_name = "Atributo"
		verbose_name_plural = "Atributos"
~~~

Otra tabla, de nombre "ProductoAtributo":

~~~
class ProductoAtributo(models.Model):
	atributo = models.ForeignKey(Atributo, models.DO_NOTHING)
	producto = models.ForeignKey(Producto, models.DO_NOTHING)

	def __str__(self):
		return self.atributo.nombre

	class Meta:
		db_table = "producto_atributo"
		verbose_name = "Producto Atributo"
		verbose_name_plural = "Productos Atributos"
~~~

Hacemos las migraciones y creamos algunos registros en la tabla "atributo", en phpMyAdmin. Por ejemplo, "grande", "pequeño", "mediano", etc. Igualmente voy a colocar el modelo en el panel de administración.

La idea con esto es que, cuando vayamos a crear un producto, nos salgan para elegir los atributos que hemos creado. Hay varias formas. Una opción sería crear un formulario nuevo para crear un atributo y asignarlo al producto, por medio de la clave foránea. Otra forma es intervenir el formulario de nuevo producto colocando los atributos ahí.

Vamos a hacerlo con un botón que lleve a un formulario nuevo. Dentro del listado de productos, vamos a colocar una columna para los atributos.

Creamos una ruta para los atributos nuevos, su vista y plantilla correspondientes.

La ruta: 

`path('agregarAtributo/<int:id>', consultasAgregarAtributo, name = "agregarAtributo"),`

La vista para empezar:

~~~
def consultasAgregarAtributo(request, id):
	try:
		producto = Producto.objects.get(pk = id)
	except Producto.DoesNotExist:
		raise Http404

	return render(request, "consultas/agregarAtributo.html", {})
~~~

Ahora creamos el formulario:

~~~
class FormularioAtributo(forms.Form):
	atributos = forms.CharField(required = True, widget=
		forms.CheckboxSelectMultiple(attrs={'class':'form-check-input'},
		choices = Atributo.objects.all().values_list('id','nombre')))
~~~

Este formulario tiene un widget de selección, y los elementos los va a cargar en una lista, utilizando el método ".values_list()", dentro del cual van, como parámetros, los campos que queremos mostrar. Tenemos que importar el archivo de modelos dentro de "forms" para que funcione.

Añadimos en la vista el procesamiento del método POST:

~~~
def consultasAgregarAtributo(request, id):
	try:
		producto = Producto.objects.get(pk = id)
	except Producto.DoesNotExist:
		raise Http404

	if request.method == "POST":
		form = FormularioAtributo(request.POST)
	else:
		form = FormularioAtributo()

	return render(request, "consultas/agregarAtributo.html", {'form' : form, 'producto' : producto})
~~~
 
Falta la validación todavía). Pasamos en el contexto el QuerySet de "producto" para utilizarlo en la plantilla. Colocamos el formulario y el input en la plantilla:

`<label for="flexCheckDefault" class="form-check-label">Atributo</label>
{{form.atributo}}`

O sea, carga los atributos guardados. Lo que sí, hay que revisar bien los checkbox de Bootstrap porque quedó desordenado.

Otra forma de hacerlo es haciendo el QuerySet de Atributo dentro de la vista y colocar los componentes de otra forma en la plantilla:

~~~
def consultasAgregarAtributo(request, id):
	try:
		producto = Producto.objects.get(pk = id)
	except Producto.DoesNotExist:
		raise Http404

	if request.method == "POST":
		form = FormularioAtributo(request.POST)
	else:
		form = FormularioAtributo()

	#Otra forma de hacerlo
	atributos = Atributo.objects.all()
	return render(request, "consultas/agregarAtributo.html", 
		{'form' : form, 'producto' : producto, 'atributos': atributos})
~~~

En la plantilla los colocamos de a uno:

~~~
<div class="row">
	<ul class="list-group">
		{% for atributo in atributos %}
		<li>
			<input type="checkbox" name="atributos" value="" id="">
			{{atributo.nombre}}
		</li>
		{% endfor %}
	</ul>				
</div>	
~~~

Adicionalmente se le puede colocar estilos de Bootstrap para que, por ejemplo, no muestre las viñetas propias de los "li". Como sea, también muestra listados los atributos guardados en la tabla.

La idea de todo esto es aplicar los atributos seleccionados al producto cargado en la plantilla. En el "onlick" del botón de submit, vamos a crear una función de JS:

~~~
function set_atributos() {
	if ($('input[type=checkbox]:checked').length===0) {
		//e.preventDefault();
		alerAlert('Debe seleccionar al menos un atributo');
		return false;
	}
	document.form.submit();
}
~~~

La función consulta los checkbox que se hicieron, y si no se ha seleccionado alguno, mostrará una alerta y retornará "false" la función. De haber seleccionado al menos una opción, se enviará el formulario. Si no está instalado jquery-alert, se debe cambiar el "alertAlert" por un "alert".

Hacemos ahora la validación del formulario:

~~~
def consultasAgregarAtributo(request, id):
	try:
		producto = Producto.objects.get(pk = id)
	except Producto.DoesNotExist:
		raise Http404

	if request.method == "POST":
		form = FormularioAtributo(request.POST)
		if form.is_valid():
			datosForm = form.cleaned_data
			modelos_checks = eval(datosForm['atributos'])
		else:
			pass
	else:
		form = FormularioAtributo()

	#Otra forma de hacerlo
	atributos = Atributo.objects.all()
	return render(request, "consultas/agregarAtributo.html", 
		{'form' : form, 'producto' : producto, 'atributos': atributos})
~~~

Cada vez que actualicemos los registros a una tabla que provengan de checkboxes, lo primero que deberíamos hacer es borrar todos los registros asociados a esos checkbox y luego crearlos todos de nuevo. Añadimos para eso:

`ProductoAtributo.objects.filter(producto_id = id).delete()`

Luego, recorremos los elementos en "modelos_checks" y guardamos lo que necesitamos.

~~~
def consultasAgregarAtributo(request, id):
	try:
		producto = Producto.objects.get(pk = id)
	except Producto.DoesNotExist:
		raise Http404

	if request.method == "POST":
		form = FormularioAtributo(request.POST)
		if form.is_valid():
			datosForm = form.cleaned_data
			modelos_checks = eval(datosForm['atributos'])
			ProductoAtributo.objects.filter(producto_id = id).delete()
			for modelos_check in modelos_checks:
				guardar = ProductoAtributo()
				guardar.producto_id = id
				guardar.atributo_id = modelos_check
				guardar.save()
			messages.add_message(request, messages.SUCCESS, 
			f"Atributo(s) aplicado(s) exitosamente")
			return HttpResponseRedirect(f"/consultas/agregarAtributo/{id}")
		else:
			pass
	else:
		form = FormularioAtributo()

	#Otra forma de hacerlo
	atributos = Atributo.objects.all()
	return render(request, "consultas/agregarAtributo.html", 
		{'form' : form, 'producto' : producto, 'atributos': atributos})
~~~

Para que funcione, tenemos que llenar los atributos que dejamos vacíos en el input de la plantilla:

`<input type="checkbox" name="atributos" value="{{atributo.id}}" id="{{forloop.counter}}">`

Con eso, el ingreso de atributos funciona. Corroboré en la base de datos.

---

Para cargar los atributos que ya tiene asignado un producto, la forma más simple sería con un template tag:

~~~
@register.filter(name="existeAtributoEnProducto")
def existeAtributoEnProducto(producto_id, atributo_id):
	datos = ProductoAtributo.objects.filter(producto_id = producto_id).filter(
		atributo_id = atributo_id).count()
	if datos == 0:
		return ''
	else:
		return 'checked=true'
~~~

Colocamos el método en la etiqueta del input:

`<input type="checkbox" name="atributos" value="{{atributo.id}}" id="{{forloop.counter}}" {{producto.id|existeAtributoEnProducto:atributo.id}} />`

Cargamos el método con el producto_id, y le pasamos como parámetro el atributo_id. Funciona bien.

Hay otras formas de hacer lo mismo, pero pueden fallar con los checkboxes. El template tag es fácil de hacer y es efectivo.

---

### 61. Login Auth y trabajo con sesiones

Vamos a crear el formulario con un campo para correo y para contraseña:

`from django.forms import PasswordInput`

~~~
class FormularioLogin(forms.Form):
	correo = forms.EmailField(required=True, widget=forms.TextInput(
		attrs={'class':'form-control', 'placeholder':'E-Mail', 
		'autocomplete':'off'}))
	password = forms.CharField(widget=forms.PasswordInput(attrs={
		'class':'form-control', 'placeholder':'Contraseña',
		'autocomplete':'off'}))
~~~

Creamos ahora la ruta:

`path('login/', formulariosLogin, name = "formulariosLogin"),`

Dejamos creada una vista con ese nombre para no romper el programa, y una plantilla para esa vista. Definimos inicialmente la vista así:

`from django.contrib.auth import authenticate, login, logout`

~~~
def formulariosLogin(request):
	form = FormularioLogin(request.POST or None)
	if request.method == "POST":
		pass

	return render(request, "formularios/formulariosLogin.html", {'form':form})
~~~

Luego, cargamos los campos en el formulario de la plantilla:

~~~
<form name="form" method="post" action="{% url 'formulariosLogin' %}">
{% csrf_token %}
	<div class="mb-3">
		<label for="correo" class="form-label">Correo</label>
		{{form.correo}}
	</div>
	<div class="mb-3">
		<label for="password" class="form-label">Contraseña</label>
		{{form.password}}
	</div>			
	<hr>
	<a href="javascript:void(0);" class="btn btn-success" onclick="document.form.submit()">Ingresar</a>
</form>
~~~

Actualizamos la vista:

~~~
def formulariosLogin(request):
	form = FormularioLogin(request.POST or None)
	if request.method == "POST":
		if form.is_valid():
			data = form.cleaned_data
			user = authenticate(request, username=data['correo'],
				password=data['password'])
			if user is not None:
				login(request, user)
				usersMetadata = UsersMetadata.objects.filter(
					user_id = request.user.id).get()
				#variable de sesión
				request.session['users_metadata_id'] = usersMetadata.id
				return HttpResponseRedirect("/formularios/logueado")
			else:
				messages.add_message(request, messages.WARNING, 
					f"Datos incorrectos. Vuelva a intentar.")
				return HttpResponseRedirect("/formularios/login")


	return render(request, "formularios/formulariosLogin.html", {'form':form})
~~~

La variable de sesión sirve para que haya datos que persistan durante la sesión en toda la página, sin tener que acceder a la base de datos a cada rato. 

Creamos la plantilla para la ruta "logueado", la ruta y la vista. La forma que se presenta en la vista para corroborar si el usuario ha iniciado sesión es un poco "rústica", pero es válida para el ejemplo:

~~~
def formulariosLogueado(request):
	if not request.user.is_authenticated:
		messages.add_message(request, messages.WARNING, 
			f"Debes iniciar sesión para ver este contenido")
		return HttpResponseRedirect("/formularios/login")
	else: 
		pass
	return render(request, "formularios/formulariosLogueado.html", {})

~~~

Hasta el momento, funciona. Como el input está configurado para ingresar un email, y el superusuario que creé no tiene ese formato, voy a crear un superusuario con ese formato de nombre para hacer pruebas: loro@loro.com. 

Me dio problemas con ese usuario que creé, pese a que es superusuario, así que cambié en el formulario el "EmailField" por "CharField", y cambié el nombre del campo "correo" a "usuario", para hacerlo más general. Ahí funcionó bien.

**Nota:** cuando hagamos un formulario, ninguno de los campos debería llamrse "user", ya que es como una palabra reservada que usa Django.

**Otra nota:** Cuando iniciamos sesión de ese modo, se inicia sesión en el panel de administración igualmente. Hay veces en que vamos a necesitar tomar algunas precauciones para que esto no ocurra.

Mostramos en la plantilla algunos elementos típicos de un inicio de sesión. Vamos a usar un tag condicional, que no es indispensable, ya que pusimos las condicionales de autenticación dentro de la vista, pero se va a hacer así para aprender cómo se hace eso:

~~~
<p>
	{% if user.is_authenticated %}
	Bienvenido(a), {{user.first_name}} {{user.last_name}}
	{% endif %}			
</p>
<p>
	<a href="">Salir</a>
</p>
~~~

Lo típico es ponerlo en la plantilla base.

Para cerrar sesión, creamos una ruta:

`path('logout/', formulariosLogout, name = "formulariosLogout"),`

Colocamos esa ruta en el href del link para salir y creamos la vista.

~~~
def formulariosLogout(request):
	logout(request)
	#borrar variables de sesión (solo si las creamos)
	try:
		del request.session['users_metadata_id']
	except KeyError:
		pass
	#/borrar variables de sesión
	messages.add_message(request, messages.WARNING,
		"Sesión cerrada exitosamente")
	return HttpResponseRedirect("/formularios/login")
~~~

Opcionalmente podríamos colocar una función jquery-alerts para preguntarle al usuario si quiere cerrar sesión:

~~~
function logout(ruta) {
	jCustomConfirm('¿Realmente quiere cerrar sesión?',
		'Ejemplo 1', 'Aceptar', 'Cancelar', function(r) {
			if (r) {
				window.location = ruta;
			}
		});
}
~~~

Si la usamos, el link de logout va así:

`<a href="javascript:void(0)" onclick="logout('{% url 'formulariosLogout' %}')">Salir</a>`


---

### 62. Decoradores

El método creado anteriormente de "request.user.is_authenticated" tendríamos que usarlo para cada vista donde queramos pedir que el usuario inicie sesión, lo cual hace el código poco mantenible. Podemos facilitar el proceso creando un decorador, para colocarlo sobre la vista:

Una de las opciones que recomienda el instructor es tener una app para el login, y ahí tener nuestras vistas y decoradores. Por ahora, o vamos a hacer dentro de la app "formularios". Creamos un archivo "decorators.py" (debe tener ese nombre para que Django reconozca para qué es).

~~~
from functools import wraps
from django.contrib.auth import authenticate
from home.models import *
from django.http import HttpResponseRedirect, Http404
from django.contrib import messages
~~~

~~~
def logueado():
	def _activo_required(func):
		@wraps(func)
		def _decorator(request, *args, **kwargs):
		#aquí va el código
			pass

		return _decorator
	return _activo_required
~~~

Todos los decoradores van a seguir ese esquema, así que podemos copiarlo como una plantilla de ejemplo dentro del archivo. Terminamos de escribir el decorador:

~~~
def logueado():
	def _activo_required(func):
		@wraps(func)
		def _decorator(request, *args, **kwargs):
			#aquí va el código
			if not request.user.is_authenticated:
				messages.add_message(request, messages.WARNING, 
			f"Debes iniciar sesión para ver este contenido")
				return HttpResponseRedirect("/formularios/login")
			else:
				return func(request, *args, **kwargs)

		return _decorator
	return _activo_required
~~~

Con esto, podemos modificar la vista y quitarle el código que añadimos al decorador. Tenemos que importar el archivo de decoradores en el de vistas:

`from .decorators import *`

~~~
@logueado()
def formulariosLogueado(request):	
	return render(request, "formularios/formulariosLogueado.html", {})
~~~

Hasta ahí, funciona. Si tenemos creadas variables de sesión, el instructor recomienda que vayan en el decorador igualmente.

Otro uso que podemos darle a los decoradores es crear uno que valide el perfil de cada usuario según nuestro "users metadata". Habitualmente, en Django, los decoradores se usan para todo lo que tiene que ver con inicios de sesión, perfilamiento y roles de usuarios, requisitos de acceso (como haber pagado por el servicio), etc.









