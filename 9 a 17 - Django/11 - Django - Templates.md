### 52. Template tags, condiciones, ciclos

El instructor crea una app para las plantillas, "template". El instructor además aplica permisos totales para todo el contenido:

`chmod 777 -R *`

Hacemos lo de siempre: incrustar la nueva app en INSTALLED_APPS, crear un archivo "urls.py" dentro del directorio de la app; luego incluirla en el archivo "urls" principal y finalmente crear ua vista para esa url en el archivo "views.py". Creamos también, dentro de "templates", un directorio "template" para esa app, y le incluimos dentro un archivo "templateHome.html".

En la plantilla de inicio colocamos un enlace hacia la url que creamos. Dejamos los links así por el momento:

~~~
<div class="nav-scroller py-1 mb-2">
	<nav class="nav d-flex justify-content-between">
		<a class="p-2 link-secondary" href="{% url 'aboutUs' %}">Nosotros</a>
		<a class="p-2 link-secondary" href="{% url 'estiloHome' %}">Estilo</a>
		<a class="p-2 link-secondary" href="{% url 'templateHome'%}">Template</a>      
	</nav>
</div>
~~~

Los comentarios de Python en los templates se hacen con 

`{% comment%} Comentario {% endcomment %}`

Estos comentarios se verán solo del lado del servidor. No serán visibles ni en la plantilla ni el el código de esta.

Añadimos variables a la vista y las vamos a pasar como parámetros:

~~~
def templateHome(request):
	listas = ["uno", "dos", "tres"]
	texto = "<h1>asdf</h1>"
	return render(request, "template/templateHome.html", 
				{"listas" : listas, "texto" : texto})
~~~

Imprimos el "texto" dentro de la plantilla:

`<p>{{texto}}</p>`

Inicialmente sale así: `<h1>asdf</h1>`, o sea, no aplica el formato HTML. Django ofrece una opción para poder aplicar el formato HTML en casos como ese, utilizando un "pipe" ("|") para utilizar una función.

`<p>{{texto|safe}}</p>`

Cargamos ahora una imagen (recordatorio: debe estar cargado "static" dentro de la plantilla):

~~~
<p>
  <img src="{% static 'img/falcon.png' %}" width="240" height="240">
</p>
~~~

Podemos igualmente añadir una variable con el nombre de la imagen dentro de la vista (también hay que añadirla al diccionario del contexto):

`halcon = "falcon.png"`

Incrustamos la imagen de la siguiente forma, que es la que recomienda el instructor:

`<img src="{% static 'img/' %}{{halcon}}"</img>`

Cuando son archivos dinámicos, se recomienda separarlos y llamarlos aparte dentro de la misma ruta.

---

#### Uso sencillo de condicionales

Añadimos una variable en la vista:

`color = "rojo"`

En la plantilla hacemos una condición sencilla:

~~~
<h3>Condicionales</h3>

	{% if color == "rojo" %}

	<span class="text-danger">es rojo</span>

	{% else %}

	<span class="text-success">es verde</span>

	{% endif %}
~~~

Dado que el valor de la variable "color" es "rojo", se va a mostrar el texto "es rojo" en la plantilla, con la clase correspondiente de Bootstrap. Si le cambiamos el valor a "color", nos va a mostrar el texto "es verde".

Podríamos pasarle una concantenación usando una función de los tags:

~~~
{% if color == "rojo" %}

	<span class="text-primary">
		{{ color|add:"es el color de la sangre"}}
	</span>
~~~

--- 

#### Iteraciones

Añadimos lo siguiente en la plantilla:

~~~
<p>
	<h3>Recorrer valores</h3>

	<ul>
	{% for lista in listas %}
		<li>{{lista}}</li>
	{% endfor %}
	</ul>			
</p>
~~~

Se puede anidar bucles for en condicionales y viceversa, tal como en Python. Igualmente se puede añidar un bucle for dentro de otro y un condicional dentro de otro.

Añadimos más elementos a la lista de ejemplo y usamos un filtro para mostrar solo los elementos en índices pares.

~~~
<ul>
	{% for lista in listas %}
		{% if forloop.counter|divisibleby:2 == True %}
			<li>{{ forloop.counter }} - {{lista}}</li>
		{% endif %}
	{% endfor %}
</ul>	
~~~

---

### 53. ORM de Django. Trabajo con filtros en consultas

Creamos una nueva app llamada "consultas", y hacemos todo el "trámite" correspondiente (incrustar la app, rutas, una vista, plantilla, etc.).

Primero, vamos a hacer la consulta por las categorías. Para poder hacer consultas, necesitamos cargar los modelos de la app "home" en el archivo de vistas:

`from home.models import *`

Para poder listar registros, creamos una variable con la consulta:

`categorias = Categoria.objects.all()`

El parámetro .all() carga todos los objetos en el modelo. Inicialmente dejamos así la vista:

~~~
def consultasHome(request):
	categorias = Categoria.objects.all()

	return render(request, "consultas/consultasHome.html", 
				{"categorias" : categorias})
~~~

Vamos a la plantilla y utilizamos un bucle for para recorrer y mostrar las categorías registradas:

~~~
<ul>
	{% for categoria in categorias %}
		<li>
			<a href="">{{ categoria.nombre }}</a>
		</li>
	{% endfor %}
</ul>	
~~~

Podemos colocar otros métodos en la consulta. Por ejemplo, si queremos que ordene por el nombre: 

`categorias = Categoria.objects.order_by('nombre').all()`

Ahora, dentro del href vamos a colocar una URL para una vista solo para esa categoría, para que muestre los productos que tiene asociados. Le vamos a pasar como parámetro el slug:

`<a href="{% url 'productos-por-categoria' categoria.slug %}" title="{{ categoria.nombre }}">{{ categoria.nombre }}</a>`

La vista en el archivo "views":

~~~
def consultaProductosPorCategoria(request, slug):
	pass

	return render(request, "consultas/productosPorCategoria.html", 
				{})
~~~

La URL queda así:

`path('productos-por-categoria/<str:slug>', consultaProductosPorCategoria, name = 'productos-por-categoria')`

Vamos a utilizar también un "breadcrumb" de Bootstrap para darle navegabilidad al sitio. Lo colocamos al principio del container en la plantilla de productos:

~~~
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{% url 'home' %}">Home</a></li>
    <li class="breadcrumb-item"><a href="{% url 'consultasHome' %}">Categorías</a></li>
    <li class="breadcrumb-item active" aria-current="page">Data</li>
  </ol>
</nav>
~~~

También va a ser bueno saber en qué categoría estamos parados. Cada vez que hagamos una consulta con parámetros, vamos a usar un try:

`from django.http import Http404`

~~~
def consultaProductosPorCategoria(request, slug):
	try:
		cat = Categoria.objects.filter(slug = slug).get()
	except Categoria.DoesNotExist:
		raise Http404

	return render(request, "consultas/productosPorCategoria.html", 
				{'cat' : cat, })
~~~

Se usa .get() cuando queremos obtener un solo registro, a diferencia de .all(), que se usa cuando queremos obtener todos los registros disponibles. El método .DoesNotExist se usa como excepción, cuando se busca una URL con un id o un slug, y este no está registrado. En ese caso, vamos a forzar un error 404.

Para mostrar la categoría actual, modificamos el "li" que nos queda del breadcrumb:

`<li class="breadcrumb-item active" aria-current="page">Productos de la categoría {{cat.nombre}}</li>`

Para listar los productos que tiene esa categoría, esta es una forma:

`productos = Producto.objects.filter(categoria_id = cat.id).all()`

En este caso, como obtuvimos la categoría por el slug, hay otra forma con la cual podemos cargar los productos:

`productos = Producto.objects.filter(categoria__slug = slug)`

Usando dos guiones bajos, podemos usar un campo de una clave foránea.

Damos formato a la plantilla para mostrar un listado de los productos:

~~~
<div class="row">
		<h2>Productos de la categoría {{cat.nombre}}</h2>
			<table class="table">
				<thead>
					<tr>
						<th>id</th>
						<th>Categoría</th>
						<th>Nombre</th>
						<th>Descripción</th>
						<th>Precio</th>
						<th>Foto</th>
						<th>Acciones</th>
					</tr>
				</thead>
				<tbody>
					{% for producto in productos %}
						<tr>
							<td>{{producto.id}}</td>
							<td>{{producto.categoria.nombre}}</td>
							<td>{{producto.nombre}}</td>
							<td>{{producto.descripcion}}</td>
							<td>${{producto.precio}}</td>
							<td>
								<img src="{% static 'upload/producto/'%}{{producto.foto}}" width="100" height="100">
							</td>
							<td></td>
						</tr>
					{% endfor %}
				</tbody>
			</table>
	</div>
~~~

Sigue el problema de las imágenes. Tengo que arreglarlo al terminar esta lección.

Vamos a crear un campo "precio" en el modelo Producto.

`precio = models.PositiveIntegerField(default=0)`

(Recordar hacer la migración)

Luego, vamos al panel de administración y añadimos precios a los productos que hemos creado.


Una forma adicional de filtrar los registros por id es usar un "where in":

`productos = Producto.objects.filter(categoria_id__in=[cat.id]).all()`

Vamos ahora a crear una plantilla que muestra un producto en detalle. La ruta va a recibir 2 parámetros:

`path('producto-detalle/<int:id>/<str:slug>/', productoDetalle, name = "producto-detalle"),`

Creamos la vista correspondiente para esa ruta y añadimos un link en la plantilla para cargar esa ruta.

La vista:

~~~
def productoDetalle(request, id, slug):
	try:
		producto = Producto.objects.filter(slug = slug, id = id).get()
	except Producto.DoesNotExist:
		raise Http404

	categorias = Categoria.objects.all()

	return render(request, "consultas/productoDetalle.html", 
		{"producto" : producto, "categorias" : categorias})
~~~

---

**Por si se me olvida:** Para colocar la imagen del objeto, se hace dentro de la etiqueta "img", tal que así:

~~~
<p>
  <img src="{% static 'uploads/producto/'%}{{producto.foto}}" width="240" height="240" alt="{{producto.nombre}}">
</p>
~~~

Lo pongo porque casi la cagué...

---

### 54. Paginación de registros

Django no provee la paginación tan "lista", así que hay que entender bien cómo funciona.

En Django, imporatmos el módulo "Paginator". En "settings.py" habíamos establecido una cantidad de registros por página (TOTAL_PAGINAS), que por el momento está en 10. Para que veamos bien cómo funciona, la idea es tener creada una cantidad mayor de registros. Creé registros hasta tener 11.

Vamos a modificar la plantilla y la vista de inicio de consultas para colocar una tabla que muestre todos los productos.

La paginación puede incluirse dentro del mismo archivo de vistas, pero el instructor prefiere desacoplarla a través de una función dentro de "utilidades". Dentro de ese directorio, creamos un archivo módulo de nombre "utilidades.py".

~~~
from django.core.paginator import Paginator
from django.conf import settings
~~~

Tenemos que importar "Paginator", que es el módulo que provee Django para la paginación. También tenemos que importar el archivo "settings" porque ahí tenemos la constante de la cantidad de páginas.

Para que funcione la paginación, vamos a pasar a la ruta un parámetro conocido como "Query string", que está en formato "?page=pagina". Creamos el método en el archivo "utilidades":

~~~
def get_paginacion(total, request):
	page = request.GET.get("page")
	paginator = Paginator(total, settings.TOTAL_PAGINAS)
	datos = paginator.get_page(page)
	numeros = []

	if len(datos) >= settings.TOTAL_PAGINAS:
		for ultima in range(1, datos.paginator.num_pages):
			numeros.append(ultima)
		numeros.append(ultima + 1)

	return [datos, numeros, page]
~~~

En el archivo "views", importamos la librería "utilidades" y el módulo "utilidades":

`from utilidades import utilidades`

Dejamos la vista así:

~~~
def consultasHome(request):
	categorias = Categoria.objects.all()
	total = Producto.objects.order_by("-id").all()
	paginar = utilidades.get_paginacion(total, request)

	return render(request, "consultas/consultasHome.html", 
				{"categorias" : categorias, "total" : total,
				"datos" : paginar[0], "numeros": paginar[1],
				"page" : paginar[2], })
~~~

Colocamos en la plantilla "consultasHome" la misma tabla que teníamos en la plantilla de productos por categoría, y vemos que funciona correctamente. Hay 11 registros en el modelo de productos, y como en el archivo JSON se establecieron 10 registros por página, solo muestra 10.

Vamos a colocar una paginación de Bootstrap en la plantilla y le vamos a hacer unas modificaciones:

~~~
<nav aria-label="Page navigation example">
	<ul class="pagination">
	{% if datos.has_previous %}
		<li class="page-item"><a class="page-link" href="?page=1" title="Primera página">Principio</a></li>
		<li class="page-item"><a class="page-link" href="?page={{datos.previous_page_number}}" title="Página anterior">Prev..</a></li>
	{% endif %}				

	{% for numero in numeros %}
		<li class="page-item"><a class="page-link" href="?page={{numero}}" title="Página número">{{numero}}</a></li>			
	{% endfor %}

	{% if datos.has_next %}
		<li class="page-item"><a class="page-link" href="?page={{datos.next_page_number}}">Sig.</a></li>
		<li class="page-item"><a class="page-link" href="?page={{datos.next_page_number}}" title="Última página">Final</a></li>
	{% endif %}
	</ul>
</nav>
~~~

Como este paginador podemos usarlo en más de un archivo, si tuviéramos que hacer modificaciones, tendríamos que buscar cada archivo que tenga este paginador y hacer las modificaciones de a uno. En cambio, podemos mover este código a un archivo aparte, que va a tener el nombre "paginacion.html", que quede en el "layout", y usar un tag para incluirlo.

En donde debería ir el paginador, escribimos:

`{% include "layout/paginacion.html" %}`

---

### 55. Creación de templates tag custom

Hay varias formas de crear nuestros propios template tags. La que usa el instructor es crear un directorio "templatetags" dentro del directorio de "home", que va a tener un archivo `__init__.py`, para poder ser importado como un módulo. También va a tener un archivo que va a contener los template tags (el instructor le nombra "utilidades.py", pero yo le voy a nombrar "templatetags.py").

~~~
from django import template

register = template.Library()
~~~

La variable "register" será usada como decorador. Con .Library() se van a registrar nuestros template tags. Dentro de los template tags podemos hacer lo que queramos.

La forma en que vamos a hacer el decorador normalmente no se hace así, pero los template tags se hacen de la forma que se va a mostrar. Este es un ejemplo muy sencillo:

~~~
@register.filter(name="ejemploFiltro")
def ejemploFiltro(parametro):
	return f"el valor del parámetro es {parametro}"
~~~

Lo probamos en la plantilla de "home", pasándole la página como parámetro:

`<p>ejemplo template tag: {{page|ejemploFiltro}}</p>`

Tenemos que cargarlo en la plantilla para que funcione, de la misma forma como cuando cargamos static:

`{% load templatetags %}`

O sea, en el video es `{% load utilidades %}`, pero yo le había puesto otro nombre al módulo.

El instructor provee un decorador para formatear los precios colocando puntos y comas según el caso. Yo lo reescribí para que ocupe una línea menos de código:

~~~
@register.filter(name="numberFormat")
def numberFormat(numero):
	if numero != None:
		return "{:,}".format(numero).replace(",",".")
	return 0
~~~

También ofrece un método para formatear las fechas, a ver si es más rápido que usar el filtro "date":

~~~
@register.filter(name="inviertirFecha")
def invertirFecha(fechaDateTime):
	return fechaDateTime.strftime('%d/%m/%Y')
~~~

Para invertir hora:

~~~
@register.filter(name="invertirFechaHora")
def invertirFechaHora(fechaDateTime):
	return fechaDateTime.strftime('%d-%m-%Y %H:%M:%S')
~~~

Es más rápido que usar el filtro "date", porque si uno quiere hacer cambios, solo va a esta parte del código en vez de estar cambiando los tags uno por uno.

Crearemos ahora unos template tags para trabajar con las bases de datos.

~~~
@register.filter(name="getMetadata")
def getMetadata(n):
	datos = Metadata.objects.get()	

	if n== 1:
		return datos.keyword
	if n == 2:
		return datos.description
	if n == 3:
		return datos.correo
	if n == 4:
		return datos.telefono
	if n == 5:
		return datos.titulo
~~~

Después, abrimos nuestra plantilla base del layout y cargamos templatetags; incluimos este template tag en la etiqueta de título:

`<title>{{5|getMetadata}}{% block title %}{% endblock %}</title>`

Colocamos 5 porque es con ese valor que retorna el título. Tenemos que crear igualmente un registro en el modelo Metadata para que funcione. Lo importante es que en este modelo tengamos un solo registro que tenga el id 1, y que tenga la información principal de nuestro sitio. En los keywords, por ejemplo, escribimos las palabras claves que van a utilizar los motores de búsqueda. La información contenida en los campos de ese registro se van a colocar en la sección "meta" de la plantilla para entregar información a los motores de búsqueda.

Entonces, el título, entre otras cosas, puede ser administrado dinámicamente desde la base de datos.

Colocamos unos meta más en el principio de la plantilla:

~~~
<meta name="description" content="{{2|getMetadata}}">
<meta name="keywords" content="{{3|getMetadata}}">
~~~

Los de correo y teléfono podemos ocuparlos más adelante en un formulario de contacto.

Este template tag se va a usar principalmente para los motores de búsqueda. 


---

### 56. Creación de buscador

Debajo del título del listado vamos a colocar un buscador. Ponemos un formulario de Bootstrap para empezar:

~~~
<p>
<form action="" method="get">
<div class="input-group mb-3">
<div class="input-group mb-3">
<input type="text" class="form-control" placeholder="Ingrese texto para buscar" name="b" id="b">
<button class="btn btn-outline-secondary" type="button" onclick="buscador()">Buscar</button>
</div>
</div>				
</form>
</p>
~~~

Tenemos que crear una función JavaScript para el "onclick" del botón.

~~~
function buscador(){
	if(document.getElementById('b').value == 0){
		return false;
	}
	window.location="";
}
~~~

En "window.location" tenemos que cargar una URL, así que creamos un path para esa URL y creamos una vista.

`path('buscador/', consultasBuscador, name = "consultasBuscador"),`

Creamos una vista, que por el momento va a ser igual a la vista "consultasHome" (no es necesario crearle una plantilla aparte):

~~~
def consultasBuscador(request):
	if not request.GET.get('b'):
		b = ''
	else:
		b = request.GET.get('b')
		
	categorias = Categoria.objects.all()
	total = Producto.objects.filter(nombre__icontains=b).order_by("-id").all()
	paginar = utilidades.get_paginacion(total, request)

	return render(request, "consultas/consultasHome.html", 
				{"categorias" : categorias, "total" : total,
				"datos" : paginar[0], "numeros": paginar[1],
				"page" : paginar[2], })
~~~

`__icontains` es como un "like" en SQL. Se definió para que busque por nombre "que se parezca a lo que escribimos".


Pasamos la ruta a la función JS:

~~~
function buscador(){
	if(document.getElementById('b').value == 0){
		return false;
	}
	window.location="/consultas/buscador?b="+document.getElementById('b').value+"&page=1";
}
~~~


Podríamos crear una plantilla para la búsqueda. En el contexto de la vista añadimos "b" como parámetro. Creamos un archivo "buscador.html". Le colocamos el breadcrumb, y al final de este un "resultados de la búsqueda", añadiendo el parámetro "b", que es el texto buscado. Yo por mi parte, le añadí esto en el título de la tabla:

~~~
<h2>Resultados de la búsqueda <strong>"{{b}}"</strong> </h2>
<p>{{total.count}} resultado(s)</p>
~~~

Nótese que cuando hacemos la búsqueda con el campo vacío, nos puede dar un error, o que se taime la página porque no está llevando el valor de "page". Eso lo podemos prevenir de varias formas. Una es colocar una etiqueta "hidden" dentro del formulario.

`<input type="hidden" name="page" value="1" />`

También podemos hacer que en el campo nos muestre el texto que buscamos, colocando en el input, un "value='{{b}}'"

---



















