## 13 - Django - Reportes

---

### 63. Exportar datos a PDF

Creamos una app llamada "reportes", y hacemos todo el trámite correspondiente: ruta, vista, plantilla, etc.

Creamos además una ruta, vista y plantilla para el trabajo con archivos PDF.

`path('pdf/', reportesPdf, name = "reportesPdf"),`


En el archivo de vistas de esta app, importamos:

~~~
from django.http import HttpResponse, Http404
from django.conf import settings
from django.template.loader import render_to_string
from weasyprint import HTML
~~~

(Debemos instalar WeasyPrint con pip)

Adicionalmente importamos la librería "os".

~~~
def reportesPdf(request):
	ruta = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
	base_url = settings.BASE_URL
	template = f"{ruta}/templates/layout/ajax.html"
	context = {'template' : template, 'request' : request, 'ruta' : ruta, 'base_url' : base_url}

	html = render_to_string(f"{ruta}/templates/reportes/reportesPdf.html", context)

	response = HttpResponse(content_type="application/pdf")
	response["Content-Disposition"] = "inline; mi_pdf.pdf"
	HTML(string=html).write_pdf(response)

	return response
~~~

La variable "base_url" ocupa el BASE_URL que pusimos en el archivo "settings". El método "render_to_string" cumple la función equivalente a "render". El parámetro "content_type" se usa para forzar a convertir el contenido de la plantilla en un PDF. Desde la variable html, el código suele ser el mismo en cualquier programa. 

La plantilla debe extender la ruta donde dejamos el archivo AJAX.

~~~
{% extends template %}
{% load static %}
{% load templatetags %}


{% block body %}

<h1>Holi</h1>

<p>¿Tení pololi?</p>

{% endblock %}
~~~

En este caso, al acceder a esa ruta, en vez de cargar "reportesPdf.html" como una página web, la carga un archivo PDF. Lo que sí, no me está tomando el `target = "_blank"`. Me está abriendo en la misma pestaña del navegador.

La carga de AJAX nos permite aplicar código HTML en el archivo PDF. De hecho, podemos escribir la estructura de la página completa, incluido el DOCTYPE, dentro del "block body". Igualmente, podemos cargar archivos de estilos, como Bootstrap. Para cargar estos archivos, y también los de imagen, no usamos los tag de "static". Usamos la variable "base_url". Por ejemplo:

`<img src="{{base_url|safe}}/static/img/rondamon.jpg" />`

**NOTA:** Para que a mí me funcionara, tuve que quitar el / entre el tag y static, porque la ruta me ponía uno adicional y no cargaba la imagen porque no la encontraba.

El instructor recomienda poner el "|safe" porque en algunos casos puede haber un error si el tag "escapa" al código HTML.

Le habíamos puesto un nombre "mi_pdf.pdf", aunque da lo mismo, porque, cuando se quiera descargar el PDF, se da la opción de ponerle un nombre. Por lo mismo, no es necesario ponerle una etiqueta "title" a la página.

**NOTA:** No se va a renderizar el contenido exactamente igual a una página web, así que hay que aprender a jugar con los estilos.

**OTRA NOTA:** Me sale un error cuando importo Bootstrap. Cualquier otra librería funciona correctamente. Solo con Bootstrap estoy teniendo problemas.

**OTRA NOTA MÁS:** Si nos sale contenido en el PDF con formato HTML "incompatible", se recomienda colocarlo dentro de una etiqueta `<pre>`

---

### 64. Importar Excel y guardar datos en MySQL

Creamos una ruta, una vista y una plantilla para importar archivos Excel.

`path('importarExcel/', reportesImportarExcel, name = "reportesImportarExcel"),`

Lo primero que tenemos que crear es un formulario dentro de la plantilla. 

~~~
<form action="{% url 'reportesImportarExcel' %}" name="form" enctype="multipart/form-data" method="post">
	{% csrf_token %}
	<p>
		<label for="archivo">
			<input type="file" name="file">
		</label>
		<hr>
		<button type="submit" class="btn btn-primary">Enviar</button>
	</p>			
</form>
~~~

Creamos ahora un archivo "forms.py" dentro del directorio de la app.

~~~
class FormularioImportarExcel(forms.Form):
	file = forms.FileField(
		required = False,
		widget = forms.FileInput(
			attrs={'class':'ss', 'id':'file', 'data-min-file-count':'1'}
			)
		)
~~~

Importamos el archivo de formularios en la vista:

`from .forms import *`

Colocamos el formulario en la vista:

~~~
def reportesImportarExcel(request):
	if request.method == "POST":
		form = FormularioImportarExcel(request.POST, request.FILES)
		if form.is_valid():
			datos = form.cleaned_data
			#guardar el archivo
			myfile=request.FILES['file']
			fs=FileSystemStorage()
			filename = fs.save(f"excel/archivo.xls", myfile)
			uploaded_file_url = fs.url(filename)

			#código para leer el archivo

			#código para guardar en la BD

	else:
		form = FormularioImportarExcel()
	return render(request, "reportes/reportesImportarExcel.html", {'form' : form})
~~~

Así como está, debéría funcionar. Usamos un archivo .xlsx para probar. Si vamos al directorio "media", vemos que se ha creado un directorio "excel" con el archivo subido correspondiente. Cuando subimos el archivo varias veces, va creando uno nuevo con el nombre más caracteres aleatorios. Eso es porque Django trata de darle un nombre diferente cada vez que lo sube. En algunos casos, vamos a preferir que sobreescriba el archivo.

Ahora que tenemos subido el archivo, vamos a leerlo. Instalamos, con pip, "xlrd" y "django_excel". Importamos una librería de Python en el archivo views:

`import xlrd`

Nota: esta librería es compatible solo con archivos .xls . Para manejar archivos .xlsx se debe usar otra librería.

Cuando importemos el archivo, es importante respetar la estructura de este. Por ejemplo, el que voy a usar de ejemplo tiene contenido en la hoja 0, y éste empieza en A1.

Probamos con este código para leer el archivo cuando se suba:

~~~
#leer el archivo
	documento = xlrd.open_workbook(settings.BASE_DIR + "/media/excel/archivo.xls")
	doc_datos = documento.sheet_by_index(0)
	return HttpResponse(doc_datos)
~~~

La librería devuelve el contenido de las celdas en forma de listas.

Lo que recomienda el instructor, para ahorrarnos dolores de cabeza a futuro con clientes, es tener disponible un archivo Excel de ejemplo en un `<a>` en la misma plantilla, para que los clientes sepan qué formato deben tener los archivos que suban y así no tener problemas. Las librerías para manipular Excel con Python no hacen de todo. Por ejemplo, puede haber problemas con las fórmulas.

Creamos un modelo para guardar los datos:

~~~
class Nombre(models.Model):
	nombre = models.CharField(max_length = 100, null = True)
	numero = models.PositiveIntegerField(default=0)
	
	def __str__(self):
		return self.nombre

	class Meta:
		db_table = "nombre"
		verbose_name = "Nombre"
		verbose_name_plural = "Nombres"
~~~

El instructor creó el modelo con el nombre "Nombres", pero yo lo dejé en singular por costumbre. Hacemos la migración correspondiente.

Para guardar los datos en la BD (hay que importar el archivo de modelos en el de vistas):

~~~
#guardar en la BD
for i in range(doc_datos.nrows):
	if i >= 1:
		Nombre.objects.create(numero=repr(
			doc_datos.cell_value(i,0)).replace("'",""),
			nombre = repr(doc_datos.cell_value(i,1)).replace("'",""))
~~~

Dio problemas al guardar los datos en la tabla, así que cambiamos el tipo de dato de "numero" a "CharField".

No me estaba guardando los datos en la BD porque no había quitado el "return HttpResponse" y se detenía la ejecución del código ahí.

**NOTA:** Los datos de Excel no son confiables. Siempre puede fallar algo. Por ejemplo, los datos numéricos los trajo como decimales, o pueden llegar los datos con comillas.

Importamos el módulo de mensajes en la vista, para cargar un mensaje flash. También importamos HttpResponseRedirect.

La vista queda así finalmente:

~~~
def reportesImportarExcel(request):
	if request.method == "POST":
		form = FormularioImportarExcel(request.POST, request.FILES)
		if form.is_valid():
			datos = form.cleaned_data
			#guardar el archivo
			myfile=request.FILES['file']
			fs=FileSystemStorage()
			filename = fs.save(f"excel/archivo.xls", myfile)
			uploaded_file_url = fs.url(filename)

			#leer el archivo
			documento = xlrd.open_workbook(settings.BASE_DIR + "/media/excel/archivo.xls")
			datos = documento.sheet_by_index(0)

			#guardar en la BD
			for i in range(datos.nrows):
				if i >= 0:
					Nombre.objects.create(numero=repr(
						datos.cell_value(i,0)).replace("'","").replace(".0",""),
					nombre = repr(datos.cell_value(i,1)).replace("'",""))

		messages.add_message(request, messages.SUCCESS, 
			"Datos importados exitosamente")
		return HttpResponseRedirect(f"/reportes/importarExcel")

	else:
		form = FormularioImportarExcel()
	return render(request, "reportes/reportesImportarExcel.html", {'form' : form})
~~~

En mi caso, puse "if i >= 0" porque tengo un dato en la primera fila en el archivo de ejemplo. Lo que me sirvió para que los números me los mostrara como enteros fue colocar un .replace() adicional. 

---

### 65. Importat .txt y guardar en MySQL

Creamos la ruta, vista y plantilla para esta lección.

En teoría, podríamos utilizar el mismo formulario que en la lección anterior, ya que no tiene validaciones para un formato específico de archivo (excepto porque guarda en formato .xls), pero vamos a crear otro de todas formas. La vista también se va a terminar pareciendo bastante.

La vista inicialmente queda así:

~~~
def reportesImportarTxt(request):
	if request.method == "POST":
		form = FormularioImportarTxt(request.POST, request.FILES)	
		if form.is_valid():
			datos = form.cleaned_data
			#guardar el archivo
			myfile=request.FILES['file']
			fs=FileSystemStorage()
			filename = fs.save(f"txt/archivo.txt", myfile)
			uploaded_file_url = fs.url(filename)	

	else:
		form = FormularioImportarTxt()
	return render(request, "reportes/reportesTxt.html", {'form':form,})
~~~

Para procesar el archivo .txt, lo más recomendable es usar las herramientas que ofrece Python. La vista queda así:

~~~
def reportesImportarTxt(request):
	if request.method == "POST":
		form = FormularioImportarTxt(request.POST, request.FILES)	
		if form.is_valid():
			datos = form.cleaned_data
			#guardar el archivo
			myfile=request.FILES['file']
			fs=FileSystemStorage()
			filename = fs.save(f"txt/archivo.txt", myfile)
			uploaded_file_url = fs.url(filename)	
			
			archivo = open(settings.BASE_DIR + "/media/txt/archivo.txt")
			lineas = archivo.readlines()

			for linea in lineas:
				Tracking.objects.create(descripcion=linea)

			messages.add_message(request, messages.SUCCESS, 
			"Datos importados exitosamente")

			os.remove(settings.BASE_DIR + "/media/txt/archivo.txt")

			return HttpResponseRedirect("/reportes/importarTxt")

	else:
		form = FormularioImportarTxt()
	return render(request, "reportes/reportesTxt.html", {'form':form,})
~~~

Ocupamos os.remove para remover el archivo después de leerlo y guardar los datos en la BD. Para probar, estamos guardando datos en el modelo Tracking. La función os.remove la implementamos también en la vista de importación de archivos Excel para que elimine esos archivos también después de leerlos.


---

### 66. Exportar datos a Excel o CSV

Creamos una ruta para trabajar con la importación de archivos.

`path('exportarExcel/', reportesExportarExcel, name = "reportesExportarExcel"),`

La vista para esa ruta se llama "reportesExportarExcel()"

Hay varias formas de exportar un archivo, pero vamos a crear un método específico para eso. Creamos otra ruta, específicamente para la exportación (aunque pudo haberse hecho en la misma ruta que creamos):

`path('ejecutarExportarExcel/', ejecutarExportarExcel, name = "ejecutarExportarExcel"),`

Para hacer las pruebas, vamos a exportar los datos del modelo Nombre. La vista para esta ruta se llama "ejecutarExportarExcel()" (lo anoto por si me confundo).

Importamos al librería "django_excel" dentro del archivo "views". Luego, creamos los encabezados del archivo dentro de esta vista "ejecutar":

`import django_excel as excel`

La vista queda así inicialmente:

~~~
def ejecutarExportarExcel(request):
	export = []
	export.append(["ID","NOMBRE","NÚMERO"])
	datos = Nombre.objects.all()
	for dato in datos:
		export.append([dato.id, dato.nombre, dato.numero])
	sheet = excel.pe.Sheet(export)
	return excel.make_response(sheet, "xlsx", file_name="archivo.xlsx")
~~~

Al instructor le costó que funcionara. Tuvo que bajar y volver a subir el servidor. Cuando falla así, en internet y el instructor recomiendan instalar las librerías de pyexcel.

`pip install pyexcel-ods odfpy pyexcel-xlsxw pyexcel-xlsx`

Después de hacer eso, funcionó. No hizo falta importar esas librerías en el archivo de vistas.

Si quisiéramos exportar a CSV, cambiamos las líneas correspondientes en la vista:

`return excel.make_response(sheet, "csv", file_name="archivo.csv")`

Lo que los clientes habitualmente piden es exportar/importar archivos PDF y Excel.