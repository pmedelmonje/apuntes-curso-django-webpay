### 46. Conexión MySQL

Hay una forma más segura para trabajar con los datos de la BD, sin tener que escribirlos directamente en el archivo "settings".

Creamos un archivo fuera del proyecto de tipo JSON (en este caso, "conf.json")

Dentro de ese archivo creamos un JSON:

~~~
{
	"debug": true,
	"base_url" : "http://127.0.0.1:8000/",
	"user": "phpmyadmin",
	"password" : "gastroll3791",
	"server" : "localhost",
	"bd" : "curso-django",	
	"paginacion" : 10,
	"ruta" : "/home/pedro/Cursos y Tutoriales/Cursos Udemy/Python Django y Webpay de Transbank/proyectos_curso/",
	"ruta2" : "/home/pedro/Cursos y Tutoriales/Cursos Udemy/Python Django y Webpay de Transbank/proyectos_curso/ejemplo_1/"
}
~~~

Para cargar este archivo, importamos la librería para leer JSON:

~~~
from pathlib import Path
import os
import json
~~~

`BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))`

~~~
# Datos de ocnfiguración

ruta = os.path.dirname(os.path.abspath(__file__))
f = open('{}/conf.json'.format(ruta), 'r')
conf_string = f.read()
f.close()
conf = json.loads(conf_string)
~~~

Lo que estamos haciendo es básicamente cargar el archivo "conf.json" y colocarlo en una variable. Luego, con json.loads, se formatea para tener los nodos disponibles. Añadimos un nodo al archivo JSON:

`"paginacion" : 10,`

~~~
BASE_URL = conf['base_url']
TOTAL_PAGINAS = conf['paginacion']

RUTA = conf['ruta']
RUTA2 = conf['ruta2']
~~~

"RUTA" y "RUTA2" los vamos a utilizar para localizar el archivo JSON. El DEBUG también se puede administrar desde el archivo conf:

`DEBUG = conf['debug']`

Añadimos un nodo `"puerto" : 3306,` en el archivo JSON, y luego configuramos los parámetros de la base de datos en el archivo "settings":

~~~
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': conf['bd'],
        'USER': conf['user'],
        'PASSWORD': conf['password'],
        'HOST': conf['host'],
        'PORT': conf['puerto'],
        'OPTIONS': {
            'autocommit': True,
        }
    }
}
~~~

El archivo JSON que usemos para las configuraciones debería estar en otro directorio, y en modo solo lectura, como medida de seguridad.

**Nota:** El servidor de prueba, aunque parezca obvio, es solo eso, un servidor de pruebas. No tiene la misma eficiencia que un servidor en producción, así que puede fallar si se le hace muchas peticiones seguidas.

---

### 47. Modelos, tipos de datos y migraciones

Al levantar el servidor por primera vez, va a aparecer un aviso en la terminal de unas migraciones pendientes, pese a que no hayamos creado tablas todavía. Esas migraciones son para implementar tablas que incluye Django para un mejor funcionamiento. Si solo se va a trabajar con páginas estáticas, sin base de datos, no es necesario aplicar esas migraciones.

Ejecutamos `python3 manage.py migrate` para hacer las migraciones iniciales para que se creen las tablas iniciales. Una de las tablas más importantes es la que guarda los registros de las migraciones. Otra tabla muy importante es **auth_user**, que va a contener los usuarios.

Para crear las tablas, utilizamos modelos. Cada app tiene un archivo "models.py" para crear modelos. Queda a gusto del programador crear modelos en cada aplicación o centralizar todos los modelos en una sola (es lo que he venido haciendo yo de hecho). 

Para que Django detecte los modelos creados, las apps deben estar incrustadas en "INSTALLED_APPS" de "settings.py".

Vamos a crear los primeros modelos en el archivo "models.py" de la app "home". Antes, vamos a instalar:

`pip install django-autoslug` Este es para aplicar slugs automáticamente a los modelos.

Importamos en el archivo "models" la tabla de usuarios.

`from django.contrib.auth.models import User`

Importamos también el módulo "django-autoslug":

`from autoslug import AutoSlugField`

Vamos a crear una tabla básica para los estados de la página:

~~~
class Estado(models.Model):
	id = models.AutoField(primary_key = True)
	nombre = models.CharField(max_length = 100)
	slug = AutoSlugField(populate_from = "nombre")
~~~

El campo "id" se crea automáticamente si no lo incluimos en el modelo, con todas las características necesarias para una clave primaria. Django da la opción de especificar en el modelo un campo id, por ejemplo, si queremos que tenga otro nombre, pero no se recomienda.

Adicionalmente podemos definir una clase "Meta" para modificar algunos parámetros del modelo. 

~~~
class Meta:
	db_table = "estado"
	verbose_name = "Estado"
	verbose_name_plural = "Estados"
~~~

"db_table" es para darle el nombre a la tabla. Si no lo hacemos, Django le va a dar un nombre predeterminado según el nombre de la app y el nombre del modelo. "verbose_name" y "verbose_name_plural" se usan para cambiar el nombre a mostrar en el panel de administración, para singular y plural respectivamente.

También incluimos el método mágico `__str__` para que podamos ver valores en forma de strings, y no solamente los objetos de las clases.

~~~
def __str__(self):
	return self.nombre
~~~

Ahora que tenemos un modelo creado, volvemos a hacer migraciones.

`python3 manage.py makemigrations`

`python3 manage.py migrate`

Después de ejecutar esos comandos, se crea un directorio "migrations" dentro del directorio de la app correspondiente, donde se muestra el registro de todas las operaciones realizadas.

Siempre que añadamos un nuevo campo a una tabla que ya tiene registros creados, Django va a "reclamar" indicando que se le tiene que asignar un valor por defecto para los registros que no tienen ese valor.

Creamos 2 modelos más:

~~~
class Genero(models.Model):
	nombre = models.CharField(max_length=100, null = True)

	def __str__(self):
		return self.nombre

	class Meta:
		db_table = 'genero'
		verbose_name = 'Género'
		verbose_name_plural = 'Géneros'


class Pais(models.Model):
	nombre = models.CharField(max_length=100, blank = True, null = True)

	def __str__(self):
		return self.nombre

	class Meta:
		db_table = "pais"
		verbose_name = "País"
		verbose_name_plural = "Países"


class Perfil(models.Model):
	nombre = models.CharField(max_length = 100, null = True)

	def __str__(self):
		return self.nombre

	class Meta:
		db_table = 'perfil'
		verbose_name = 'Perfil'
		verbose_name_plural = 'Perfiles'


class Metadata(models.Model):
	description = models.CharField(max_length=255, blank = True,
		null = True)
	keyword = models.TextField(default='0')
	correo = models.CharField(max_length=100, blank = True, null = True)
	telefono = models.CharField(max_length = 50, blank=True, null = True)
	titulo = models.CharField(max_length = 255, default = "dd")

	def __str__(self):
		return self.correo

	class Meta:
		db_table = "metadata"
~~~

"null = True" se usa cuando queremos que un campo no sea obligatorio. "blank = True" cuando queremos que el campo pueda estar vacío. 

> Por convención, debe haber 2 espacios entre cada clase.

La ventaja del ORM es que detecta cuando la base de datos es MySQL, Postgre o SQLite y ejecuta los comandos correspondientes para cada caso. 

---

Vamos ahora a establecer relaciones entre tablas. Creamos una tabla de categorías y una tabla de productos que va a estar relacionada a la tabla de categorías:

~~~
class Categoria(models.Model):	
	nombre = models.CharField(max_length = 100)
	slug = AutoSlugField(populate_from = "nombre")

	def __str__(self):
		return self.nombre

	class Meta:
		db_table = "categoria"
		verbose_name = "Categoria"
		verbose_name_plural = "Categorias"


class Producto(models.Model):	
	categoria = models.ForeignKey(Categoria, on_delete = models.DO_NOTHING)
	nombre = models.CharField(max_length = 100)
	slug = AutoSlugField(populate_from = "nombre")
	fecha = models.DateTimeField(auto_now = True)
	descripcion = models.TextField()

	def __str__(self):
		return self.nombre

	class Meta:
		db_table = "producto"
		verbose_name = "Producto"
		verbose_name_plural = "Productos"

~~~

Ahí se establece una relación Categoría:Producto = 1:N. Una categoría puede tener muchos productos, pero un producto tiene una sola categoría. "on_delete = models.DO_NOTHING" significa que, al eliminar una categoría, no se eliminanrán los productos asociados a esa categoría.


---

### 48. Creación de superusuario y extensión del modelo auth

Todo lo de los usuarios se almacena dentro de la tabla "auth_user". Creamos un superusuario con el comando `python3 manage.py createsuperuser`. El instructor recomienda que el nombre de usuario sea el email del cliente, ya que a nuestros futuros clientes se les puede olvidar un nombre de usuario.

Después de ejecutar ese comando y crear un superusuario, al revisar la tabla de usuarios, vemos que se ha creado un registro para el usuario que creamos. La contraseña es encriptada por Django así que vamos a ver un conjunto de caracteres distinto en ese campo.

En el campo "is_staff", si está en 1, quiere decir que el usuario tendrá superpoderes en el backend de la app. En el campo "is_active", si está en 0, el usuario no podrá realizar operaciones aunque se haya iniciado sesión correctamente.

A la tabla no podemos añadirle más campos. Para extender la tabla de usuarios, vamos a crear un modelo nuevo y le vamos a asignar una clave foránea para relacionarla al modelo User. La tabla puede tener cualquier nombre, pero por convención se le suele llamar "UsersMetadata".

~~~
class UsersMetadata(models.Model):
	user = models.ForeignKey(User, on_delete = models.DO_NOTHING)
	pais = models.ForeignKey(Pais, on_delete = models.DO_NOTHING)
	genero = models.ForeignKey(Genero, on_delete = models.DO_NOTHING)
	correo = models.CharField(max_length = 100, blank = True, null = True)
	telefono = models.CharField(max_length = 100, blank = True, null = True)
	direccion = models.CharField(max_length = 100, blank = True, null = True)
	fecha_nacimiento = models.DateField(default = '1980-01-01')

	def __str__(self):
		return f"{self.user.first_name} {self.user.last_name}"

	class Meta:
		db_table = 'users_metadata'
		verbose_name = 'User metadata'
		verbose_name_plural = 'Users metadata'
~~~

En general, podemos poner una clave foránea hacia User en cualquier tabla, pero se recomienda hacer todo en el modelo UsersMetadata.

Ahora, por cada usuario registrado, deberíamos ver un registro en "users_metadata"

---

### 49. Backend de Django

El backend viene de forma nativa dentro de Django. No es mucho lo que se puede modificar.

En el archivo "urls" principal ya viene una ruta para acceder al panel de administración. Por defecto es "admin/", aunque el instructor recomienda una un poco "menos fácil de acceder", como "core/backend/" o algo así. 

Iniciamos sesión con algún usuario que hayamos registrado. Inicialmente, solo vemos dos tablas, la de grupos y la de usuarios. Para incorporar las tablas de los modelos que hemos creado, vamos al archivo "admin.py"


~~~
admin.site.site_header = 'Holi'
admin.site.index_title = 'lala'
admin.site.site_title = 'lele'
~~~

El primero cambia el encabezado del panel de administración. El segundo cambia El título del índice y el tercero cambia el título en la pestaña del navegador. También hay módulos para cambiar la interfaz del panel.

Importamos los modelos y agregamos las tablas:

`from .models import *`

Creamos una clase de extensión para personalizar la forma en que se van a mostrar los datos, con ModelAdmin.

~~~
class EstadoAdmin(admin.ModelAdmin):
	list_display = ('id', 'nombre')
	search_fields = ('nombre', )

admin.site.register(Estado, EstadoAdmin)
~~~

"list_display" es una tupla donde colocamos los datos que se van a mostrar. "search_fields" coloca un buscador dentro del modelo para buscar según los campos que hayamos especificado.

`admin.site.register(Modelo)` registra el modelo para que sea visible en el panel de administración. Debemos escribirlo aunque hayamos hecho una clase ModelAdmin.

Creamos un par de registros para probar el funcionamiento. Cuando vemos la base de datos en phpmyadmin, vemos que se ha creado un slug automáticamente para cada registro.

Al registrar el modelo de UsersMetadata, vemos que cuando queremos crear un registro, nos muestra todas las claves foráneas y nos da la opción de elegir registros de los otros modelos. Cuando queramos mostrar datos, hay veces en que va a ser necesario formatear los valores. Una de las opciones es la siguiente:

Dentro del directorio base del proyecto creamos uno nuevo (el instructor lo llama "utilidades"). Dentro, creamos un archivo `__init__.py` para que sea considerado un paquete, y un archivo "formularios.py". Lo que se va a hacer ahora puede hacerse dentro del archivo "admin.py", pero el instructor prefiere hacerlo aparte.

En el archivo "formularios.py" escribimos:

~~~
from home.models import *
from django.utils.html import format_html

def set_user(obj):
	return f"{obj.user.first_name} {obj.user.last_name}"

set_user.short_description = "Usuario"
~~~

Importamos este nuevo módulo en "admin.py":

`from utilidades import formularios`

Seguido, en "UsersMetadataAdmin", reemplazamos el campo "user" por el método que hemos creado.

~~~
class UsersMetadataAdmin(admin.ModelAdmin):
	list_display = ('id', formularios.set_user, 'telefono')
~~~

Entonces, si hay un "first_name" y un "last_name" en el registro, se mostrará eso en el campo correspondiente. Esto mismo se puede usar para, por ejemplo, formatear fechas, para que así salgan siempre de la misma forma (por ejemplo, en formato año/mes/día). Lo hice y me funcionó. Lo hice así:

`from datetime import date`

~~~
def set_birth_date(obj):
	return obj.fecha_nacimiento.strftime('%Y/%m/%d')

set_birth_date.short_description = "Fecha de nacimiento"
~~~

Cuando ingresemos los modelos de categoría y producto, podríamos querer habilitar en el panel la opción de modificar la categoría en un producto creado, directamente en el listado. Se puede hacer dentro del archivo "admin.py", pero como creamos un módulo "utilidades", lo vamos a hacer ahí:

~~~
def set_categoria_con_link(obj):
	return format_html(f"""<a href='/admin/home/categoria/{obj.categoria_id}/change/' target='_blank'>{obj.categoria}</a>""")

set_user.short_description = "Categoría"
~~~

Cambiamos el campo correspondiente en el archivo "admin":

~~~
class ProductoAdmin(admin.ModelAdmin):
	list_display = ('id', formularios.set_categoria_con_link, 'nombre', formularios.set_date, 'descripcion')
	search_fields = ('nombre', )
~~~

Entonces, cuando pulsamos sobre la categoría del producto, nos abre una nueva ventana hacia esa categoría. **Nota:** se renombra la categoría a nivel global. Si se cambia, se cambia el registro de la tabla de categorías, no solamente la categoría para un producto puntual.

El instructor recomienda sacarle el mayor provecho posible al panel de administración. 

Si tratamos de eliminar una categoría, no nos va a dejar. Va a arrojar un error de integridad.

Existe una herramienta para personalizar el panel de administración que se llama django-jazzmin, que se instala con pip. Aplica temas de estilo para el panel de administración.

Hay muchos componentes externos que se puede añadir al panel, incluidos archivos JavaScript, pero utilizar muchos en un proyecto lo puede hacer muy pesado.

---


### 50. Manejo de imágenes en el backend

No es llegar y subir imágenes.

Django utiliza un directorio "media/" para manejar imágenes, y lo crea cuando subimos una imagen, por ejemplo, por medio de un formulario. Esto es, en el caso de que no hayamos definido previamente un directorio para esos archivos. En "settings.py" habíamos establecido constantes para el directorio "media". Creamos un directorio con ese nombre dentro del directorio base del proyecto.

Hay proveedores de hosting que manejan los directorios multimedia aparte del código Python. En esos casos hay que realizar procedimientos especiales. Como muestra, vamos a crear un archivo dentro de "utilidades" de nombre "dreamhost.py". Esto es, para simular lo que tendríamos que hacer en el caso de que subamos un proyecto a Dreamhost. Inicialmente vamos a importar lo siguiente:

~~~
import os
import shutil
from home.models import *
from django.conf import settings
from pathlib import Path
from datetime import datetime, date, timedelta
~~~

Una opción sería crear un campo para subir imágenes a la base de datos, pero eso termina consumiendo muchos recursos. Otra opción es crear un campo de tipo texto donde escribimos el nombre de la imagen, y guardamos la imagen en un directorio público, accesible desde nuestras plantillas. No siempre, ya que hay veces en que vamos a tener que guardar imágenes en nuestra base de datos; por ejemplo, cuando usemos un lector biométrico.

El instructor recomienda cambiar el nombre a las imágenes cuando se suban, ya que puede ocurrir que dos usuarios suban imágenes con el mismo nombre, y eso puede provocar algún tipo de conflicto.

Luego, creamos un campo para imágenes en nuestro modelo Producto:

`foto = models.ImageField(upload_to = "producto", default = "default.png")`

Es recomendable especificar un directorio en "upload_to", y que ese directorio tenga que ver con el modelo. En este caso es fácil, porque como son productos, elegimos un subdirectorio llamado "productos". Esto ahorra a la larga muchos dolores de cabeza. También es recomendable tener una imagen por defecto.

Si quisiéramos tener más de una imagen por producto, nos convendría crear una tabla para imágenes.

https://via.placeholder.com/(anchoxalto) es una página web para crear placeholders genéricos rápidamente. Por ejemplo: https://via.placeholder.com/400x400 crea un placeholder de 400x400. 

En nuestro directorio de archivos estáticos creamos uno de nombre "uploads". Dentro, un directorio por cada uno de los campos donde vamos a subir imágenes. En este caso, un directorio de nombre "producto". **NOTA:** No es este directorio en donde se van a subir las imágenes del campo de la tabla, porque esas imágenes se van a subir dentro de "/media".

Además, tenemos que instalar la librería Pillow, que es una librería usada en Python para trabajar con imágenes.

`pip install pillow`

Cuando hagamos la migración correspondiente y levantemos el servidor, al crear un nuevo producto en el panel de administración nos saldrá automáticamente un botón para subir una imagen.

La idea es que ahora podamos mostrar la foto en el panel de administración. Primero vamos a nuestra clase ProductoAdmin, y en la constante "list_display" añadimos el campo "foto". Al volver a cargar, nos va a mostrar el nombre del archivo, pero al tratar de abrirlo nos va a mostrar un error. El programa busca el archivo en un directorio "media".

Dentro del archivo "formularios" creamos un método:

~~~
def foto_producto(obj):
	return "mi foto"

foto_producto.short_description = "Foto"
~~~

Momentáneamente la dejamos así.

Luego, vamos al archivo "dreamhost.py" y creamos unos métodos:

~~~
RUTA = settings.RUTA
RUTA2 = settings.RUTA2


def existeArchivo(carpeta, archivo):
	try:
		ruta = f"{RUTA}ejemplo_1/static/uploads/{carpeta}/{archivo}"
		fileObj = Path(ruta)
		return fileObj.is_file()

	except Exception as e:
		return False


def existeArchivoMedia(archivo):
	try:
		ruta = f"{RUTA}ejemplo_1/media/{archivo}"
		fileObj = Path(ruta)
		return fileObj.is_file()
	except Exception as e:
		return False


def moverArchivoProducto(archivo, id):
	if existeArchivoMedia(archivo):
		fecha = datetime.now()
		nombre = f"{datetime.timestamp(fecha)}{os.path.splitext(str(archivo))[1]}"
		shutil.move(f"{RUTA}ejemplo_1/media/{archivo}", f"{RUTA2}static/uploads/producto/{nombre}")
		Producto.objects.filter(pk=id).update(foto=nombre)
~~~

Los dos primeros métodos los vamos a usar para que el programa busque archivos en la ruta de archivos estáticos y en la ruta "media". El método "moverArchivoProducto" comprueba si existe el archivo en "static/", le pone un nombre nuevo y lo mueve al directorio "media/". Finalmente, busca por id el registro en la tabla y actualiza el valor de "foto".

Para poder utilizar esos métodos, se debe importar el módulo "dreamhost" en "formularios.py":

`from utilidades import dreamhost`

Modificamos la función "foto_producto":

~~~
def foto_producto(obj):
	if dreamhost.existeArchivo('producto', obj.foto) == False:
		dreamhost.moverArchivoProducto(obj.foto, obj.id)
	return format_html(f"""<a href='/static/upload/producto/{obj.foto}' target='_blank'>
		<img src='/static/upload/producto/{obj.foto}' width='100' height='100'/></a> """)

foto_producto.short_description = "Foto"
~~~

Con lo anterior, el administrador debería ser capaz de procesar la imagen.


---

### 51. Eventos de modelos con Signal

Hay un módulo de Django que se llama django-signal. Permite ejecutar eventos que esperan (escuchar) que ocurran ciertas cosas en los modelos. Podríamos hacer, por ejemplo, que se cree un registro cuando añadimos un registro nuevo en una tabla (como crear un producto nuevo); o podríamos enviar un correo electrónico; crear un archivo de texto, etc. 

Es bastante útil ya que nos permite tener trazabilidad en proyectos grandes. **SPOILER:** Parte del éxito en nuestros proyectos está en la trazabilidad. Si nuestro programa no es capaz de saber quién hizo algo, cuándo se hizo o dónde se hizo, nuestro programa no tendrá mucho futuro.

Para usar Signal, la importamos en nuestro archivo de modelos con algunos módulos:

`from django.db.models.signals import post_save, pre_save, pre_delete`

Importamos también un decorador:

`from django.dispatch import receiver`

Vamos a detectar cuando se creee un registro en una tabla, cuando se modifica o cuando se elimina, y lo vamos a hacer a través del decorador "receiver". Esto permite entender cómo funcionan los decoradores.

Creamos un modelo y lo añadimos luego al admin:

~~~
class Tracking(models.Model):
	descripcion = models.TextField()
	fecha = models.DateTimeField(auto_now = True)

	def __str__(self):
		return self.descripcion

	class Meta:
		db_table = "tracking"
		verbose_name = "Tracking"
		verbose_name_plural = "Trackings"
~~~

La idea ahora es ejecutar funcionalidades cuando se agregue, modifique o elimine un registro. Vamos a hacer lo siguiente en la tabla de productos:

~~~
@receiver(post_save, sender=Producto)
def producto_save(sender, instance, **kwargs):
	if kwargs['created']:
		Tracking.objects.create(descripcion = f"Se creó el producto {instance.id}")
~~~

El decorador "receiver" recibe como parámetros el método signal (en este caso "post_save" que es cuando se crea un registro) y un sender, que en este caso es el modelo con el que va a trabajar (Producto).

La función recibe como parámetros el sender, una instancia, que es el objeto creado, y `**kwargs`. Lo que hace la función que hemos creado es revisar si se ha "creado" un registro, y luego crear un registro en el modelo Tracking, guardando en ese registro el id del registro creado en el modelo Producto. 

Lo anterior es un equivalente a los triggers de las bases de datos, y tiene un potencial enorme.

Creamos una función ahora para cuando se edite un registro. En este caso, tenemos que pasar un parámetro adicional, ya que recibe lo que ya hay en un registro:

~~~
@receiver(pre_save, sender=Producto)
def producto_change(sender, instance: Producto, **kwargs):
	if instance.id is None:
		pass
	else:
		previous = Producto.objects.get(id = instance.id)
		if previous.nombre != instance.nombre:
			Tracking.objects.create(descripcion = f"Se modificó el producto {instance.id}")
~~~

El condicional que va anidado en el else es opcional. Solo corrobora si se modificó el nombre del registro.

Ahora vamos a crear una función para cuando se elimine un registro:

~~~
@receiver(pre_delete, sender = Producto)
def producto_delete(sender, instance: Producto, **kwargs):
	if instance.id is None:
		pass
	else:
		Tracking.objects.create(descripcion = f"Se eliminó el producto {instance.id}")
~~~

Maravilloso.

**Nota:** Al eliminar un producto, su imagen no se elimina. Se puede programar en receiver de eliminación, utilizando la librería os, para eliminar el archivo.
