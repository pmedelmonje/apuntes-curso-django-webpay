## 41. Introducción técnico teórica

Django está hecho específicamente para hacer programas web. Lo que haríamos en un programa de escritorio con TkInter se hace con HTML.

Es un framework de "desarrollo ágil", lo que quiere decir que con pocas líneas de código podemos hacer operaciones amplias. Por ejemplo, manejo con bases de datos.

#### Elementos que componen Django:

- Patrones de desarrollo
- Herramientas de autentificación
- Herramientas de validación y serialización de datos
- Manejo y administración de URLs
- Herramientas de manejo de persistencia temporal
- Herramientas de administración de persistencia remota
- Herramientas de abstracción de datos (ORM)
- Herramientas de gestión de persistencia fija
- Internacionalización
- Seguridad básica

En la documentación recomiendan trabajar con MySQL y PostgreSQL.

#### Patrones que coexisten en Django

> El peor error que se puede cometer es ir a escribir código antes de entender cómo funciona la tecnología que estamos aprendiendo.

Los patrones más conocidos son:

- **PSR-4**: Permite manejar el autocarga (autoload). El autocarga consiste que, a través de un solo archivo o vista puede cargarse todo el proyecto.

- **Builder**: Va construyendo todo en tiempo de ejecución. Eso es muy útil y no debe olvidarse. Todo lo que ocurre en una app web ocurre en tiempo de ejecución. El framework no almacena más que lo que uno mismo ha creado.

- **Singleton**: Permite centralizar la conexión a  la base de datos que se va a utilizar. O sea, tiene una conexión única a la base de datos que permite utilizarla a lo largo de todo el proyecto. **El instructor recomienda no usar más de una base de datos por temas de seguridad, e indica que si llega a ser necesario conectarse a más de una BD, es porque la aplicación está mal planteada**. Igualmente, si una tabla tiene más de 50 campos, también la app está mal planteada. 

- **Decorator**: Esto es como decir "inyección de dependencias". Se usa mucho en los login; no solo cuando se quiere corroborar si un usuario ha iniciado sesión, sino que también si un usuario tiene permisos de acceso.

- **ORM**: Básicamente se crea un modelo por cada tabla de la base de datos, que se van a conectar entre sí por medio de claves foráneas. También permite crear eventos, para que, por ejemplo, se realice una acción al crear un registro. **NOTA:** Los modelos en local no funcionan de la misma forma que en producción. Hay que manejar las migraciones de cierta forma para no tener problemas.

- **Template Layout**: Está muy asociado al modelo **Vista Template**.


#### "Helpers" para la implementación del DOM (Document Object Model)

El DOM tiene que ver con todo lo que se carga en una página web. Está compuesto por JavaScript, CSS, HTML y todas las herramientas que se cargan en el navegador. 

Todo lo que se escribe en Django va a funcionar bien en el navegador. Si algo no funciona, no es culpa del framework, sino porque hubo algo que escribimos mal. La compatibilidad con los distintos navegadores aún siguie puede ser un problema hoy en día para los programadores frontend, aunque en menor medida.


---

#### PSR4 Autoload

- Carga inicial del entorno principal
- Inyección de dependencias
- Colaboración de objetos
- Muestreo y renderización de recursos
- Inicialización de bibliotecas principales

Es importante entender en qué momento se carga cada componente para saber cómo tenemos que tomar decisiones con el código.

Por ejemplo, se puede incrustar trozos de código dentro de nuestras plantillas, de forma desacoplada. Esto nos permite, por ejemplo, aplicar estilos CSS solo en algunas vistas.

---

#### Modelo Vista Template

La **Vista** es el manejador de datos. A través de las vistas nos conectamos al DOM, y recién ahí cargamos los datos.

Por ejemplo, si tenemos una página donde cargamos los productos, tenemos una vista, en un archivo Python, que va a tener un método de clase o función, que va a estar conectado a una ruta que va a recibir una petición de un método HTTP (por lo general una GET). Luego, eso va a renderizar información a través del ORM, y después de haber formateado toda la información, se muestran los datos.

Sin embargo, va a haber casos en que necesariamente se va a tener que hacer consultas a la base de datos en las vistas. En Django no se puede hacer eso, pero da la opción de implementar una "custom template tag" para que se pueda desacoplar esas consultas y no romper el esquema de implementación.

---

#### Object-Relational Mapping

Lo que hace es, básicamente, procesar los datos de la base de datos y devolverlos en forma de listas de Python.

---

#### Reglas de validación

Django trae bastantes funciones para "sanitizar" datos. Trae funciones, por ejemplo, para manejar mensajes flash, validar email, etc.

---

### 42. Instalación y primeros pasos

El instructor trabaja con una base de datos MySQL y usa phpMyAdmin. Costó harto, pero se pudo al final. Creé una BD llamada "curso-django".

Se accede así al phpmyadmin: http://localhost/phpmyadmin/index.php.

Creamos luego el entorno virtual para instalar Django ahí. En el curso instalan puntualmente la 3.0.5 con `pip install Django==3.0.5`, pero yo instalé la más reciente.

Acto seguido, instalar algunos paquetes para trabajar con MySQL.

`sudo apt install python-dev default-libmysqlclient-dev`


Instalar después `pip install mysqlclient` (a veces falla)

Luego, creamos el primer proyecto de Django:

`django-admin startproject ejemplo_1`

Al crear el proyecto, crea un directorio con el nombre, y dentro un módulo con el mismo nombre que el proyecto.

Se hace una explicación sobre el archivo "settings.py" y "urls.py".

Dentro de "settings.py", hay una variable DEBUG que ajusta si el programa se va a ejecutar en modo prueba (True) o en modo de producción (False).

En ALLOWED_HOSTS se informan los servidores, de modo que Django los reconozca y no tengamos un error 400 o 500 al momento de levantar el servidor. Una opción es colocar `"*"` dentro de la lista para que ejecute todo.

En INSTALLED_APPS colocamos las aplicaciones que creamos con `django-admin startapp`.

En MIDDLEWARE se registran todos los recursos que se van a cargar de forma automática, sin que el usuario lo requiera. Hay, por ejemplo, un middleware de seguridad, uno de autentificación, etc.

En TEMPLATES configuramos las plantillas que vamos a utilizar en nuestro proyecto.

En DATABASES configuramos la base de datos que vamos a utilizar en el proyecto. Para efectos del curso, vamos a dejar comentado todo el diccionario. 

~~~
'''
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': BASE_DIR / 'db.sqlite3',
    }
}
'''
~~~

Luego, importamos las configuraciones para MySQL:

~~~
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'curso-django',
        'USER': 'phpmyadmin',
        'PASSWORD': 'gastroll3791',
        'HOST': 'localhost',
        'PORT': '3306',
        'OPTIONS': {
            'autocommit': True,
        }
    }
}
~~~

(Al levantar el servidor, no me funcionó con el usuario root, seguramente porque no tiene los suficientes permisos. Con el usuario "phpmyadmin" funcionó)

LANGUAGE_CODE contiene la información regional y de idioma que va a utilizar Django. Por defecto es "en-us" (Inglés de Gringolandia). Podemos cambiarla a Español de Chile con "es-cl".


STATIC_URL determina el directorio de los archivos estáticos (CSS, imágenes, JS).

---

Levantamos el servidor con `python3 manage.py runserver`

Si queremos levantar el servidor con una IP específica, para que pueda ser accedida desde otro dispositivo, tenemos que corroborar primero la IP de nuestro dispositivo con `ifconfig` en Linux (en el preciso instante de escribir esto, la IP es 192.168.100.39:8080). Luego, escribimos 

`python3 manage.py runserver 192.168.100.39:8080`

Django reconoce la IP, pero como no está en ALLOWED_HOSTS, no podemos acceder. Registramos entonces la IP en esa constante y volvemos a intentar.

---

**NOTA:** Siempre que tengamos errores y tengamos el modo DEBUG activado, Django nos va a mostrar detalles de nuestro entorno. Cuando pasemos el proyecto a producción, lo ideal es tenerlo desactivado para que solo muestre un error genérico. Así los hackers no tienen información de más.

---

### 43. Implementación de DOM

El proyecto, tal como está, ya se puede desplegar, pero se puede hacer unas cuantas configuraciones más para darle forma. Sin embargo, la prioridad en este curso va a ser la funcionalidad. 

Elegir si voy a usar Bootstrap o alguna plantilla de https://www.free-css.com/. Por el momento no usar CSS puro para no tardarme tanto en aplicar estilos básicos.

Dentro de "settings.py" personalizamos la localización del directorio de archivos estáticos. Creo un directorio "static" dentro del directorio base del proyecto. Importamos la librería os dentro del archivo, y luego creamos una constante de nombre "STATICFILES_DIRS", que tiene que ser una tupla:

~~~
STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'static'),
    )
~~~

Igualmente creamos constantes para un directorio "media":

~~~
MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
~~~

Adicionalmente, creamos un directorio "templates", y en el archivo "settings", en la constante TEMPLATES, hay una clave de nombre "DIRS", en donde pondremos:

`'DIRS': [os.path.join(BASE_DIR, 'templates')],`

Aún con lo anterior falta hacer un "hola mundo". Creamos ahora una aplicación dentro de nuestro proyecto de ejemplo.

`django-admin startapp home`

Se crea un directorio llamado "home" que contiene una app del mismo nombre. 

La arquitectura de Django indica que existe una estructura central llamada proyecto, que puede estar compuesta por una o más aplicaciones con características propias, que son reutilizables y se puede traspasar a otros proyectos.

Una app incluye un archivo para hacer un despliegue de pruebas, un archivo para configurar el modelo de la base de datos, un archivo para configurar las vistas, entre otros. El directorio del proyecto tiene un archivo de rutas "urls.py". Lo que se recomienda es crear un archivo "urls.py" en cada aplicación. Este archivo se puede incluir en el principal de la siguiente manera: de django.urls, importamos el módulo `include`. Dejamos así el archivo:

~~~
from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('home.urls')),
]
~~~

Con `include`, incluímos el archivo "urls" que se encuentra dentro del subdirectorio "home". 

En el archivo "urls" de la nueva aplicación importamos path y creamos una lista "urlpatterns", donde importaremos todas las vistas de esa app, así que igualmente importamos esa ruta. El instructor importa de las vistas de a una, pero yo puse un * para importar todo.

~~~
from django.urls import path
from .views import *

urlpatterns = [
	path('', homeStart, name="home"),
]
~~~

El primer parámetro de "path" es la ruta de la URL; el segundo parámetro es la vista que se va a llamar, y el tercer parámetro es el nombre de la ruta, con la cual se puede llamar dentro de la plantilla.

En el archivo "views.py" hacemos unas importaciones iniciales:

~~~
from django.shortcuts import render
from django.http import HttpResponse, Http404
~~~

Empezamos creando una vista basada en funciones:

~~~
def homeStart(request):
	return HttpResponse("holi")
~~~

El HttpResponse no es recomendado. Se puede usar, pero la idea es que sea solo para pruebas. Lo que normalmente se usa es "render". La sintaxis es así:

`return render(request, "plantilla.html", contexto)`

Siempre son 3 parámetros. El primero siempre es `request`. El segundo es la plantilla que se va a cargar para esa vista, y el tercer parámetro es un diccionario, que puede quedar vacío.

Vamos al directorio "templates" y creamos un subdirectorio "layout", dentro del cual crearemos una plantilla HTML que voy a llamar "base.html" (el instructor le pone otro nombre). Descargué Bootstrap y lo puse en el directorio "/static/lib". 

Igualmente creamos en un directorio por cada app con el nombre de esa app dentro de "templates". Dentro de templates/home", creamos un archivo por cada vista de esa app. En este caso, "home.html" para la vista "homeStart".

En la plantilla base, cargamos el directorio de archivos estáticos. Colocamos al principio del archivo: `{% load static %}`. Tiene que ser antes de que llamemos los elementos. Con eso, Django va a buscar el directorio que se haya configurado en el archivo "settings.py". Voy a incrustar Bootstrap dentro del archivo base:

En `<head>`:

`<link rel="stylesheet" type="text/css" href="{% static 'lib/bootstrap-5.2.1-dist/css/bootstrap.min.css'%}">`

Al final de `<body>`:

`<script type="text/javascript" src="{% static 'lib/bootstrap-5.2.1-dist/js/bootstrap.min.js'%}"></script>`

---

**Truco:** Si queremos mostrar el año actual en la plantilla, podemos usar el tag `{% now 'Y' %}`.

---

Dentro de nuestras plantillas, tenemos que ir indicando lo que queremos que se mantenga igual y qué es lo que va a cambiar. Por ejemplo, vamos a querer mostrar la barra de navegación superior en todas las plantillas. Podemos usar los {% block %} para colocar código específico para cada plantilla. En la plantilla base puse un {% block body %} (el instructor prefiere {% block content %}, pero yo estoy más acostumbrado a llamarle "body") dentro de la etiqueta `<body>`. Los "bloques" son trozos de código que se comparten entre plantillas.

También se recomienda un "block title" para cambiar de una forma más dinámica el título de cada plantilla, según lo que pongamos en las vistas.

~~~
<title>
{% block title %}
{% endblock %}
</title>
~~~

Otra práctica que recomienda el instructor es incrustar códigos de CSS específicos para cada vista. También tener plantillas de Free CSS y Bootstrap para "pelear y encontrar errores y luego corregirlos"; no tener un solo esquema de estilos.

---

### 44. Configuración de rutas

Antes de empezar, tengo que tener un estilo mínimo aplicado a mis plantillas. Como descargué Bootstrap, voy a hacer algo con eso.

En el archivo "urls.py" de la app "home", creamos una ruta nueva:

`path('aboutUs/', aboutUs, name = "aboutUs"),`

Seguido, creamos una vista en el archivo "views" para esa ruta. El nombre del método debe ser el mismo que el puesto en el path:

~~~
def aboutUs(request):
    return render(request, "home/about.html", {})
~~~

Esta ruta y vista creadas deben tener una plantilla a la cual dirigirse.

La ventaja de tener un archivo de rutas en cada app, desacoplado del directorio base, permite tener el código más ordenado, controlado y categorizado. Por ejemplo, si en una web queremos mostrar productos, podemos tenerlos todos dentro de una app "productos", crear un archivo de rutas al que se acceda escribiendo "/productos/producto" en el navegador.


En producción, cuando se trabaja con frameworks, los errores se muestran como "Error 500". Para configurar la página de error, creamos un archivo HTML con el número del error como nombre. Por ejemplo "500.html", "404.html", etc. Esos archivos pueden extender la plantilla base y se les puede aplicar estilos. 

NO VA A FUNCIONAR MIENTRAS TENGAMOS EL MODO DEBUG ACTIVADO.

A una ruta en el archivo de urls podemos pasarle parámetros, que pueden ser un id o un slug. Un slug es una URL con texto enriquecido. Hay un módulo en Django para crear un slug de forma automática.

Ejemplo de ruta con parámetros:

`path('aboutUs/<int:id>/<slug>', aboutUs, name = "aboutUs")`

En una vista basada en funciones, debemos pasarle los parámetros con el mismo nombre puesto en la ruta:

`def aboutUs(request, id, slug):`

Si quisiéramos utilizar esos parámetros, los incluimos en el diccionario del contexto:

~~~
def aboutUs(request, id, slug):
    return render(request, "home/aboutUs.html", {"id" : id, "slug" : slug})
~~~

Cada parámetro se asigna como un valor a una clave. En la plantilla, llamamos a ese parámetro según el nombre que le hayamos puesto a la clave.

Podemos incluir otros datos, variables, estructuras dentro del diccionario del contexto. Podemos declarar, por ejemplo, una lista dentro de la vista, e incluirla en el contexto de la misma forma.

Si quisiéramos llamar, por ejemplo, el id dentro de la plantilla, utilizamos `{{ id }}`.

--- 

#### Creación de enlaces

Los nombres que ponemos en las rutas nos sirven para llamar a esas URL dentro de las plantillas. Por ejemplo, la ruta '' tiene en nombre "home". En el "href" de un enlace, podemos colocar `{% url 'home' %}`. Para pasarle parámetros, siguiendo con el ejemplo del id y el slug: `{% url 'ruta' id 'slug' %}`. Los strings estáticos deben pasarse entre comillas, aunque normalmente vamos a pasar variables o índices, así que no va a ser nevesario. Va ser muy raro que no usemos datos dinámicos en una ruta.


---

### 45. Implementación de AJAX y plugins JS, ventanas modales, entre otras cosas.

Creamos una app nueva dentro del proyecto de ejemplo:

`django-admin startapp estilo`. El instructor le nombró "diseno" (por diseño), pero como yo soy choro pulento, le puse otro nombre. Recordar que se tiene que incrustar en INSTALLED_APPS en "settings.py". La incluimos igualmente en el archivo "urls" principal y creamos un directorio con ese nombre dentro de "templates".

Preliminarmente, la plantilla "home" en "estilo" será así:

~~~
{% extends 'layout/base.html' %}

{% block title %}Estilos{% endblock %}

{% block body %}

<div class="container">
    <h1>Estilos, diseños y herramientas varias</h1>
    <ul>
        <li>
            <a href="#">Ventana Modal</a>
        </li>
        <li>
            <a href="#">Jquery alert</a>
        </li>
        <li>
            <a href="#">Fancybox</a>
        </li>       
        <li>
            <a href="#">AJAX</a>
        </li>
    </ul>
</div>

{% endblock %}
~~~

---

#### Implementación del Modal

Implementamos un modal con Bootstrap. Se recomienda tener el código del modal en las plantillas base porque normalmente se usan en más de una plantilla.

~~~
<!-- Modal -->
<div class="modal fade" id="ventanaModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content" id="_ventanaModal">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
<!-- /Modal -->
~~~

Se asigna en el "modal-content" un id distinto al asignado al principio para que muestre distintos datos según la plantilla.

Los modales se ven solo cuando se llaman. Esto normalmente se hace con un botón, aunque basta con traer los parámetros de Bootstrap:

~~~
data-bs-toggle="modal" data-bs-target="#ventanaModal"
~~~

En el enlace de la ventana modal que tenemos en "estiloHome.html" colocamos los atributos para llamar la modal:

`<a href="javascript:void(0);" data-bs-toggle="modal" data-bs-target="#ventanaModal">Ventana Modal</a>`

Para que funcione, es necesario tener importado el JS de Bootstrap (ya sea con el CDN o con el archivo descargado).

En el caso de Bootstrap 5, ya tiene incorporado jQuery en el archivo JS, y por eso funciona el modal sin tener que importar jQuery, pero en versiones anteriores de Bootstrap hace falta importar jQuery aparte.

---

#### jquery-alerts

**NOTA:** Hay mucha gente que dice que jQuery está muerto, especialmente los programadores frontend; pero para los que trabajan con frameworks de backend, puede llegar a ser muy útil.

Hay librerías en Python para trabajar con librerías JavaScript, incluido jQuery, pero según el instructor se dan muchas vueltas, y él prefiere trabajar "a mano". Hace una prueba con una librería llamada "django-jquery" y no le resultó.

El instructor va a usar también jQuery y jquery-alerts. Descargué jQuery, pero no encontré jquery-alerts.

El enlace para jquery-alerts va a quedar pendiente. Pedí los archivos JS y CSS de esa librería al instructor.

---

#### Fancybox

El instructor implementa Font Awesome igualmente, pero como descomprimido ocupa más de 20mb y me va a tomar más tiempo subir los elementos a un repositorio, voy a usar el CDN por el momento.

Vamos a colocar un bloque para archivos CSS que sean solo para plantillas en particular, dentro de la etiqueta "head" de la plantilla base, y un bloque para archivos JS en lo más bajo de "body". Luego, en el archivo "estiloHome" vamos a colocar un "bloque css" y un "bloque js" para incrustar Fancybox. Traté de importar el CDN igualmente, pero no funcionó, así que descargué la librería, que no ocupa tanto espacio:

~~~
{% block css %}
<!-- Fancybox -->
<link rel="stylesheet" href="{% static 'lib/jquery.fancybox-1.3.4/fancybox/jquery.fancybox-1.3.4.css' %}" media="screen" />
{% endblock %}
{% block js %}
<!-- Fancybox -->
<script type="text/javascript" src="{% static 'lib/jquery.fancybox-1.3.4/jquery-1.4.3.min.js' %}"></script>

<script type="text/javascript" src="{% static 'lib/jquery.fancybox-1.3.4/fancybox/jquery.fancybox-1.3.4.pack.js' %}"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $(".fancybox").fancybox( {
            openEffect: 'none',
            closeEffect: 'none',
        });
    });
</script>
{% endblock %}
~~~

---

#### AJAX (Asynchronous JavaScript And XML)

AJAX permite hacer peticiones asíncronas, que no requieren que se cargue toda la página. Eso nos permitiría, por ejemplo, embeber código HTML de una página dentro de otra sin tener que refrescar el servidor. En el caso de Django, una vista. Creamos una vista para trabajar con un ejemplo de AJAX. La ruta para la vista va a ir con un parámetro. No es necesario en este ejemplo, pero va a servir para mostrar cómo funciona con parámetros:

`path('ajax/<int:id>', estiloAjax, name = "estiloAjax"),`

La vista queda así:

~~~
def estiloAjax(request, id):
    return render(request, "estilo/estiloAjax.html", {'id': id})
~~~

Creamos la siguiente función en el archivo "funciones.js":

~~~
function carga_ajax_get(ruta, valor1, div) {
    $.get(ruta, { valor1: valor1 }, function(resp) {
        $("#" + div + "").html(resp);
    });
    return false;
}
~~~

Dentro de la plantilla "estiloHome" modificamos el "li" de AJAX y colocamos un contenido que se va a cambiar después:

~~~
<li>
    <a href="javascript:void(0);" onclick="carga_ajax_get('{% url "estiloAjax" 123 %}', '123', 'contenido_ajax');">AJAX</a>
</li>
</ul>
<hr />
<div id="contenido_ajax">Contenido inicial</div>
</div>
~~~

Colocamos un código sencillo en el archivo "estiloAJAX.html" como demostración:

~~~
{% extends 'layout/base.html' %}
{% load static %}

{% block body %}

respuesta AJAX {{id}}

{% endblock%}
~~~

Al probar y pulsar el link de AJAX, se reemplaza el contenido inicial por el contenido de la plantilla "estiloAjax". Eso sí, carga todo el contenido de la plantilla. El instructor recomienda tener una plantilla base especialmente para las peticiones AJAX, que contenga solamente un bloque de contenido. Los archivos que usemos con AJAX deberían extender el archivo "layout/ajax.html" en vez del "base.html".

---

Modificamos la modal para hacerla dinámica. Creamos una vista para la modal:

~~~
def estiloModal(request, id):
    return render(request, "estilo/estiloModal.html", {'id': id})
~~~

La plantilla "estilo/estiloModal.html" también va a heredar de "layout/ajax.html", y nos vamos a llevar el contenido desde el "div modal-header" que hay en la plantilla base:

~~~
{% block body %}

<div class="modal-header">
    <h5 class="modal-title" id="exampleModalLabel">Título</h5>
    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
</div>
<div class="modal-body">
    Texto de prueba. Holi, ¿tení pololi?
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
    <button type="button" class="btn btn-primary">Guardar</button>
</div>
</div>

{% endblock%}
~~~

El id que le pusimos al "modal-content", que es distinto al id del "div" global de la modal, nos va a servir para refrescar con AJAX. Vamos al "li" donde se carga la modal y modificamos el link:

~~~
<li>
<a href="javascript:void(0);" data-bs-toggle="modal" data-bs-target="#ventanaModal" onclick="carga_ajax_get('{% url "estiloModal" "123" %}', 'ss', '_ventanaModal')">Ventana Modal</a>
</li>
~~~

Al volver a cargar, cuando pulsamos el link para la modal, hace la petición AJAX y muestra lo que colocamos en el código.






