### 67. Instalación, configuración de Django Rest. Crear API Rest con request y response

Para acceder a una base de datos, se ha vuelto de moda que los programas se conecten a una API administrada desde un backend, no a la base de datos directamente. Un programador backend DEBE saber crear APIs completamente funcionales. Las API, por lo general, son consumidas por las ĺibrerías y frameworks frontend, como Angular. Django también tiene herramientas consumir una API.

Django en su núcleo no tiene soporte nativo de APIs, porque su consigna es desacoplar todo. Por ende, ofrece un módulo completo para trabajar con APIs, que es el "microframework" Django Rest framework, que contiene todas las herramientas que vamos a necesitar y es fácil de usar.

Instalamos Django REST framework con pip:

`pip install djangortframework`

La documentación también recomienda instalar (aunque es opcional):

~~~
pip install markdown
pip install django-filter
~~~

Toda aplicación carga a través del método HTTP "GET". Cuando enviamos datos, por ejemplo, por medio de un formulario, usamos el método "POST". Hay distintos métodos HTTP que se pueden usar en una API.

Añadimos Django REST en nuestro archivo "settings": `'rest_framework'`.

Para no tener problemas, podemos añadir al archivo (puede ser debajo de MIDDLEWARE) el siguiente código, que va a permitir desactivar un poco la autenticación por el "crsf_token" automatizada, dando prioridad a la autenticación via token.

~~~
REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': [
    'rest_framework.authentication.TokenAuthentication',]
}
~~~

Esto asegura que las API funcionen de forma correcta, aunque no es del todo indispensable. Por el momento vamos a dejarla comentada.

Vamos a crear una API funcional:

`django-admin startapp api`

Luego, la agregamos tal como cualquier otra app, en el archivo de urls principal:

`path('api/', include('api.urls'), name="apiUrls"),`

Por convención, se le suele poner la versión a la API que estamos creando. Por ejemplo:

`path('api/v1/', include('api.urls'), name="apiUrls"),`

Creamos dentro del directorio de la app, el archivo "urls.py" y creamos una ruta inicial. Cuando hablamos de rutas en las APIs, nos referimos a ellas como "endpoints".

Cuando usamos APIs, ya no usamos render, y el instructor recomienda usar vistas basadas en clases para las APIs. Importamos:

`from rest_framework.views import APIView`
`from rest_framework.response import Response`

Las APIs tampoco utilizan recursos gráficos, así que tampoco tenemos que hacerles templates. Solo retornan información. 

La vista:

`class Class_Test(APIView):
	pass`

La ruta:

`path('test/', Class_Test.as_view(), name="api_test"),`

Ponerle un "name" a la ruta tampoco es muy indispensable que digamos.

**NOTA:** "from django.conf.urls import url" fue removido en Django 4. Ahora se usa `from django.urls import re_path`, y podemos colocarle `as url` por si se nos complica mucho.

Al cargar la ruta por primera vez, nos muestra una ventana propia del microframework mostrando un HTTP 405 "Method not allowed", y más abajo un JSON con un `"detail":"Método \"GET\" no permitido."`. Es muestra de que funciona.

Las APIs funcionan como URLs que no vamos a visualizar como cuando cargamos una plantilla. El microframework muestra una interfaz gráfica solamente para tener una interacción mínima, para cosas puntuales.

Las APIs siempre retornan un estado HTTP. Para probar las APIs, el instructor usa Postman, pero como a mí nunca me funciona, voy a usar Insomnia.


---
**IMPORTANTE:** Hay una convención en el mundo de las APIs, que en el mundo laboral la suelen pedir. Cuando construimos una API para listar información, la idea es que el método que se utilice para consultar esa información sea el método GET. Podríamos configurar nuestra API para que funcione con un solo método, pero la convención dice que:

- el método GET sea únicamente para consultas de información. 

- POST debería usarse solo para crear registros, aunque eso pueda hacerse con otros métodos.

- PUT para modificar registros.

- DELETE para eliminar registros.

---

Probamos haciendo una petición GET a la misma ruta `http://localhost:8000/api/v1/test/` en Insomnia y nos devuelve el mismo JSON que nos devolvía el front del framework:

~~~
{
	"detail": "Método \"GET\" no permitido."
}
~~~

Independiente del texto del JSON, la idea es siempre fijarnos en el *status* de la petición que nos aparece en la parte superior del "body" de la ventana (en este caso un "405 Method Not Allowed").

El formato de la url al que hacemos una petición es estándar, y lo vamos a ver en cualquier framework o librería de backend.

Volviendo al código, el trabajar con vistas basadas en clases nos permite utilizar muchos métodos específicos que incluye Django que facilitan el trabajo. Es posible crear una API con Django sin usar Django REST, pero no es buena idea.

---

Creamos un método dentro de la clase para las peticiones GET:

~~~
def get(self, request):
	return Response({"mensaje":"Holi. ¿Tení pololi?"})
~~~

Para las peticiones GET, el método SIEMPRE debe llamarse get(). Ahora retornamos un "Response", que debe ser un JSON dentro de un diccionario Python.

Al volver a hacer la petición GET en Insomnia, nos muestra un JSON con la información que pusimos en el diccionario, con un *status* 200 (exitoso).

Podríamos devolver un resultado que no sea JSON; podemos retornar un HttpResponse, escribir un texto, y eso será mostrado en formato HTML; pero lo habitual es trabajar con JSON.

También podemos forzar un estado, aunque la petición sea exitosa. Por ejemplo:

~~~
res = HttpResponse("texto")
res.status_code=401
return res
~~~

Si hacemos eso, el programa nos va a mostrar el texto del HttpResponse correctamente, pero nos va a mostrar un *status* 401. Esto nos puede servir, por ejemplo, cuando queremos hacer una verificación de información que recibamos.

---

Vamos a crear un método para cada una de las otras peticiones:

~~~
def post(self, request):

	return Response({"mensaje":"Respuesta POST"})


#petición PUT
def put(self, request):

	return Response({"mensaje":"Respuesta PUT"})


#petición DELETE
def delete(self, request):

	return Response({"mensaje":"Respuesta DELETE"})
~~~

Esta es una configuración muy básica para una API desde Django REST.

Creamos una nueva URL para probar la funcionalidad con parámetros.

`path('test-parametros/<int:id>', Class_TestParametros.as_view(), 
		name="api_test_parametros"),`

Siempre que trabajemos con parámetros, estos solo se requieren en el método GET. Por convención, no tendría sentido pasar los parámetros en los otros métodos. El parámetro queda definido dentro del método, tal como cuando pasamos parámetros en las vistas basadas en funciones.

~~~
class Class_TestParametros(APIView):
	#petición GET
	def get(self, request, id):
		return Response({"mensaje":f"id={id}"})
~~~

Normalmente vamos a crear API Rest solo para recuperar información. Si en el ejemplo anterior hubiéramos hecho una petición POST, nos habría dado problemas por el parámetro que estábamos ingresando.

---

Vamos a crear ahora otra ruta y otra vista, para probar otra forma de pasarle parámetros a las APIs. Normalmente usamos la manera de arriba, con el id, para consultar un registro o eliminarlo, y que es una forma muy usada. La siguiente forma también es muy usada; funciona con cualquier método que no sea GET.

`path('test-request/', Class_TestRequest.as_view(), 
	name="api_test_request"),` 

La vista solo va a tener un método POST. Podemos tener una clase para cada método. Como sea, se recomienda implementar todos los métodos para que no nos salgan los típicos errores de Python o Django, que puede llegar a usar un hacker. La idea es tener declarados todos los métodos con mensajes personalizados, aunque haya métodos que no vamos a usar, solo por motivos de seguridad.

Si quisiéramos enviar datos como si fuera un formulario, en el programa que estamos usando para consumir la API tendríamos que seleccionar el método POST para que podamos elegir el "body", y ahí seleccionar la opción correspondiente (en Insomnia no sale igual que en Postman), aunque la vamos a usar solamente cuando creemos una API donde subamos archivos. Para lo demás, seleccionamos la opción "raw", y en el tipo de contenido, elegimos "application/json".

Ahora, a nuestra API, le pasamos un cabecero. Se le llama así porque se le pasa en el mismo orden de prioridad que la ruta, antes que baje al navegador. Vamos a pasar un parámetro en formato JSON. Escribimos un JSON sencillo, con pocos nodos, al "body" de la petición.

~~~
class Class_TestRequest(APIView):

	#pasar parámetros vía JSON request
	def post(self, request):

		return Response({"mensaje":"hola"})
~~~

O sea, funciona. Cuando enviamos el JSON nos retorna el mensaje puesto en el Response(), pero predeterminadamente no podemos ver los parámetros que enviamos. Para recuperar esos datos, la forma estándar que sugiere Django es:

`return Response(request.data)`

Para acceder a uno solo de los datos del JSON, llamamos a ese dato en el return. Por ejemplo, si en el JSON que mandamos tenemos una clave "nombre", tendríamos que escribir:

`return Response({"nombre": request.data.get("nombre")})`

Hacemos otra prueba consumiendo la API directamente desde Python.

Creamos un archivo .py que contenga el siguiente código:

~~~
import requests

url = "http://localhost:8000/api/v1/test-request/"

payload = {
            "id": 1,
            "nombre":"Pedro",
            "correo":"asdf@holi.com"
            }

response = requests.post(url, data=payload)

print(f"Estado petición HTTP={response.status_code}\nrespuesta={response.text}")
~~~

Al ejecutar, nos funciona perfectamente.

Las APIs no son solamente para consumirlas desde Postman o Insomnia. Hay veces en que funcionan en estos programas, pero no funcionan en los lenguajes de programación donde las vamos a consumir. Por lo general fallan cuando les pasamos algunos parámetros, así que hay que estar probando las APIs en el lenguaje de programación de vez en cuando.

---

### 68. Implementación de JWT (JSON Web Token)

Instalamos `pip install jwt`. Si llega a dar un error, podemos instalar `pip install PyJWT`. 

Ahora vamos a nuestra librería de "utiliades" para crear un método:

`import jwt`

~~~
def getToken(json):
	token = jwt.encode(json, settings.SECRET_KEY,
		algorithm='HS256')
	return token
~~~

Para el ejemplo, vamos a usar la misma clave secreta que provee Django al crear el proyecto. El token puede ser cualquier combinación de caracteres, incluso "1234", pero la clave de Django es bastante segura.

Vamos a las vistas de "api" e importamos la librería de utilidades:

`from utilidades import utilidades`

Para probar que el método funciona, vamos a hacer una ruta para un login.

`path('test-login/', Class_TestLogin.as_view()),`

Normalmente en la vista, se coloca un documentación con la información requerida en el JSON para iniciar sesión, para que un desarrollador que tenga que trabajar con esta API sepa qué JSON debe pasarle.

Importamos en "views":

`from django.contrib.auth import authenticate, login`
`import time`

~~~
class Class_TestLogin(APIView):

	"""
	request: {"usuario":"admin", "password":"gastroll3791"}

	"""

	def post(self, request):
		data = request.data
		if data.get("usuario") == None or data.get("password") == None:
			raise Http404

		return Response({"texto": "holi"})
~~~

Cuando hacemos la petición POST a `http://localhost:8000/api/v1/test-login/` usando un JSON con "usuario" y "password", nos devuelve el {"texto":"holi"}. Si pasamos solo uno de los valores, nos devuelve un 404.

Añadimos ahora el código para iniciar sesión:

~~~
def post(self, request):
		data = request.data
		if data.get("usuario") == None or data.get("password") == None:
			raise Http404

		user = authenticate(request, username=data.get("usuario"), 
			password=data.get("password"))

		if user is not None:
			login(request, user)
			usersMetadata = UsersMetadata.objects.filter(user_id=
				request.user.id).get()
			nombre = f"{request.user.first_name} {request.user.last_name}"
			data_json = {"mensaje":"Ok", "nombre": nombre}
			return Response(data_json)
		else:
			return Response({"mensaje":"Se ha ingresado datos incorrectos."})
~~~

Se ha añadido la carga de los metadatos del usuario correspondiente llamando a UsersMetadata (hay que importar el archivo "home.models"). También se va a retornar un mensaje de error cuando se ingresan datos incorrectos.

Hacer lo anterior está bien, pero es inseguro. Para segurizar un poco la aplicación, tenemos que hacer lo siguiente: siempre tendremos un método de login donde pasamos un nombre de usuario y contraseña, que nos devuelve un token en formato JTW. Luego, en las otras peticiones que hagamos dentro de la aplicación, vamos a pasar ese token por cabecero, y esa va a ser nuestra regla de validación en nuestros endpoints para procesar la información que se generó.

Para construir un token y retornarlo, vamos a usar el método que creamos en "utilidades". Se recomienda siempre enviar en nuestros token un campo que nos permita compararlo del otro lado cuando lo recibamos y verificar, porque una cosa es tener un token, y otra cosa es tener una doble llave de confirmación. Una buena técnica es colocar un campo que solo nosotros o nuestra aplicación conozca.

Añadimos en la vista:

~~~
token = utilidades.getToken({"id":request.user.id, "campo":"hola"})
data_json = {"mensaje":"Ok", "nombre": nombre, "token":token}
~~~ 

Al probar después de esto, la petición nos arroja un error de CSRF token. Podemos desactivar esta configuración descomentando la constante "REST_FRAMEWORK" en el archivo "settings".

~~~
REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': [
    'rest_framework.authentication.TokenAuthentication',]
}
~~~

Al volver a hacer la petición, me salía un error de que "'jwt' has no attribute 'encode'". Buscando en internet, es por causa de la librería "jwt". Desinstalándola e instalando "pyjwt" se resolvió el problema. Se pueden ver los datos correctamente.

Podemos agregarle también un parámetro de tiempo, un timestamp, lo que nos va a servir para revisar la fecha del token, y si es anterior a cierta fecha, que pierda su vigencia.

Creamos ahora un método para utilizar el token que se genera. Añadimos una ruta en el archivo "urls":

`path('test-jwt', Class_TestJwt.as_view()),`

El token que le vamos a pasar como cabecero a la clase puede funcionar con cualquier método. 

~~~
class Class_TestJwt(APIView):
	
	def get(self, request):
		cabeceros = request.headers.get('Authorization')
		if not cabeceros:
			res = HttpResponse("Unauthorized")
			res.status_code = 401
			return res
		data = request.data
		return Response({"texto":data.get('usuario')})
~~~

El nombre de un header puede ser cualquiera, pero un estándar es que el cabecero que de autenticación se llame 'Authorization'.

Al probar, llega bien, pero en cualquier caso llega un 401 con un texto "Unauthorized".

Para tener un token válido, es necesario que el usuario pase por el login, o sea tener sus credenciales registradas en el sistema. Tenemos ahora que "traducir" este token. Primero, tenemos que instalar "jose":

`pip install python-jose`

Importamos lo siguiente en el archivo de vistas:

`from jose import jwt`

Hacemos la primera prueba tratando de decodificar el token.

~~~
resuelto = jwt.decode(cabeceros, settings.SECRET_KEY, 
	algorithms = ['HS256'])
~~~

Hacemos la petición en el navegador, seleccionando "headers" y añadiendo un nuevo header "Authorization", pasándole como valor un token que nos entregue el mismo navegador al hacer una petición POST con la ruta "/test-login", sin las comillas. Importamos también el archivo "settings" y "HttpResponse".

Token: `eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiY2FtcG8iOiJob2xhIn0.s8kBEWlpcY8uSConjvkAcHwtOUMn4lQrZO_2nhAqrR0`

La vista va quedando así:

~~~
class Class_TestJwt(APIView):
	
	def get(self, request):
		cabeceros = request.headers.get('Authorization')
		if not cabeceros:
			res = HttpResponse("Unauthorized")
			res.status_code = 401
			return res

		resuelto = jwt.decode(cabeceros, settings.SECRET_KEY, 
			algorithms = ['HS256'])

		data = request.data
		return Response({"texto":resuelto})
~~~

Funciona correctamente. Cuando pasamos otro token, devuelve errores. Para que no pase, colocamos la variable "resuelto" en un "try":

~~~
try:	
	resuelto = jwt.decode(cabeceros, settings.SECRET_KEY, 
		algorithms = ['HS256'])
	except Exception as e:
		res = HttpResponse("Unauthorized")
		res.status_code = 401
		return res
~~~

Con eso, ingresar cualquier token no válido devuelve un "Unauthorized".

Podemos hacer una prueba adicional con el "campo":"hola". Podemos colocar en el código algo como esto:

~~~
if resuelto['campo'] != 'hola':
	res = HttpResponse("Unauthorized")
	res.status_code = 401
	return res
~~~

También, si tenemos un campo de fecha, podemos colocar un código que valide, por ejemplo, si el valor es menor a determinada fecha. Los tokens no expiran solos, mágicamente, sino que por lo general están programados para que dejen de tener validez desde cierta fecha.

---

### 69. Crear registros y subir archivos al servidor con API rest

Vamos a crear un endpoint para crear registros:

`path('test-crear-registros/', Class_TestCrearRegistros.as_view()),`

Vamos a probar creando un registro para Categoría.

En la nueva vista conservamos casi todo el código de la vista creada en la lección anterior, salvo porque cambiamos de "get" a "post", y añadimos la línea para crear un registro.

~~~
data = request.data
	cat = Categoria.objects.create(nombre=data["nombre"], 
		slug=data['nombre'])		
	return Response({"texto":f"Se creó la categoría {cat.nombre}"})
~~~

Hacemos un JSON con las claves que contengan los campos de esa tabla y los colocamos en una petición POST en la ruta "test-crear-registros/". Por ejemplo:

~~~
{"nombre":"Alimentos naturales", "slug":"alimentos-naturales"}
~~~

Revisando la base de datos, la categoría se creó correctamente. No hacía falta pasarle el slug porque lo teníamos automatizado, pero funcionó igual. Luego, mandé un JSON solo con un campo "nombre" y se creó el registro igual con el slug automático.

El ejemplo mostrado es solo una demostración. Normalmente no se hace así.

Para procesar, de forma más eficiente, un endpoint de creación de registros, creamos uno nuevo:

`path('test-serializable/', Class_TestSerializable.as_view()),`

Las clases serializables funcionan de la misma forma. Reciben JSON, cabeceros, etc., tal como en las clases anteriores que hemos creado. Para efectos de la lección, se va a hacer simple para poder entender fácil lo que es la serialización. Vamos a crear un método para listar los registros y un método para crear los registros. Para eso, tenemos que crear, en nuestra app de API, un archivo que siempre debe tener el nombre "serializers.py".

Los serializers son archivos que permiten desacoplar la estructura de los modelos que vamos a usar cuando consumamos una API Rest. El método que usamos para crear un registro no se considera el correcto, pero funciona, así que queda a criterio nuestro si lo usamos. Lo formal es usar serializables. Normalmente vamos a crear una clase para serializar cada uno de los modelos con los cuales vamos a trabajar en nuestras API. Por ejemplo, si queremos trabajar con nuestro modelo Categoría, creamos un serializable para ese modelo.

Importamos en el archivo:

`from rest_framework import serializers`
`from home.models import *`

~~~
class CategoriaSerializer(serializers.ModelSerializer):
	
	class Meta:
		model = Categoria
		fields = ("id", "nombre", "slug") #Campos específicos
		#fields = "__all__" #para todos los campos
~~~

Hay varias clases que se pueden heredar, pero vamos a usar `ModelSerializer`. Tenemos que formatearla con una "class Meta" para asignarle el modelo y los campos con los cuales vamos a trabajar.

Ahora, tenemos que importar el archivo "serializers" en el de vistas:

`from .serializers import *`

---
**NOTA:** En producción, no es necesario que todas las clases se llamen "Class_nombre". El instructor lo está haciendo así solo por mera costumbre, pero la clase, perfectamente, puede llamarse "TestSerializable".

---

Modificamos ahora la clase "Class_TestSerializable":

~~~
def get(self, request):
	datos = Categoria.objects.all()
	datos_json = CategoriaSerializer(datos, many=True)
	return Response(datos_json.data)
~~~

Ese método, así como está definido, nos va a servir en cualquier proyecto. El método, según sea el caso, puede recibir un parámetro (como un id); el queryset puede tener filtros (no siempre tiene que traer todos los datos). Creamos una variable donde traemos la clase serializer que reciba como parámetros el queryset, y también "many=True", que es una constante para traer todos los datos. Todo esto es para poder devolver los datos en formato JSON.

Hacemos una petición GET en el navegador (Insomnia en mi caso) y nos devuelve en formato JSON todos los registros del modelo Categoria.

Hacemos ahora el método POST para crear una categoría:

~~~
def post(self, request):
	datos_json = CategoriaSerializer(data = request.data)

	if datos_json.is_valid():
		datos_json.save()
		return Response(datos_json.data)

	return Response(datos_json.errors, status=400)
~~~

Dentro del llamado a CategoriaSerializer, pasamos como parámetro, la constante "data". Luego, hacemos la validación del JSON para que guarde los datos si son válidos. En el caso de que no, forzamos un HTTP 400 y devolvemos el error.

Hago la prueba haciendo la petición POST con un JSON simple:

`{"nombre":"Ropa"}`

Crea el registro correctamente, y devuelve:

~~~
{
	"id": 7,
	"nombre": "Ropa",
	"slug": "ropa"
}
~~~

Cuando hacemos una petición GET, no es necesario que devuelva todos los registros, sino que en el return Response() podemos colocar otra cosa. Igualmente con la petición POST al guardar un registro; podemos colocar que nos devuelva otra cosa, no necesariamente el JSON.data.


---

Hago por las mías una prueba mandando un JSON con un nodo distinto a "nombre" y devuelve el error correspondiente con un 400:

~~~
{
	"nombre": [
		"Este campo es requerido."
	]
}
~~~

**Nota mental:** ¿No será un poco peligroso que muestre el campo requerido? ¿No será carnada de hackers eso? Me imagino que ahí convendría más devolver un JSON genérico en vez del JSON.errors.

---

Creamos un endpoint para registrar un producto. El procedimiento cambia un poco porque vamos a crear un registro con clave foránea y con subida de archivos.

`path('test-productos/', Class_TestProductos.as_view()),`

Creamos ahora un serializable para nuestros productos. Dejamos el serializable y la vista así para empezar:

~~~
Serializable

class ProductoSerializer(serializers.ModelSerializer):
	
	class Meta:
		model = Producto		
		fields = "__all__"


Vista

class Class_TestProductos(APIView):
	
	def get(self, request):
		datos = Producto.objects.all()
		datos_json = ProductoSerializer(datos, many=True)
		return Response(datos_json.data)

	def post(self, request):
		datos_json = ProductoSerializer(data = request.data)

		if datos_json.is_valid():
			datos_json.save()
			return Response(datos_json.data)

		return Response(datos_json.errors, status=400)

~~~

Hacemos una petición GET para consultar los productos registrados, y vemos que el JSON nos devuelve, en el campo "foto", la ruta del archivo, y en el campo "categoria", el id de la categoría correspondiente, no el nombre de la misma. Para poder formatear de alguna manera la forma en que se muestra los datos, en la clase Meta cambiamos el `"__all__"` por la tupla:

~~~
fields = ("id", "categoria", "categoria_id", "nombre", 
	"descripcion", "foto")
~~~

Aún con esto, tanto "categoria" como "categoria_id" devuelven el id de la clave foránea y no el nombre de la categoría correspondiente. Para personalizar, sobre la clase Meta podemos definir la estructura del campo, pasándole el mismo tipo de campo con el que configuramos el campo en el modelo:

`categoria = serializers.CharField(source='categoria.nombre')`

Como se trata de una clave foránea, le pasamos como parámetro la constante "source", y ahí el valor que queremos buscar. En este caso, "categoria.nombre".

Haciendo la petición GET nuevamente, el nodo "categoria" nos muestra ahora el nombre de la categoría correspondiente. 

Ahora, para que el nodo "foto" no muestre la ruta completa, sino que solo el archivo, de manera simple, Django ofrece lo siguiente:

`foto = serializers.ImageField(use_url=False)`

Cuando nos toque registrar un producto, vamos a tener que usar un procedimiento distinto al JSON, ya que esto involucra subir archivos. Cuando vayamos a pasar los parámetros, en Postman seleccionamos la opción "form-data". En Insomnia parece que se selecciona "Multipart". Esto es en general cuando se hace una petición POST con subida de archivos.

Creamos una variable dentro del método POST, e importamos una clase en el archivo para trabajar con esa variable:

`from rest_framework.parsers import FileUploadParser`

~~~
class Class_TestProductos(APIView):
	
	def get(self, request):
		datos = Producto.objects.all()
		datos_json = ProductoSerializer(datos, many=True)
		return Response(datos_json.data)

	def post(self, request):
		parser_class = (FileUploadParser, )
		datos_json = ProductoSerializer(data = request.data)

		if datos_json.is_valid():
			datos_json.save()
			return Response(datos_json.data)

		return Response(datos_json.errors, status=400)
~~~

**Nota:** la variable parser_class siempre debe recibir la clase en una tupla para que funcione.

Creamos ahora otro serializable:

~~~
class ProductoSaveSerializer(serializers.ModelSerializer):

	class Meta:
		model = Producto
		fields = "__all__"


	def create(self, validated_data):
		return Producto.objects.create(**validated_data)
~~~

Modificamos ahora el método POST:

`datos_json = ProductoSaveSerializer(data = request.data)`


Ahora, probamos haciendo la petición POST. Usamos la opción "form-data" de Postman solamente porque va a haber un archivo de por medio. De no ser así, lo habríamos hecho con un JSON. 

Efectivamente, en Insomnia, es en la opción "Multipart". Tenemos que ir definiendo en el formulario todos los campos requeridos. En el campo "foto", nos da la opción de elegir un archivo, y ahí seleccionamos la imagen.

El procedimiento funcionó correctamente. En Insomnia funcionó a la primera, pero en Postman, para que funcione, hay que desmarcar temporalmente el "Content-Type = application/json".

Cuando vemos el directorio "/media", vemos que la imagen se guardó con el mismo nombre que el archivo original. Para mover el archivo y renombarlo como lo teníamos en la aplicación web, el instructor ofrece una variación de este método para que haga esas operaciones:

~~~
class Class_TestProductos(APIView):
	
	def get(self, request):
		datos = Producto.objects.all()
		datos_json = ProductoSerializer(datos, many=True)
		return Response(datos_json.data)

	def post(self, request):
		parser_class = (FileUploadParser, )
		datos_json = ProductoSaveSerializer(data = request.data)

		if datos_json.is_valid():
			datos_json.save()
			fotoarray = datos_json.data["foto"].split("/")
			foto = f"producto/{fotoarray[3]}"
			dreamhost.moverArchivoProducto(foto, 
				int(datos_json.data["id"]))

			return Response(datos_json.data, status=201)

		return Response(datos_json.errors, status=400)
~~~

---

### 70. Cliente API

Vamos a crear una vista sencilla para consumir la API que hemos creado. Instalamos la librería "requests" con pip si no lo hemos hecho. Creamos una ruta dentro de la app "reportes", con una vista y una plantilla, como siempre.

En el archivo de vistas, importamos "requests", y en el método que hemos creado pasaremos la ruta de nuestra API:

~~~
def clienteApi(request):
	url = "http://127.0.0.1:8000/api/v1/test-request/"
	response = requests.post(url=url)
	return render(request, "reportes/clienteApi.html", 
		{"response":response})
~~~

Hay que vigilar que la url tenga el "/" al final o va a mostrar un error.